<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

// my groups
$string['groupname'] = 'Gruppenavn';
$string['creategroup'] = 'Opret gruppe';
$string['groupmemberrequests'] = 'Afventende anmodninger om medlemskab';
$string['membershiprequests'] = 'Anmodninger om medlemskab';
$string['sendinvitation'] = 'Send invitation';
$string['invitetogroupsubject'] = 'Du er blevet inviteret til være med i en gruppe';
$string['invitetogroupmessage'] = '%s har inviteret dig til at være med i en gruppe, \'%s\'.  Klik på linket nedenfor for mere information.';
$string['inviteuserfailed'] = 'Kunne ikke invitere brugeren';
$string['userinvited'] = 'Invitation sendt';
$string['addedtogroupsubject'] = 'Du blev tilfæjet til en gruppe';
$string['addedtogroupmessage'] = '%s har rilføjet dig til en gruppe, \'%s\'.  Klik på linket nedenfor for at se gruppen';
$string['adduserfailed'] = 'Brugeren blev ikke tilføjet';
$string['useradded'] = 'Bruger tilføjet';
$string['editgroup'] = 'Rediger gruppe';
$string['savegroup'] = 'Gem gruppe';
$string['groupsaved'] = 'Gruppen er blevet gemt';
$string['invalidgroup'] = 'Gruppen eksisterer ikke';
$string['canteditdontown'] = 'Du kan ikke redigere gruppen, da du ikke ejer den';
$string['groupdescription'] = 'Gruppebeskrivelse';
$string['membershiptype'] = 'Gruppens medlemskabstype';
$string['membershiptype.controlled'] = 'Kontrolleret medlemskab';
$string['membershiptype.invite']     = 'Kun per invitation';
$string['membershiptype.request']    = 'Anmodet medlemskab';
$string['membershiptype.open']       = 'Åbent medlemskab';
$string['membershiptype.abbrev.controlled'] = 'Kontrolleret';
$string['membershiptype.abbrev.invite']     = 'Invitation';
$string['membershiptype.abbrev.request']    = 'Anmodning';
$string['membershiptype.abbrev.open']       = 'Åben';
$string['pendingmembers']            = 'Afventende medlemmer';
$string['reason']                    = 'Grund';
$string['approve']                   = 'Godkend';
$string['reject']                    = 'Afvis';
$string['groupalreadyexists'] = 'Der findes allerede en gruppe med det navn';
$string['Created'] = 'Oprettet';
$string['groupadmins'] = 'Gruppeadministratorer';
$string['Admin'] = 'Admin';
$string['grouptype'] = 'Gruppetype';
$string['publiclyviewablegroup'] = 'Offentligt synlig?';
$string['publiclyviewablegroupdescription'] = 'Tillad brugere, der ikke er logget ind, at se gruppen inklusiv gruppens fora?';
$string['Type'] = 'Type';
$string['publiclyvisible'] = 'Offentlig synlig';
$string['Public'] = 'Offentlig';
$string['usersautoadded'] = 'Tilføj automatisk brugere?';
$string['usersautoaddeddescription'] = 'Tilføj automatisk alle nye brugere til gruppen?';
$string['groupcategory'] = 'Gruppekategori';
$string['allcategories'] = 'Alle kategorier';
$string['groupoptionsset'] = 'Gruppeindstillingerne blev opdateret.';
$string['nocategoryselected'] = 'Ingn kategori valgt';
$string['categoryunassigned'] = 'Uden kategori';
$string['hasrequestedmembership'] = 'har anmodet om medlemskab til gruppen';
$string['hasbeeninvitedtojoin'] = 'er blevet inviteret til gruppen';
$string['groupinvitesfrom'] = 'Inviteret til gruppen:';
$string['requestedmembershipin'] = 'Anmodet om medlemskab ved:';
$string['viewnotify'] = 'Visningsmeddelelser';
$string['viewnotifydescription'] = 'Hvis slået til, vil en besked blive sendt til alle gruppemedlemmer når et af medlemmerne deler en visning med gruppen. Hvis dette er slået til i større grupper, kan det resultere i mange beskeder.';

$string['editgroupmembership'] = 'Rediger grupemedlemskab';
$string['editmembershipforuser'] = 'Rediger medlemskab for %s';
$string['changedgroupmembership'] = 'Gruppemedlesskab blev opdatret.';
$string['changedgroupmembershipsubject'] = 'Dine gruppemedlemskaber er blevet ændret';
$string['addedtogroupsmessage'] = "%s har tilføjet dig til følgende gruppe(r):\n\n%s\n\n";
$string['removedfromgroupsmessage'] = "%s har fjernet dig fra følgende gruppe(r):\n\n%s\n\n";
$string['cantremoveuserisadmin'] = "Undervisere kan ikke fjerne administratorer og andre underviser-medlemmer.";
$string['cantremovemember'] = "Undervisere kan ikke fjerne medlemmer.";

// Used to refer to all the members of a group - NOT a "member" group role!
$string['member'] = 'medlem';
$string['members'] = 'medlemmer';
$string['Members'] = 'Medlemmer';

$string['memberrequests'] = 'medlemskabsanmodninger';
$string['declinerequest'] = 'Afvis anmodning';
$string['submittedviews'] = 'Indsendte visninger';
$string['releaseview'] = 'Frigiv visning'; // Kontekst?
$string['invite'] = 'Inviter';
$string['remove'] = 'Fjern';
$string['updatemembership'] = 'Opdater medlemskab';
$string['memberchangefailed'] = 'Ikke al medlemskabsinformation blev opdateret';
$string['memberchangesuccess'] = 'medlemskabsstatus er blevet ændret';
$string['viewreleasedsubject'] = 'Din visning "%s" er blevet frigivet fra %s af %s';
$string['viewreleasedmessage'] = 'Din visning "%s" er blevet frigivet fra %s af %s';
$string['viewreleasedsuccess'] = 'Visningen blev frigivet';
$string['groupmembershipchangesubject'] = 'Gruppemedlemskab: %s';
$string['groupmembershipchangedmessagetutor'] = 'Du er blevet forfremmet til underviser i gruppen';
$string['groupmembershipchangedmessagemember'] = 'Du er blevet degraderet fra underviser i gruppen';
$string['groupmembershipchangedmessageremove'] = 'Du er blevet fjerne fra gruppen';
$string['groupmembershipchangedmessagedeclinerequest'] = 'Din anmodning om at blive medlem af gruppen er blevet afvist';
$string['groupmembershipchangedmessageaddedtutor'] = 'Du er blevet tilføjet som en underviser i gruppen';
$string['groupmembershipchangedmessageaddedmember'] = 'Du er blevet tilføjet som et medlem af gruppen';
$string['leavegroup'] = 'Forlad gruppen';
$string['joingroup'] = 'Meld dig ind i gruppen';
$string['requestjoingroup'] = 'Anmod om at blive medlem af gruppen';
$string['grouphaveinvite'] = 'Du er blevet inviteret til at blive medlem af gruppen';
$string['grouphaveinvitewithrole'] = 'Du er blevet inviteret til at blive medlem af gruppen med en rolle som'; // Kontekst? Jeg går udfra at det næste ord er rollen man har fået tildelt?
$string['groupnotinvited'] = 'Du er ikke blevet inviteret til at blive medlem af gruppen';
$string['groupinviteaccepted'] = 'Invitation accepteret. Du er nu medlem af gruppen';
$string['groupinvitedeclined'] = 'Invitation afvist.';
$string['acceptinvitegroup'] = 'Accepter';
$string['declineinvitegroup'] = 'Afvis';
$string['leftgroup'] = 'Du har nu forladt gruppen';
$string['leftgroupfailed'] = 'Forladning af gruppen mislykkedes';
$string['couldnotleavegroup'] = 'Du kan ikke forlade gruppen';
$string['joinedgroup'] = 'Du er nu et gruppemedlem';
$string['couldnotjoingroup'] = 'Du kan ikke blive medlem af gruppen';
$string['grouprequestsent'] = 'Gruppemedlemskab anmodning sendt';
$string['couldnotrequestgroup'] = 'Kunne ikke sende anmoding om gruppemedlemskab';
$string['cannotrequestjoingroup'] = 'Du kan ikke anmode om medlemskab af gruppen';
$string['groupjointypeopen'] = 'Gruppen har åbent medlemskab. Du er velkommen til at være med.';
$string['groupjointypecontrolled'] = 'Gruppen har kontrolleret medlemskab. Du kan ikke blive medlem af gruppen.';
$string['groupjointypeinvite'] = 'medlemskab i gruppep er kun per invitation.';
$string['groupjointyperequest'] = 'medlemskab i gruppen er kun per anmodning.';
$string['grouprequestsubject'] = 'Ny gruppemedlemskabsanmodning';
$string['grouprequestmessage'] = '%s vil gerne være medlem af din gruppe %s';
$string['grouprequestmessagereason'] = "%s vil gerne være medlem af din gruppe %s. Grunden til at være medlem er:\n\n%s";
$string['cantdeletegroup'] = 'Du kan ikke slette gruppen';
$string['groupconfirmdelete'] = 'Er du sikker på at du vil slette gruppen?';
$string['deletegroup'] = 'Gruppen er blevet slettet';
$string['allmygroups'] = 'Alle mine grupper';
$string['groupsimin']  = 'Grupper jeg er med i';
$string['groupsiown']  = 'Grupper jeg ejer';
$string['groupsiminvitedto'] = 'Grupper jeg har fået en invitation fra';
$string['groupsiwanttojoin'] = 'Grupper jeg vil være medlem af';
$string['requestedtojoin'] = 'Du har anmodet om medlemskab af gruppen';
$string['groupnotfound'] = 'Kunne ikke finde en gruppe med id %s';
$string['groupconfirmleave'] = 'Er du sikker på at du vil forlade gruppen?';
$string['groupconfirmleavehasviews'] = 'Er du sikker på at du vil forlade gruppen? Nogle af dine visninger bruger gruppen som adgangskontrol, og forlader du gruppen vil gruppemedlemmerne ikke længere have adgang til disse visninger.';
$string['cantleavegroup'] = 'Du kan ikke forlade gruppen';
$string['usercantleavegroup'] = 'Brugeren kan ikke forlade gruppen';
$string['usercannotchangetothisrole'] = 'Brugeren kan ikke skifte til rollen';
$string['leavespecifiedgroup'] = 'Forlad gruppe \'%s\'';
$string['memberslist'] = 'Medlemmer: ';
$string['nogroups'] = 'Ingen grupper';
$string['deletespecifiedgroup'] = 'Slet gruppe \'%s\'';
$string['requestjoinspecifiedgroup'] = 'Anmod om at blive medlem af gruppen \'%s\'';
$string['youaregroupmember'] = 'Du er medlem af gruppen';
$string['youaregrouptutor'] = 'Du er underviser i gruppen';
$string['youaregroupadmin'] = 'Du er administrator i gruppen';
$string['youowngroup'] = 'Du ejer gruppen';
$string['groupsnotin'] = 'Grupper jeg ikke er med i';
$string['allgroups'] = 'Alle grupper';
$string['allgroupmembers'] = 'Alle gruppemedlemmer';
$string['trysearchingforgroups'] = 'Prøv at %ssøge efter grupper%s du kan blive medlem af';
$string['nogroupsfound'] = 'Fandt ingen grupper.';
$string['group'] = 'gruppe';
$string['Group'] = 'Gruppe';
$string['groups'] = 'grupper';
$string['notamember'] = 'Du er ikke medlem af gruppen';
$string['notmembermayjoin'] = 'Du skal være medlem af gruppen \'%s\' for at se denne side.';
$string['declinerequestsuccess'] = 'Anmonding om gruppemedlemskab er blevet afvist.';
$string['notpublic'] = 'Gruppen er ikke offentlig.';

// Bulk add, invite
$string['addmembers'] = 'Tilføj medlemmer';
$string['invitationssent'] = '%d invitationer sendt';
$string['newmembersadded'] = 'Tilføjede %d nye medlemmer';
$string['potentialmembers'] = 'Potentielle medlemmer';
$string['sendinvitations'] = 'Send invitationer';
$string['userstobeadded'] = 'Brugere der skal tilføjes';
$string['userstobeinvited'] = 'Brugere der skal inviteres';

// friendslist
$string['reasonoptional'] = 'Grund (valgfri)';
$string['request'] = 'Anmodning';

$string['friendformaddsuccess'] = 'Tilføjede %s til din venneliste';
$string['friendformremovesuccess'] = 'Fjernede %s fra din venneliste';
$string['friendformrequestsuccess'] = 'Venneanmodning sendt til %s';
$string['friendformacceptsuccess'] = 'Accepterede venneanmodning';
$string['friendformrejectsuccess'] = 'Afviste venneanmodning';

$string['addtofriendslist'] = 'Tilføj som ven';
$string['requestfriendship'] = 'Tilføj som ven';

$string['addedtofriendslistsubject'] = '%s har tilføjet dig som ven';
$string['addedtofriendslistmessage'] = '%s har tilføjet dig som ven. Det betyder at %s også er på din venneliste nu. '
    . ' Klik på linket nedenfor for at se %ss profil';

$string['requestedfriendlistsubject'] = 'Ny venneanmodning';
$string['requestedfriendlistmessage'] = '%s har anmodet om blive tilføjet som din ven.'
    . ' Du kan enten gøre dette fra linket nedenfor eller fra siden med din venneliste';

$string['requestedfriendlistmessagereason'] = '%s har anmodet om at blive tilføjet som din ven.'
    . ' Du kan enten gøre dette fra linket nedenfor eller fra siden med din venneliste.'
    . ' Grunden var:
    ';

$string['removefromfriendslist'] = 'Fjern ven';
$string['removefromfriends'] = 'Fjern %s fra venner';
$string['confirmremovefriend'] = 'Er du sikker på at du vil fjerne brugeren fra din venneliste?';
$string['removedfromfriendslistsubject'] = 'Fjernet fra venneliste';
$string['removedfromfriendslistmessage'] = '%s har fjernet dig fra sin venneliste.';
$string['removedfromfriendslistmessagereason'] = '%s har fjernet dig fra sin venneliste. Grunden var: ';
$string['cantremovefriend'] = 'Du kan ikke fjerne brugeren fra din venneliste';

$string['friendshipalreadyrequested'] = 'Du har anmodet om at blive tilføjet til %s\s venneliste';
$string['friendshipalreadyrequestedowner'] = '%s har anmodet om at blive tilføjet til din venneliste';
$string['rejectfriendshipreason'] = 'Grund til at afvise anmodning';
$string['alreadyfriends'] = 'Du er allerede venner med %s';

$string['friendrequestacceptedsubject'] = 'Venneanmodning accepteret';
$string['friendrequestacceptedmessage'] = '%s har accepteret din venneanmodning og er blevet tilføjet til din venneliste'; 
$string['friendrequestrejectedsubject'] = 'Venneanmodning afvist';
$string['friendrequestrejectedmessage'] = '%s har afvist din venneanmodning.';
$string['friendrequestrejectedmessagereason'] = '%s har afvist din venneanmodning. Grunden var: ';

$string['allfriends']     = 'Alle venner';
$string['currentfriends'] = 'Nuværende venner';
$string['pendingfriends'] = 'Afventende venner';
$string['backtofriendslist'] = 'Tilbage til vennelisten';
$string['findnewfriends'] = 'Find nye venner';
$string['Views']          = 'Visninger';
$string['Files']          = 'Filer';
$string['seeallviews']    = 'Se alle %s visninger...';
$string['noviewstosee']   = 'Ingen som du kan se :(';
$string['whymakemeyourfriend'] = 'Dette er grunden til at vi skal være venner:';
$string['approverequest'] = 'Godkend anmodning';
$string['denyrequest']    = 'Afvis anmodning';
$string['pending']        = 'afventende';
$string['trysearchingforfriends'] = 'Prøv at %ssøge efter nye venner%s for at udvide dit netværk!';
$string['nobodyawaitsfriendapproval'] = 'Ingen afventer din godkendelse til at blive din ven';
$string['sendfriendrequest'] = 'Tilføj som ven';
$string['addtomyfriends'] = 'Tilføj til mine venner';
$string['friendshiprequested'] = 'Venskab anmodet';
$string['existingfriend'] = 'eksisterende ven';
$string['nosearchresultsfound'] = 'Ingen søgeresultater fundet :(';
$string['friend'] = 'ven';
$string['friends'] = 'venner';
$string['user'] = 'bruger';
$string['users'] = 'brugere';
$string['Friends'] = 'Venner';

$string['friendlistfailure'] = 'Redigering af din venneliste mislykkedes';
$string['userdoesntwantfriends'] = 'Brugeren vil ikke have nye venner';
$string['cannotrequestfriendshipwithself'] = 'Du kan ikke anmode om et venskab med dig selv';
$string['cantrequestfriendship'] = 'Du kan ikke anmode om et venskab med brugeren';

// Messaging between users
$string['messagebody'] = 'Send besked'; // wtf
$string['sendmessage'] = 'Send besked';
$string['messagesent'] = 'Besked sendt';
$string['messagenotsent'] = 'Sending af besked mislykkedes';
$string['newusermessage'] = 'Ny besked fra %s';
$string['newusermessageemailbody'] = '%s har sendt dig en besked.  For at se beskeden, besøg

%s';
$string['sendmessageto'] = 'Send besked til %s';
$string['viewmessage'] = 'Se besked';
$string['Reply'] = 'Svar';

$string['denyfriendrequest'] = 'Afvis venneanmodning';
$string['sendfriendshiprequest'] = 'Send en venneanmodning til %s';
$string['cantdenyrequest'] = 'Det er ikke en gyldig venneanmodning';
$string['cantrequestfrienship'] = 'Du kan ikke anmode om venskab med brugeren';
$string['cantmessageuser'] = 'Du kan ikke sende en besked til brugeren';
$string['cantviewmessage'] = 'Du kan ikke se beskeden';
$string['requestedfriendship'] = 'anmodede om venskab'; // Kontekst?
$string['notinanygroups'] = 'Ikke med i nogle grupper';
$string['addusertogroup'] = 'Tilføj til ';
$string['inviteusertojoingroup'] = 'Inviter til ';
$string['invitemembertogroup'] = 'Inviter %s til at blive medlem af \'%s\'';
$string['cannotinvitetogroup'] = 'Du kan ikke invitere denne bruger til gruppen';
$string['useralreadyinvitedtogroup'] = 'Brugeren er allerede blevet inviteret til eller er medlem af gruppen.';
$string['removefriend'] = 'Fjern ven';
$string['denyfriendrequestlower'] = 'Afvis venneanmodning';

// Group interactions (activities)
$string['groupinteractions'] = 'Gruppeaktiviteter';
$string['nointeractions'] = 'Der er ingen aktivteter i gruppen';
$string['notallowedtoeditinteractions'] = 'Du har ikke tilladelse til at tilføje eller redigere aktiviteter i gruppen';
$string['notallowedtodeleteinteractions'] = 'Du har ikke tilladelse til at slette aktiviteter i gruppen';
$string['interactionsaved'] = '%s er blevet gemt';
$string['deleteinteraction'] = 'Slet %s \'%s\'';
$string['deleteinteractionsure'] = 'Er du sikker på at du vil gøre dette? Det kan ikke fortrydes.';
$string['interactiondeleted'] = '%s er blevet slettet';
$string['addnewinteraction'] = 'Tilføj ny %s';
$string['title'] = 'Titel';
$string['Role'] = 'Rolle';
$string['changerole'] = 'Skift rolle';
$string['changeroleofuseringroup'] = 'Skift rolle af %s i %s';
$string['currentrole'] = 'Nuværende rolle';
$string['changerolefromto'] = 'Skift rolle fra %s til';
$string['rolechanged'] = 'Rolle skiftet';
$string['removefromgroup'] = 'Fjern fra gruppe';
$string['userremoved'] = 'Bruger fjernet';
$string['About'] = 'Om';
$string['aboutgroup'] = 'Om %s';

$string['Joined'] = 'Blev medlem';

$string['membersdescription:invite'] = 'Gruppen er kun per invitation. Du kan invitere brugere gennem deres profilsider eller <a href="%s">sende flere invitationer på én gang</a>.';
$string['membersdescription:controlled'] = 'Gruppen har kontrolleret medlemskab. Du kan tilføje brugere gennem deres profilsider ellerY <a href="%s">tilføje flere brugere på én gang</a>.';

// View submission
$string['submit'] = 'Indsend';
$string['allowssubmissions'] = 'Tillad insendinger';
?>
