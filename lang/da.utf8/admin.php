<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['administration'] = 'Administration';

// Installer
$string['installation'] = 'Installation';
$string['release'] = 'version %s (%s)';
$string['copyright'] = 'Copyright &copy; 2006 onwards, <a href="http://wiki.mahara.org/Contributors">Catalyst IT Ltd and others</a>';
$string['installmahara'] = 'Installer Mahara';
$string['component'] = 'Komponent eller plugin';
$string['continue'] = 'Fortsæt';
$string['coredata'] = 'kernedata';
$string['coredatasuccess'] = 'Kernedata er blevet installeret';
$string['fromversion'] = 'Fra version';
$string['information'] = 'Information';
$string['installsuccess'] = 'Fik installeret version ';
$string['toversion'] =  'Til version';
$string['localdatasuccess'] = 'Lokale tilpasninger installeret';
$string['notinstalled'] = 'Ikke installeret';
$string['nothingtoupgrade'] = 'Intet at opdateree';
$string['performinginstallation'] = 'Udfører installation...';
$string['performingupgrades'] = 'Udfører opdatering...';
$string['runupgrade'] = 'Kør opdatering';
$string['successfullyinstalled'] = 'Mahara er blevet installeret!';
$string['thefollowingupgradesareready'] = 'De følgende opgraderinger er klar:';
$string['registerthismaharasite'] = 'Registrer denne Mahara webside';
$string['upgradeloading'] = 'Indlæser...';
$string['upgrades'] = 'Opgraderinger';
$string['upgradesuccess'] = 'Opgradering fuldført';
$string['upgradesuccesstoversion'] = 'Opgradering gennemført til version ';
$string['upgradefailure'] = 'Opgradering mislykkedes!';
$string['noupgrades'] = 'Intet at opgradere! Du er fuldt ajour!';
$string['youcanupgrade'] = 'Du kan opgradere Mahara fra %s (%s) til %s (%s)!';
$string['Plugin'] = 'Plugin';
$string['jsrequiredforupgrade'] = 'Du skal slå javascript til for at udføre en installation eller opgradering.';
$string['dbnotutf8warning'] = 'Du bruger ikke en UTF-8 database. Mahara gemmer al data som UTF-8 internt. Du kan stadig forsøge denne opgradering, men det anbefales at du konverterer din database til UTF-8.';
$string['dbcollationmismatch'] = 'En kolonne af din database bruger en kollationering, der ikke er den samme som databasens standard.  Sørg venligst for at alle kolonner bruger samme kollationering som databasen.';

// Admin navigation menu
$string['adminhome']      = 'Admin hjem';
$string['configsite']  = 'Opsætning af websiden';
$string['configusers'] = 'Administrer brugere';
$string['groups'] = 'Grupper';
$string['managegroups'] = 'Administrer grupper';
$string['Extensions']   = 'Udvidelser';
$string['configextensions']   = 'Administrer udvidelser';
$string['manageinstitutions'] = 'Administrer institutioner';

// Admin homepage strings
$string['siteoptions']    = 'Webside indstillinger';
$string['siteoptionsdescription'] = 'Indstil grundlæggende webside indstillinger såsom navn, sprog og tema';
$string['editsitepages']     = 'Rediger websidens sider';
$string['editsitepagesdescription'] = 'Rediger indholdet af forskellige sider rundtomkring på websiden';
$string['menus'] = 'Menuer';
$string['menusdescription'] = 'Administrer links og filer fundet i Links, Ressourcer og Sidefod menuerne';
$string['sitefiles']          = 'Webside filer';
$string['sitefilesdescription'] = 'Upload og administrer filer, der kan placeres i Links og Ressourcer menuerne og i webside visninger';
$string['siteviews']          = 'Webside visninger';
$string['siteviewsdescription'] = 'Opret og administrer visninger og visningsskabeloner for hele websiden';
$string['networking']          = 'Netværk';
$string['networkingdescription'] = 'Konfigurer netværk for Mahara';

$string['staffusers'] = 'Personale brugere';
$string['staffusersdescription'] = 'Giv brugere personale tilladelser';
$string['adminusers'] = 'Admin brugere';
$string['adminusersdescription'] = 'Give webside administrator adgangsrettigheder';
$string['institution']   = 'Institution';
$string['institutions']   = 'Institutioner';
$string['institutiondetails']   = 'Institution detaljer';
$string['institutionauth']   = 'Institution autoriteter';
$string['institutionsdescription'] = 'Installer or administerer installerede institutioner';
$string['adminnotifications'] = 'Admin beskeder';
$string['adminnotificationsdescription'] = 'Overblik over hvordan administratorer modtager systembeskeder';
$string['uploadcsv'] = 'Tilføj brugere via CSV';
$string['uploadcsvdescription'] = 'Upload en CSV fil med de nye brugere';
$string['usersearch'] = 'Bruger søgning';
$string['usersearchdescription'] = 'Søg blandt alle brugere og lav administrative handlinger på dem'; // Det lød slet ikke skummelt...
$string['usersearchinstructions'] = 'Du kan søge efter brugere ved at klikke på forbogstaverne af deres for- og efternavne, eller ved at indtaste et navn i søgefeltet.  Du kan også indtaste en e-mail-adresse i søgefeltet, hvis vil søge efter e-mail-adresser.';

$string['administergroups'] = 'Administrer gruppper';
$string['administergroupsdescription'] = 'Udpeg gruppeadministratorer og slet grupper';
$string['groupcategoriesdescription'] = 'Opret og rediger gruppekategorier';

$string['institutionmembersdescription'] = 'Tilknyt brugere til institutioner';
$string['institutionstaffdescription'] = 'Giv brugere personale tilladelser';
$string['institutionadminsdescription'] = 'Giv institutionsadministrator rettigheder';
$string['institutionviews']          = 'Institutionsvisninger';
$string['institutionviewsdescription'] = 'opret og administrer visninger og visningsskabeloner for en institution';
$string['institutionfiles']          = 'Institution filer';
$string['institutionfilesdescription'] = 'Upload og administrer filer til brug i institutionsvisninger';

$string['pluginadmin'] = 'Plugin administration';
$string['pluginadmindescription'] = 'Installer og konfigurer plugins';

$string['htmlfilters'] = 'HTML-filtre';
$string['htmlfiltersdescription'] = 'Slå nye filtre til HTML-renseren til';
$string['newfiltersdescription'] = 'Hvis du har downloadet et nyt sæt HTML-filtre, kan du installere dem ved at pake filen ud i mappen %s aog klikke på knappen nedenfor';
$string['filtersinstalled'] = 'Filtre installeret.';
$string['nofiltersinstalled'] = 'Ingen HTML-filtre installeret.';


// Group management
$string['groupcategories'] = 'Gruppekategorier';
$string['allowgroupcategories'] = 'Tillad gruppekategorier';
$string['enablegroupcategories'] = 'Slå gruppekategorier til';
$string['addcategories'] = 'Tilføj kategorier';
$string['allowgroupcategoriesdescription'] = 'Hvis dette er slået til kan administratorer lave kategorier som brugerne kan tilføje til deres grupper';
$string['groupoptionsset'] = 'Gruppeindstilingerne er blevet opdateret.';
$string['groupcategorydeleted'] = 'Kategori slettet';
$string['confirmdeletecategory'] = 'Er du sikker på at du vil slette denne kategori?';
$string['groupcategoriespagedescription'] = 'De viste kategorier her kan tilføjes til grupper under deres oprettelse og kan bruges til at filtrere grupper når man søger på dem.';
$string['groupadminsforgroup'] = "Gruppeadministratorer for '%s'";
$string['potentialadmins'] = 'Potentielle administratorer';
$string['currentadmins'] = 'Nuværende administratorer';
$string['groupadminsupdated'] = 'Gruppeadministratorne er blevet opdateret';

// Register your Mahara
$string['Field'] = 'Felt';
$string['Value'] = 'Værdi';
$string['datathatwillbesent'] = 'Data der vil blive sendt';
$string['sendweeklyupdates'] = 'Send ugentlige opdateringer?';
$string['sendweeklyupdatesdescription'] = 'Hvis dette er valgt vil denne webside sende ugenlige opdateringer til mahara.org med nogle tal om din webside';
$string['Register'] = 'Registrer';
$string['registrationfailedtrylater'] = 'Registrering mislykkedes med fejlkode %s. Prøv igen senere.';
$string['registrationsuccessfulthanksforregistering'] = 'Registrering gennemført - tak fordi du restrerede!';
$string['registeryourmaharasite'] = 'Registrer din Mahara webside';
$string['registeryourmaharasitesummary'] = '
<p>Du kan vælge at registere din Mahara webside hos <a href="http://mahara.org/">mahara.org</a>, og hjælpe os med at få indsigt i, hvordan Mahara bliver brugt verden rundt.  Denne besked kan fjernes ved at registere.</p>
<p>Du kan registere websiden og se, hvilken information, der vil bleve sendt på <strong><a href="%sadmin/registersite.php">Webside registreringssiden.</a></strong></p>';
$string['registeryourmaharasitedetail'] = '
<p>Du kan vælge at registere din Mahara webside hos <a href="http://mahara.org/">mahara.org</a>. Det er gratis at registere sig, og det hjælper os med at få indsigt i, hvordan Mahara bliver brugt verden rundt</p>
<p>Du kan se den information, der vil blive sendt til mahara.org - ingen der kan bruges til at identificere vil blive sendt.</p>
<p>Hvis du vælger &quot;send ugentlige opdateringer&quot;, vil Mahara automatisk sende en opdatering til mahara.org en gang om ugen med opdateret information.</p>
<p>Denne besked kan fjernes ved at registere. Du kan ændre om du vil sende ugenlige opdateringer på <a href="%sadmin/site/options.php">webside indstilinger</a> siden.</p>';
$string['siteregistered'] = 'Din webside er blevet regisreret. Du kan ændre om du vil sende ugenlige opdateringer på <a href="%sadmin/site/options.php">webside indstilinger</a> siden.</p>';

// Close site
$string['Close'] = 'Luk';
$string['closesite'] = 'Luk websiden';
$string['closesitedetail'] = 'Du kan lukke siden for alle undtaget administratorer.  Dette kan være nyttigt i forbindelse med database opgraderinger.  Kun administratorer vil være i stand til at logge ind, indtil du enten genåbner siden eller gennemfører en opdatering.';
$string['Open'] = 'Åbn';
$string['reopensite'] = 'Genåbn websiden';
$string['reopensitedetail'] = 'Din webside er lukket.  Website administratorer kan fortsat være logget ind indil der sker en opdatering.';

// Statistics
$string['siteinformation'] = 'Website information';
$string['viewfullsitestatistics'] = 'Se den samlede statistik for websitet';
$string['sitestatistics'] = 'Website statistik';
$string['siteinstalled'] = 'Website installeret';
$string['databasesize'] = 'Database størrelse';
$string['diskusage'] = 'Diskforbrug';
$string['maharaversion'] = 'Mahara version';
$string['activeusers'] = 'Aktive brugere';
$string['loggedinsince'] = '%s i dag, %s siden %s, %s i alt';
$string['groupmemberaverage'] = 'I gennemsit, hver bruger er i %s grupper';
$string['viewsperuser'] = 'Brugere, der kan lave visninger, har omkring %s visninger hver';
$string['Cron'] = 'Cron';
$string['runningnormally'] = 'Kører normalt';
$string['cronnotrunning'] = 'Cron kører ikke.<br>Se i <a href="http://wiki.mahara.org/System_Administrator\'s_Guide/Installing_Mahara">installationsguiden</a> for instruktioner i hvordan man sætter det op.';
$string['Loggedin'] = 'Logget ind';
$string['youraverageuser'] = 'Den gennemsnitlige bruger...';
$string['statsmaxfriends'] = 'Har %s venner (rekorden er <a href="%s">%s</a> med %d)';
$string['statsnofriends'] = 'Har 0 venner :(';
$string['statsmaxviews'] = 'Har lavet %s visninger (rekorden er <a href="%s">%s</a> med %d)';
$string['statsnoviews'] = 'Har lavet 0 visninger :(';
$string['statsmaxgroups'] = 'Er i %s grupper (rekorden er <a href="%s">%s</a> med %d)';
$string['statsnogroups'] = 'Er i 0 grupper :(';
$string['statsmaxquotaused'] = 'Har brugt omkring %s af diskkvoten (rekorden er <a href="%s">%s</a> med %d)';
$string['groupcountsbytype'] = 'Antal grupper efter gruppetyper';
$string['groupcountsbyjointype'] = 'Antal grupper efter adgangstype';
$string['blockcountsbytype'] = 'Oftest brugt blok i portefølje visningerne:';
$string['uptodate'] = 'opdateret';
$string['latestversionis'] = 'nyeste version er <a href="%s">%s</a>';
$string['viewsbytype'] = 'Visninger efter type';
$string['userstatstabletitle'] = 'Daglig brugerstatistik';
$string['groupstatstabletitle'] = 'Største grupper';
$string['viewstatstabletitle'] = 'Mest populære visninger';

// Site options
$string['adminsonly'] = 'Kun for administratorer';
$string['adminsandstaffonly'] = 'Kun for administratorer og personale';
$string['advanced'] = 'Avanceret';
$string['allowpublicviews'] = 'Tillad offentlige visninger';
$string['allowpublicviewsdescription'] = 'Hvis slået til, vil brugere kunne lave portefølje vinsinger, der kan ses af offentligeheden i stedet for kun at kunne ses af registerede brugere.';
$string['allowpublicprofiles'] = 'Tillad offentlige profiler';
$string['allowpublicprofilesdescription'] = 'Hvis slået til, vil brugerne kunne sætte deres profilvisninger til at kunne ses af offentligeheden i stedet for kun at kunne ses af registerede brugere.';
$string['anonymouscomments'] = 'Anonyme kommentarer';
$string['anonymouscommentsdescription'] = 'Hvis slået til, kan brugere der ikke er logget ind kommentere på offentlige visninger eller visninger de kan se via henneligt URL.';
$string['antispam'] = 'Anti-spam';
$string['antispamdescription'] = 'Hvilken type anti-spam der bruges på ofentligt tilgængelige measures used on publicly visible indtastningsfelter';
$string['defaultaccountinactiveexpire'] = 'Standard inaktiv konto tid';
$string['defaultaccountinactiveexpiredescription'] = 'Hvor længe en brugerkonto vil være aktiv uden at brugeren logger ind';
$string['defaultaccountinactivewarn'] = 'Advarselstid for inaktivitet/udløb';
$string['defaultaccountinactivewarndescription'] = 'Hvor lang tid før brugerkonti enten er ved at udløbe eller blive inaktive skal der sendes en advarsel om det til brugeren?';
$string['defaultaccountlifetime'] = 'Standar konto levetid';
$string['defaultaccountlifetimedescription'] = 'Hvis slået til, vil en konto udløbe efter den angivne tid efter de blev oprettet';
$string['embeddedcontent'] = 'Indlagt indhold';
$string['embeddedcontentdescription'] = 'Hvis du ønsker at brugerne skal kunne indlægge video eller andet indhold fra ekserne kilder i deres porteføljer, kan du vælge hvilke sider du stoler på nedenfor.';
$string['Everyone'] = 'Alle';
$string['homepageinfo'] = 'Vis hjemmeside information';
$string['homepageinfodescription'] = 'Hvis slået til, vil information om Mahara og hvordan det bruges blive vist på Mahara hjemmesiden. Brugere der er logget ind vil have mulighed for at slå det fra.';
$string['institutionautosuspend'] = 'Luk automatisk udløbe institutioner';
$string['institutionautosuspenddescription'] = 'Hvis slået til, vil udløbne institutioner automatisk blive lukket';
$string['institutionexpirynotification'] = 'Advarselstid for udløbne institutioner';
$string['institutionexpirynotificationdescription'] = 'Hvor længe før udløbningen at en besked vil blive sendt til website og institutions administratorne';
$string['language'] = 'Sprog';
$string['none'] = 'Ingen';
$string['country'] = 'Land';
$string['pathtoclam'] = 'Sti til clam'; // Så fristende at skrive "Rute til musling" her...
$string['pathtoclamdescription'] = 'Filsystem stien til clamscan eller clamdscan';
$string['registerterms'] = 'Registrationsaftale';
$string['registertermsdescription'] = "Kræv at brugerne accepterer Vilkår og Betingelser aftalen før de kan registere.  Du bør redigere dit website Vilkår og Betingelser før du slår denne funktion til.";
$string['remoteavatars'] = 'Vis fjernavatarer';
$string['remoteavatarsdescription'] = 'Hvis slået til, vil <a href="http://www.gravatar.com">Gravatar</a> tjenesten blive brugt til brugernes standard profil-ikoner.';
$string['searchplugin'] = 'Søgeplugin';
$string['searchplugindescription'] = 'Hvilker søgeplugin der bruges';
$string['searchusernames'] = 'Søg brugernanve';
$string['searchusernamesdescription'] = 'Tillad at der bliver søgt på brugernavne sem en del af en brugersøgning.';
$string['sessionlifetime'] = 'Automatisk log ud tid';
$string['sessionlifetimedescription'] = 'Tid i minutter om hvor længe der går inden en inaktiv, men logget ind bruer bliver logget ud automatisk';
$string['setsiteoptionsfailed'] = 'Ændring af %s indstillingen slog fejl';
$string['showonlineuserssideblock'] = 'Vis online brugere';
$string['showonlineuserssideblockdescription'] = 'Hvis slået til, vil brugere se en blok i højre side af sitet, der viser en liste over hvem der er online.';
$string['showselfsearchsideblock'] = 'Slå portefølje søgning til';
$string['showselfsearchsideblockdescription'] = 'Vis "Søg mit portefølje" sideblokken på Mit portefølje delen af websitet';
$string['showtagssideblock'] = 'Slå Tag-sky til';
$string['showtagssideblockdescription'] = 'Hvis slået til vil brugerne se en sideblok i deres Mit portefølje side, der viser en liste over deres mest brugte tags';
$string['simple'] = 'Simpel';
$string['sitedefault'] = 'Website standard';
$string['sitelanguagedescription'] = 'Standardsproget for websitet';
$string['sitecountrydescription'] = 'Standardlandet for websitet';
$string['sitename'] = 'Websitenavn';
$string['sitenamedescription'] = 'Websitenavnet der vises på forskellige steder rundt omkring på websitet og i e-mails sendt fra websitet';
$string['siteoptionspagedescription'] = 'Her kan du indstille globale indstillinger, der vil fungere som standard over hele websitet. <BR> Bemærk: Indstillinger, der er slået fra, bliver tilsidesat af din config.php fil.';
$string['siteoptionsset'] = 'Websiteindstillingerne er blevet opdateret.';
$string['sitethemedescription'] = 'Standardtemaet for denne website';
$string['smallviewheaders'] = 'Vis lille visnings side overskrift'; // Skal kontekst checkes!
$string['smallviewheadersdescription'] = 'Hvis slået til vil en lille overskrift og webside navigationsblok blive vist når man ser på eller redigerer visninger.';
$string['spamhaus'] = 'Slå Spamhaus URL blacklist til';
$string['spamhausdescription'] = 'Hvis slået til, vil URL adresser blive undersøgt op imod Spamhaus DNSBL';
$string['surbl'] = 'Slå SURBL URL blacklist til';
$string['surbldescription'] = 'Hvis slået til, vil URL adresser blive undersøgt op imod SURBL DNSBL';
$string['tagssideblockmaxtags'] = 'Maksimalt antal tags i Tag-sky';
$string['tagssideblockmaxtagsdescription'] = 'Standardantallet af tags vist i Tag-skyer';
$string['trustedsites'] = 'Godkendte websites';
$string['updatesiteoptions'] = 'Opdater websiteindstilllinger';
$string['usersallowedmultipleinstitutions'] = 'Brugere kan være i flere institutioner';
$string['usersallowedmultipleinstitutionsdescription'] = 'Hvis slået til kan brugerne være medlem af flere institutioner på én gang';
$string['userscanchooseviewthemes'] = 'Brugere kan vælge visningstemaer';
$string['userscanchooseviewthemesdescription'] = 'Hvis slået til kan brugerne vælge et tema når de redigerer en visning.  Visningen vil blive vist til andre med det valgte tema.';
$string['userscanhiderealnames'] = 'Brugere kan skjule deres rigtige navn';
$string['userscanhiderealnamesdescription'] = 'Hvis slået til kan brugere der har lavet et vist navn vælge kun at kunne søges på deres viste navn og vil ikke blive fundet i søgninger på deres rigtige navn.  (I website administrations delen af websitet kan brugere altid findes på deres rigtige navn.)';
$string['usersseenewthemeonlogin'] = 'Andre brugere vil se det nye tema næste gang de logger ind.';
$string['viruschecking'] = 'Virusscanning';
$string['viruscheckingdescription'] = 'Hvis slået til vil alle uploadede filer bliver virusscannet med ClamAV';
$string['whocancreategroups'] = 'Hvem kan oprette grupper';
$string['whocancreategroupsdescription'] = 'Hvilke brugere er i stand til at oprette nye grupper';
$string['whocancreatepublicgroups'] = 'Hvem kan lave offentlige grupper';
$string['whocancreatepublicgroupsdescription'] = 'Hvilke brugere er i stand til at oprette nye grupper, der kan ses af offentligheden';

// Site content
$string['about']               = 'Om';
$string['discardpageedits']    = 'Fortryd dine ændringer på denne side?';
$string['editsitepagespagedescription'] = 'Her kan du redigere indholdet af nogle sider på websitet, inklusiv hjemmesiden (for brugere der er logget ind og ud separat) og siderne linket til fodnoten.';
$string['home']                = 'Hjem';
$string['loadsitepagefailed']  = 'Indlæsning af websitet mislykkedes';
$string['loggedouthome']       = 'Logget ud hjemmeside'; // Skal kontekst checkes
$string['pagename']            = 'Sidenavn';
$string['pagesaved']           = 'Side gemt';
$string['pagetext']            = 'Sidetekst';
$string['privacy']             = 'Fortrolighedserklæring';
$string['savechanges']         = 'Gem ændringer';
$string['savefailed']          = 'Ændringerne blev ikke gemt';
$string['sitepageloaded']      = 'Websiden er blevet indlæst';
$string['termsandconditions']  = 'Vilkår og betingelser';
$string['uploadcopyright']     = 'Upload ophavsretserklæring';

// Links and resources menu editor
$string['sitefile']            = 'Website fil';
$string['adminpublicdirname']  = 'public';  // Name of the directory in which to store public admin files (og dermed ikke rørt!)
$string['adminpublicdirdescription'] = 'Filer som brugere der ikke er logget ind kan tilgå';
$string['badmenuitemtype']     = 'Ukendt menupunkt type';
$string['confirmdeletemenuitem'] = 'Er du sikker på at du vil slette dette menupunkt?';
$string['deletingmenuitem']    = 'Sletter menupunkt';
$string['deletefailed']        = 'Sletning af menupunkt mislykkedes';
$string['externallink']        = 'Eksternt link';
$string['editlinksandresources'] = 'Rediger links or ressourcer';
$string['linkedto']            = 'Linker til:';
$string['linksandresourcesmenu'] = 'Links og ressourcer menu';
$string['linksandresourcesmenupagedescription'] = 'Links og ressourcer menuen vises for alle brugere på de fleste sider. Du kan tilføje links til andre websider og til filer uploadet til %sWebside filer%s sektionen.';
$string['loadingmenuitems']    = 'Indlæser genstande';
$string['loadmenuitemsfailed'] = 'Indlæsning af genstande mislykkedes';
$string['loggedinmenu']        = 'Links og ressourcer for brugere der er logget ind';
$string['loggedoutmenu']       = 'Links og ressourcer for offentligheden';
$string['menuitemdeleted']     = 'Menupunkt slettet';
$string['menuitemsaved']       = 'Menupunkt gemt';
$string['menuitemsloaded']     = 'Menupunkt indlæst';
$string['name']                = 'Navn';
$string['nositefiles']         = 'Ingen webside filer tilgængelige';
$string['public']              = 'public'; // Lader lige den stå, den ser lidt system-agtig ud.
$string['savingmenuitem']      = 'Saving menupunkt';
$string['type']                = 'Type';
$string['footermenu']          = 'Fodnote menu';
$string['footermenudescription'] = 'Slå links til og fra i fodnoten.';
$string['footerupdated']       = 'Fodnote opdateret';

// Admin Files
$string['adminfilespagedescription'] = 'Her kan du uploade filer, der kan inkluderes i %sLinks og ressoucer menuen%s. Filer i home mappen kan tilføjes til menuen for brugere der er logget ind mens filer i public mappen kan tilføjes til den offentlige menu.';

// Networking options
$string['networkingextensionsmissing'] = 'Beklager, du kan ikke konfigurere Maharas networking indstillinger, da din PHP installation mangler en eller flere påkrævede udvidelser:';
$string['publickey'] = 'Offentlig nøgle';
$string['publickeydescription2'] = 'Denne offentlige nøgle bliver automatisk genereret og skifter efter der er gået %s dage';
$string['publickeyexpires'] = 'Offentlig nøgle udløbet';
$string['enablenetworkingdescription'] = 'Tillader at din Mahara server kan kommunikere med servere der kører Moodle og andre programmer';
$string['enablenetworking'] = 'Slå networking til';
$string['networkingenabled'] = 'Networking er slået til. ';
$string['networkingdisabled'] = 'Networking er slået fra. ';
$string['networkingpagedescription'] = 'Maharas networking funktioner tilleder det at kommunikere med Mahara eller Moodle websider, der kører på samme eller en anden maskine. Hvis networking er slået til kan du bruge det til at konfigurere single-sign-on for brugere, der logger ind i enten Moodle ellerr Mahara.';
$string['networkingunchanged'] = 'Netværksindstillinger blev ikke ændret';
$string['promiscuousmode'] = 'Auto-registrér alle hosts';
$string['promiscuousmodedisabled'] = 'Auto-registrér er slået fra. ';
$string['promiscuousmodeenabled'] = 'Auto-registrér er slået til. ';
$string['promiscuousmodedescription'] = 'Lav en istitutionsoptegnelse for envher host der er forbundet med dig og tillad dens brugere at logge ind i Mahara';
$string['wwwroot'] = 'WWW Root';
$string['wwwrootdescription'] = 'Dette er den URL som dine brugere kan tilgå denne Mahara installation på og som bliver SSL nøglerne blivere genereret til';
$string['proxysettings'] = 'Proxyindstillinger';
$string['proxyaddress'] = 'Proxyadresse';
$string['proxyaddressdescription'] = 'Hvis din webside bruger en proxy server til at tilgå internettet, angiv proxyserverne i <em>hostname:portnumber</em> format';
$string['proxyaddressset'] = 'Proxyadresse indstillet';
$string['proxyauthmodel'] = 'Proxygodkendelses model';
$string['proxyauthmodeldescription'] = 'Vælg din proxygodkendelsesmodel, hvis det er nødvendigt';
$string['proxyauthmodelset'] = 'Proxy godkendelses model indstillet';
$string['proxyauthcredentials'] = 'Proxy oplysninger';
$string['proxyauthcredentialsdescription'] = 'Indtast de oplysninger der kræves for at din proxy kan godkende din web server i <em>username:password</em> format';
$string['proxyauthcredntialsset'] = 'Proxygodkendelses oplysninger indstillet';


// Upload CSV and CSV errors
$string['csvfile'] = 'CSV fil';
$string['emailusersaboutnewaccount'] = 'Send e-mail til brugere om denne konto?';
$string['emailusersaboutnewaccountdescription'] = 'Om en e-mail skal sendes til brugerne for at fortælle dem om deres nye kontooplysninger';
$string['forceuserstochangepassword'] = 'Kræv nyt kodeord?';
$string['forceuserstochangepassworddescription'] = 'Om brugere skal tvinges til at skifte deres kodeord når de logger på første gang';
$string['uploadcsvinstitution'] = 'Institutions og godkendelsesmetode for nye brugere'; // Det gav ikke engang mening for mig på engelsk, så... kontekst?
$string['configureauthplugin'] = 'Du skal indstille et godkendelsesplugin før du kan tilføje brugere';
$string['csvfiledescription'] = 'Filen med brugere, der skal tilføjes';
$string['csverroremptyfile'] = 'CSV filen er tom.';
$string['invalidfilename'] = 'Filen "%s" eksisterer ikke';
$string['uploadcsverrorinvalidfieldname'] = 'Feltnavet "%s" er ikke gyldigt, eller du har flere felter end din header række angiver';
$string['uploadcsverrorrequiredfieldnotspecified'] = 'Et påkrævet felt "%s" er ikke specificeret i formateringslinjen';
$string['uploadcsverrornorecords'] = 'Det ser ikke ud til at filen indeholder nogle optegnelse (men headeren ser fin ud)';
$string['uploadcsverrorunspecifiedproblem'] = 'Optegnelserne i din CSV fil kunne ikke indsættes af en eller anden grund. Hvis din fil er i det korrekte format er dette en programfejl og du bør <a href="https://eduforge.org/tracker/?func=add&group_id=176&atid=739">lave en fejlrapport</a>, hvor du vedhæfter CSV filen (huske at rense den for kodeord!) og, hvis muligt, fejlrapporterings-logfilen.';
$string['uploadcsverrorinvalidemail'] = 'Fejl på linje %s af din fil: E-mail-adressen for denne bruger er ikke i den korrekte form';
$string['uploadcsverrorincorrectnumberoffields'] = 'Fejl på linje %s af din fil: Denne linje har ikke det korrekte antal felter';
$string['uploadcsverrorinvalidpassword'] = 'Fejl på linje %s af din fil: Kodeordet for denne bruger er ikke i den korrekte form';
$string['uploadcsverrorinvalidusername'] = 'Fejl på linje %s af din fil: Brugernavnet for denne bruger er ikke i den korrekte form';
$string['uploadcsverrormandatoryfieldnotspecified'] = 'Linje %s af filen mangler det påkrævede "%s" felt';
$string['uploadcsverroruseralreadyexists'] = 'Linje %s af filen angiver brugernavnet "%s", der allerede eksisterer';
$string['uploadcsverroremailaddresstaken'] = 'Linje %s af filen angiver e-mail-adressen "%s", der allerede er i brug af en anden bruger';
$string['uploadcsvpagedescription2'] = '<p>Du kan bruge denne funktion til at uploade nye brugere via en <acronym title="Comma Separated Values">CSV</acronym> fil.</p>
   
<p>Den første række af din CSV fil bør vise formatet af din CSV data. Den kan for eksempel se sådan ud:</p>

<pre>username,password,email,firstname,lastname,studentid</pre>

<p>Denne række skal inkludere <tt>username</tt>, <tt>password</tt>, <tt>email</tt>, <tt>firstname</tt> og <tt>lastname</tt> felterne. Den skal også indeholde de felter, som du kræver at alle brugere skal udfylde, og ethvert felt der er låst for den institution som du uploader brugere for. Du kan <a href="%s">konfigurere de påkrævede felter</a> for alle institutioner, eller <a href="%s">konfigure de låste felter for hver institution</a>.</p>

<p>Din CSV fil kan inkludere alle de profil felter som du kan ønske. Den komplette liste af felter er:</p>

%s';
$string['uploadcsvpagedescription2institutionaladmin'] = '<p>Du kan bruge denne funktion til at uploade nye brugere via en <acronym title="Comma Separated Values">CSV</acronym> fil.</p>

<p>Den første række af din CSV fil bør vise formatet af din CSV data. Den kan for eksempel se sådan ud:</p>

<pre>username,password,email,firstname,lastname,studentid</pre>

<p>Denne række skal inkludere <tt>username</tt>, <tt>password</tt>, <tt>email</tt>, <tt>firstname</tt> og <tt>lastname</tt> felterne. Det skal også inkludere alle felter som webside administratoren har krævet, og alle felter, der er lås for din institution. Du kan <a href="%s">konfigurere de låste felter</a> for de institutioner som du står for.</p>

<p>Din CSV fil kan inkludere alle de profil felter som du kan ønske. Den komplette liste af felter er:</p>

%s';
$string['uploadcsvsomeuserscouldnotbeemailed'] = 'Der kunne ikke sendes en e-mail til nogle af brugerne. Deres e-mail-adresser kan være ugyldige, eller serveren som Mahara er installeret på kan muligvis ikke være sat op til at sende e-mails. Serverfejl loggen har flere detaljer. Indtil videre, så kan du eventualt kontakte disse personer manuelt:';
$string['uploadcsvusersaddedsuccessfully'] = 'Brugerne i filen er blevet tilføjet';
$string['uploadcsvfailedusersexceedmaxallowed'] = 'Ingen brugere er blevet tilføjet, da der er for mange brugere i din fil.  Antallet af brugere i institutionen ville have overskredet det maksimale antal brugere i en istitution.';

// Bulk Leap2A import
$string['bulkleap2aimport'] = 'Importer brugere fra Leap2A filer';
$string['bulkleap2aimportdescription'] = '<p>Du kan masseinportere brugere fra en samling af Leap2A filer på din server.  Du skal angive en zip fil i serverens filsystem, der indeholder alle Leap2A zip filerne, og en enkelt CSV fil der hedder usernames.csv, for at tilpasse brugernavnene til dine filnavne.</p>
<p>usernames.csv skal se nogenlunde sådan ud:</p>
<pre>
&nbsp;&nbsp;bob,mahara-export-leap-user8-1265165366.zip<br>
&nbsp;&nbsp;nigel,mahara-export-leap-user1-1266458159.zip
</pre>
<p>Hvor mahara-export-leap-user8-1265165366.zip og mahara-export-leap-user1-1266458159.zip er filer i en undermappe kaldet users.</p>
<p>Denne zip fil burde normalt kunne genereres med masseeksport funktionen i Mahara.</p>
<p>Vær tålmodig, hvis du importere mange brugere.  Importeringsprocessen kan tage lang tid.</p>';
$string['importfile'] = 'Masseeksport fil';
$string['importfilemissinglisting'] = 'Masseeksport filen mangler en fil kaldet usernames.csv. Brugte du Maharas masseeksport funktion til at eksportere disse brugere?';
$string['importfilenotafile'] = 'Fejl under insendelse: filen blev ikke genkendt';
$string['importfilenotreadable'] = 'Fejl under indseldelse: Filen kunne ikke læses';
$string['bulkleap2aimportfiledescription'] = 'Zip filen på din server med alle de eksporterede brugere(i Leap2A format) sammen med en CSV liste af brugernavne';
$string['importednuserssuccessfully'] = 'Importerede %d af %d brugere';
$string['Import'] = 'Importer';
$string['bulkimportdirdoesntexist'] = 'Mappen %s eksisterer ikke';
$string['unabletoreadbulkimportdir'] = 'Mappen %s kunne ikke læses';
$string['unabletoreadcsvfile'] = 'Kunne ikke læse CSV filen %s';
$string['importfilenotreadable'] = 'Kunne ikke læse Leap2A filen %s';
$string['importfileisnotazipfile'] = 'Importeringsfilen %s blev ikke genkendt som en zip fil';
$string['unzipfailed'] = 'Kunne ikke pakke Leap2A filen %s ud. Se fejlrapporten for mere information.';
$string['importfailedfornusers'] = 'Importering mislykkedes med %d af %d brugere';
$string['invalidlistingfile'] = 'Ugyldig brugernavnsliste. Brugte du Maharas masseeksport funktion til at eksportere disse brugere?';

// Admin Users
$string['adminuserspagedescription'] = '<p>Her kan du vælge hvilke brugere, der er administratorer for denne webside. De nuværende administratorer står skrevet til højre og potentielle administratorer til venstre.</p><p>Systemet skal have mindst én administrator.</p>';
$string['institutionadminuserspagedescription'] = 'Her kan du vælge hvilke brugere der er administratorer for institutionen. De nuværende administratorer står skrevet til højre og potentielle administratorer til venstre.';
$string['potentialadmins'] = 'Potentielle admins';
$string['currentadmins'] = 'Nuværende admins';
$string['adminusersupdated'] = 'Administrator brugere opdateret';

// Staff Users
$string['staffuserspagedescription'] = 'Her kan du vælge hvilke brugere der er personale på dette site. Det nuværende personale står skrevet til højre og potentielt personale til venstre.';
$string['institutionstaffuserspagedescription'] = 'Her kan du vælge hvilke brugere der er personale i denne institution. Det nuværende personale står skrevet til højre og potentielt personale til venstre.';
$string['potentialstaff'] = 'Potentielt personale';
$string['currentstaff'] = 'Nuværende personale';
$string['staffusersupdated'] = 'Personale brugere opdateret';

// Admin Notifications

// Suspended Users
$string['deleteusers'] = 'Slet brugere';
$string['deleteuser'] = 'Slet bruger';
$string['confirmdeleteusers'] = 'Er du sikker på at du vil slette de valgte brugere?';
$string['exportingnotsupportedyet'] = 'Eksport af brugerprofiler er ikke understøttet endnu';
$string['exportuserprofiles'] = 'Eksporter brugerprofiler';
$string['nousersselected'] = 'Ingen brugere valgt';
$string['suspenduser'] = 'Suspender bruger';
$string['suspendedusers'] = 'Suspender brugere';
$string['suspensionreason'] = 'Grund til suspension';
$string['errorwhilesuspending'] = 'Der skete en fejl i suspenderingsprocessen';
$string['suspendedusersdescription'] = 'Suspender eller genaktiver brugeres adgang til websiden';
$string['unsuspendusers'] = 'Afsuspender brugere'; // Kan man sige det?
$string['usersdeletedsuccessfully'] = 'Brugerne er blevet slettet';
$string['usersunsuspendedsuccessfully'] = 'Brugerne er blevet afsuspenderet';
$string['suspendingadmin'] = 'Suspenderer administrator';
$string['usersuspended'] = 'Bruger suspenderet';
$string['userunsuspended'] = 'Bruger afsuspenderet';

// User account settings
$string['accountsettings'] = 'Kontoindstillinger';
$string['siteaccountsettings'] = 'Kontoindstillinger for websiden';
$string['resetpassword'] = 'Nulstil kodeord';
$string['resetpassworddescription'] = 'Hvis du skriver tekst her, vil det erstatte brugerens nuværende kodeord.';
$string['forcepasswordchange'] = 'Kræv nyt kodeord ved næste login';
$string['forcepasswordchangedescription'] = 'Brugeren vil blive bedt om at skifte kodeord ved næste log ind.';
$string['sitestaff'] = 'Webside personale';
$string['siteadmins'] = 'Webside administratorer';
$string['siteadmin'] = 'Webside administrator';
$string['accountexpiry'] = 'Konto udløber';
$string['accountexpirydescription'] = 'Datoen hvor brugerens login automatisk bliver slået fra.';
$string['suspended'] = 'Suspenderet';
$string['suspendedreason'] = 'Årsag til suspendering';
$string['suspendedreasondescription'] = 'Den tekst der vil blive vist når brugeren forsøger at logge ind næste gang.';
$string['unsuspenduser'] = 'Afsuspender bruger';
$string['thisuserissuspended'] = 'Denne bruger er bleet suspenderet';
$string['suspendedby'] = 'Denne bruger er bleet suspenderet af %s';
$string['deleteuser'] = 'Slet bruger';
$string['userdeletedsuccessfully'] = 'Brugeren er blevet slettet';
$string['confirmdeleteuser'] = 'Er du sikker på at du vil slette denne bruger?';
$string['filequota'] = 'Filkvote (MB)';
$string['filequotadescription'] = 'Den samlede plads tilgængelig i brugerens filområde.';
$string['addusertoinstitution'] = 'Tilføj bruger til institution';
$string['removeuserfrominstitution'] = 'Fjern bruger fra denne institution';
$string['confirmremoveuserfrominstitution'] = 'Er du sikker på at du vil fjerne brugeren fra denne institution?';
$string['usereditdescription'] = 'Her kan du se og ændre detaljer vedrørende denne brugerkonto. Nedenfor kan du også <a href="#suspend">suspendere eller slette kontoen</a>, eller ændre indstillingerne for brugeren i de <a href="#institutions">institutioner han er i</a>.';
$string['suspenddeleteuser'] = 'Suspender/slet bruger';
$string['suspenddeleteuserdescription'] = 'Her kan du suspendere eller slette en brugerkonto. Suspenderede brugere kan ikke logge ind før end suspenderingen ophæves. Bemærk at en suspendering kan ophæves, mens sletning af kontoen er permanent og <strong>ikke</strong> kan ophæves.';
$string['deleteusernote'] = 'Bemærk at denne handling <strong>ikke kan ophæves</strong>.';
$string['youcannotadministerthisuser'] = 'Du kan ikke administere denne bruger';

// Add User
$string['adduser'] = 'Tilføj bruger';
$string['adduserdescription'] = 'Opret en ny bruger';
$string['basicinformationforthisuser'] = 'Grundlæggende information om denne bruger.';
$string['clickthebuttontocreatetheuser'] = 'Klik på knappen for at oprette brugeren.';
$string['createnewuserfromscratch'] = 'Opret ny bruger fra bunden af';
$string['createuser'] = 'Opret bruger';
$string['failedtoobtainuploadedleapfile'] = 'Kunne ikke få fat i den uploadede Leap2A fil';
$string['failedtounzipleap2afile'] = 'Kunne ikke pakke Leap2A filen ud. Se fejlrapporten for mere information';
$string['fileisnotaziporxmlfile'] = 'Denne fil kunne ikke genkendes som en zip eller XML fil';
$string['howdoyouwanttocreatethisuser'] = 'Hvordan vil du oprette denne bruger?';
$string['leap2aimportfailed'] = '<p><strong>Beklager - Import af Leap2A filen mislykkedes.</strong></p><p>Dette kunne være fordi du ikke havde valgt en gyldig Leap2A fil i din upload, eller fordi versionen af din Leap2A fil ikke er understøttet af denne Mahara version. Alternativt kan der være en fejl i Mahara, der får din fil til at mislykkes, opå trods af at den er korrekt.</p><p>Prøv <a href="add.php">at gå tilbage og prøve igen</a>, og hvis problemet stadig er der, kan det være at du vil besøge det officielle <a href="http://mahara.org/forums/">Mahara Forum</a> for t spørge om hjælp. Vær klar over at du nok bliver bedt om at sende en kopi af din fil!</p>';
$string['newuseremailnotsent'] = 'Kunne ikke sende velkomst e-mail til ny bruger.';
$string['newusercreated'] = 'Ny brugerkonto oprettet';
$string['noleap2axmlfiledetected'] = 'Kunne ikke finde en leap2a.xml fil - undersøg din eksportfil igen';
$string['Or...'] = 'Eller...'; // Hvor mon den bruges?
$string['userwillreceiveemailandhastochangepassword'] = 'De vil modtage en e-mail, der fortæller dem om deres nye kontooplysninger. Ved deres første login vil de blive krævet at skifte deres kodeord.';
$string['uploadleap2afile'] = 'Upload Leap2A fil';

$string['usercreationmethod'] = '1 - Metode til brugeoprettelse';
$string['basicdetails'] = '2 - Grundlæggende detaljer';
$string['create'] = '3 - Opret';

// Login as
$string['loginas'] = 'Login som';
$string['loginasuser'] = 'Login som %s';
$string['becomeadminagain'] = 'Bliv til %s igen';
// Login-as exceptions
$string['loginasdenied'] = 'Forsøgte at logge ind som en anden bruger uden at have tilladelse';
$string['loginastwice'] = 'Forsøgte at logge ind som en anden bruger mens du allerede var logget ind som en anden bruger';
$string['loginasrestorenodata'] = 'ingen brugerdata at genoprette';
$string['loginasoverridepasswordchange'] = 'Da du er forklædt som en anden bruger kan du vælge at %slogge ind alligevel%s og ignorere at der skal skiftes kodeord.';

// Institutions
$string['Add'] = 'Tilføj';
$string['admininstitutions'] = 'Administrer institutioner';
$string['adminauthorities'] = 'Administrer autoriteter';
$string['addinstitution'] = 'Tilføj institution';
$string['authplugin'] = 'Godkendelsesplugin';
$string['deleteinstitution'] = 'Slet institution';
$string['deleteinstitutionconfirm'] = 'Er du helt sikker på at du vil slette denne institution?';
$string['institutionaddedsuccessfully2'] = 'Institutionen er blevet tilføjet';
$string['institutiondeletedsuccessfully'] = 'Institutionen er blevet fjernet';
$string['noauthpluginforinstitution'] = 'Din webside administrator har ikke opsat et godkendelsesplugin for denne institution.';
$string['adminnoauthpluginforinstitution'] = 'Sæt venligst et godkendelsesplugin op for denne institution.';
$string['institutionname'] = 'Institutionsnavn';
$string['institutionnamealreadytaken'] = 'Dete institutionsnavn er allerede taget';
$string['institutiondisplayname'] = 'Vist institutionsnavn';
$string['institutionexpiry'] = 'Udløbsdato for institutionen';
$string['institutionexpirydescription'] = 'Datoen hvor denne institutions medlemskab af %s bliver suspenderet.';
$string['institutionupdatedsuccessfully'] = 'Institutionen er blevet updateret.';
$string['registrationallowed'] = 'Registration tilladt?';
$string['registrationalloweddescription2'] = 'Om brugere kan registere sig på din webside for denne institution gennem registrationsformularen.  Hvis registration er slået fra, kan ikke-medlemmer ikke anmode om medlemskab af institutionen, og medlemmer kan ikke forlade institutionen eller slette deres brugerkonti frivilligt.';
$string['defaultmembershipperiod'] = 'Standard medlemskabsperidode';
$string['defaultmembershipperioddescription'] = 'Hvor længe nye medlemmer bliver ved med at være en del af institutionen';

$string['authenticatedby'] = 'Godkendelsesmetode';
$string['authenticatedbydescription'] = 'Hvordan denne bruger godkendes af Mahara';
$string['remoteusername'] = 'Brugernavn til ekstern godkendelse';
$string['remoteusernamedescription'] = 'Hvis denne bruger logger ind på %s fra en anden webside med et XMLRPC godkendelses plugin, er dette det brugernavn som indentificerer brugeren på den anden webside';
$string['institutionsettings'] = 'Institutionsindstillinger';
$string['institutionsettingsdescription'] = 'Her kan du ændre indstillinger vedrørende denne brugers medlemskab af institutioner i systemet.';
$string['changeinstitution'] = 'Skift institution';
$string['institutionstaff'] = 'Institutionspersonalte';
$string['institutionadmins'] = 'Institutionsadministratorer';
$string['institutionadmin'] = 'Institution admin';
$string['institutionadministrator'] = 'Institutionsadministrator';
$string['institutionadmindescription'] = 'Hvis slået til, kan denne bruger administere alle brugere i denne institution.';
$string['settingsfor'] = 'Indstillinger for:';
$string['institutionadministration'] = 'Institutionsadministration';
$string['institutionmembers'] = 'Institutionsmedlemmer';
$string['notadminforinstitution'] = 'Du er ikke en administrator for den institution';
$string['institutionmemberspagedescription'] = 'På denne side kan du se hvilke brugere, der har anmodet om medlemskab i din institution og tilføje dem som medlemmer.  Du kan oså fjerne brugere fra din institution og invitere brugere til at blive medlemmer.';

$string['institutionusersinstructionsrequesters'] = 'Brugerlisten til venstre viser alle de brugere, der har anmodet om medlemskab i din institution.  Du kan bruge søgefeltet til at reducere antallet af viste brugere.  Hvis du vil tilføje brugere til institutionen eller afvise deres anmodning skal du flytte nogle brugere over i højre side ved at vælge en eller flere brugere og kille på pilen der peger til højre.  "Tilføj medlemmer" knappen tilføjer alle brugere til højre til institutionen.  "Avis anmodninger" knappen fjerne medlemskabsanmodningerne fra brugerne til højre.';
$string['institutionusersinstructionsnonmembers'] = 'Brugerlisten til venstre viser alle de brugere, der endnu ikke er medlemmer af din institution.  Du kan bruge søgefeltet til at reducere antallet af viste brugere.  For at invitere brugere til at blive medlemmer af institutionen skal du flytte nogle brugere over i højre side ved at vælge en eller flere brugere og kille på pilen der peger til højre.  "Inviter brugere" knappen vil sende invitationer til alle brugere til højre.  Disse brugere vil ikke være en del af din institution før de har accepteret invtationen.';
$string['institutionusersinstructionsmembers'] = 'Brugerlisten til venstre viser alle de brugere, der er medlemmer af din institution.  Du kan bruge søgefeltet til at reducere antallet af viste brugere.  For at fjerne brugere fra institutionen skal du flytte nogle brugere over i højre side ved at vælge en eller flere brugere og kille på pilen der peger til højre.  "Fjren brugere" kanppen vil fjerne alle brugerne til højre fra institutionen.  Brugerne til venstre vil stadig være en del af institutionen.';

$string['editmembers'] = 'Rediger medlemmer';
$string['editstaff'] = 'Rediger personale';
$string['editadmins'] = 'Rediger administratorer';
$string['membershipexpiry'] = 'Medlemskab udløber';
$string['membershipexpirydescription'] = 'Den dato hvor brugeren automatisk bliver fjernet fra institutionen.';
$string['studentid'] = 'Studienummer';
$string['institutionstudentiddescription'] = 'En valgfri identifikator, der er specifik for den enkelte institution.  Dette felt kan ikke redigeres af brugeren.';

$string['userstodisplay'] = 'Brugere der skal vises:';
$string['institutionusersrequesters'] = 'Folk der har anmodet om medlemskab af institutionen';
$string['institutionusersnonmembers'] = 'Folk der ikke har anmodet om medlesskab endnu';
$string['institutionusersmembers'] = 'Folk der allerde er medlemmer af institutionen';

$string['addnewmembers'] = 'Tilføj nye medlemmer';
$string['usersrequested'] = 'Brugere der har andmodet om medlemskab';
$string['userstobeadded'] = 'Brugere der skal tilføjes som medlemmer';
$string['userstoaddorreject'] = 'Brugere der skal tilføjes/afvises';
$string['addmembers'] = 'Tilføj medlemmer';
$string['inviteuserstojoin'] = 'Inviter brugere til medlemskab a insitutionen';
$string['Non-members'] = 'Ikke-medlemmer';
$string['userstobeinvited'] = 'Brugere der skal inviteres';
$string['inviteusers'] = 'Inviter brugere';
$string['removeusersfrominstitution'] = 'Fjern brugere fra institutionen';
$string['currentmembers'] = 'Nuværende medlemmer';
$string['userstoberemoved'] = 'Brugere der skal fjernes';
$string['removeusers'] = 'Fjern brugere';
$string['declinerequests'] = 'Afvis anmodninger';
$string['nousersupdated'] = 'Ingen brugere blev opdateret';

$string['institutionusersupdated_addUserAsMember'] = 'Brugere tilføjet';
$string['institutionusersupdated_declineRequestFromUser'] = 'Anmodninger afvist';
$string['institutionusersupdated_removeMembers'] = 'Brugere fjernet';
$string['institutionusersupdated_inviteUser'] = 'Invitationer sendt';

$string['maxuseraccounts'] = 'Maksimalt antal tilladte brugerkonti';
$string['maxuseraccountsdescription'] = 'Det maksimale antal brugerkonti der kan være en det af institutionen.  Hvis der ikke er en grænse bør dette felt efterlades tomt.';
$string['institutionmaxusersexceeded'] = 'Denne institution er fuld og du er nødt til at forøge det tillandte antal brugere i denne institution før denne brger kan blive tilføjet.';
$string['institutionuserserrortoomanyusers'] = 'Brugerne blev ikke tilføjet.  Antallet af medlemmer kan ikke overskride det maksimale antal brugere for denne institution.  Du kan tilføje færre brugere, fjerne nogle brugere fra instittionen eller bede en webside administrator om at øge det maksimale antal brugere.';
$string['institutionuserserrortoomanyinvites'] = 'Dine invitationer blev ikke sendt.  Antallet af eksisterende medlemmer plus antallet af åbne invitationer kan ikke overskride det maksimale antal brugere for denne institution.  Du kan invitere færre brugere, fjerne nogle brugere fra instittionen eller bede en webside administrator om at øge det maksimale antal brugere.';

$string['Members'] = 'Medlemmer';
$string['Maximum'] = 'Maksimum';
$string['Staff'] = 'Personale';
$string['Admins'] = 'Administratorer';

$string['noinstitutions'] = 'Ingen institutioner';
$string['noinstitutionsdescription'] = 'Hvis du ønsker a brugerne skal være en del af en institution, bør du først oprette institutionen.';

$string['Lockedfields'] = 'Låste felter';
$string['disabledlockedfieldhelp'] = 'Bemærk: Hvis du ikke er i stand til at tilføje eller fjene flueben i et profil felt er det fordi at de er låst i institutionsindstillingerne for "%s".  Disse profilfelter er låst på webside niveau og kan ikke åbnes her.';

// Suspend Institutions
$string['errorwhileunsuspending'] = 'Der skete en fejl under forsøget på af afsuspendere';
$string['institutionsuspended'] = 'Institution suspenderet';
$string['institutionunsuspended'] = 'Institution afsuspenderet';
$string['suspendedinstitution'] = 'SUSPENDERET';
$string['suspendinstitution'] = 'Suspender institution';
$string['suspendinstitutiondescription'] = 'Her kan du suspendere en institution. Brugere fra suspenderede institutioner vil ikke være i stand til at logge ind før institutionen bliver afsuspenderet.';
$string['suspendedinstitutionmessage'] = 'Denne institution er blevet suspenderet';
$string['unsuspendinstitution'] = 'Afsuspender institution';
$string['unsuspendinstitutiondescription'] = 'Her kan du afsuspendere en institution. Brugere fra suspenderede institutioner vil ikke være i stand til at logge ind før institutionen bliver afsuspenderet.<br /><strong>Advarsel:</strong> Hvis man afsuspenderer en institution uden at rette eller fjerne den udløbsdato kan det ende med at instutionen bliver gen-suspenderet dagligt.';
$string['unsuspendinstitutiondescription_top'] = '<em>Advarsel:</em> Hvis man afsuspenderer en institution uden at rette eller fjerne den udløbsdato kan det ende med at instutionen bliver gen-suspenderet dagligt.';
$string['unsuspendinstitutiondescription_top_instadmin'] = 'Brugere af suspenderede institutioner er ikke i stand til at logge ind. Kontakt webside administratoren for at afsuspendere institutionen.';

// Bulk Leap2A User export
$string['bulkexport'] = 'Eksporter brugere';
$string['bulkexportempty'] = 'Intet passende at eksportere. Undersøg listen af brugernavne igen.';
$string['bulkexportinstitution'] = 'Insitutionen hvorfra alle brugere skal eksporeres';
$string['bulkexporttitle'] = 'Eksporter brugere til Leap2A filer';
$string['bulkexportdescription'] = 'Vælg den institution der skal eksporteres <b>ELLER</b> angiv en liste af brugernavne:';
$string['bulkexportusernames'] = 'Brugernavne til eksport';
$string['bulkexportusernamesdescription'] = 'En liste over de brugere (et brugernavn pr. linje), som skal eksporteres sammen med deres data.';
$string['couldnotexportusers'] = 'De følgende brugere kunne ikke eksporteres: %s';
$string['exportingusername'] = 'Eksporterer \'%s\'...';

// Admin User Search
$string['Search'] = 'Søg';
$string['Institution'] = 'Institution';
$string['confirm'] = 'bekræft';
$string['invitedby'] = 'Inviteret af';
$string['requestto'] = 'Anmodning til';
$string['useradded'] = 'Bruger tilføjer';
$string['invitationsent'] = 'Invitation sendt';

// general stuff
$string['notificationssaved'] = 'Beskedindstillinger gemt';
$string['onlyshowingfirst'] = 'Viser kun de første';
$string['resultsof'] = 'resultater af';

$string['installed'] = 'Installeret';
$string['errors'] = 'Fejl';
$string['install'] = 'Installér';
$string['reinstall'] = 'Geninstallér';

// spam trap names
$string['None'] = 'Ingen';
$string['Simple'] = 'Simpel';
$string['Advanced'] = 'Avanceret';

//admin option fieldset legends
$string['sitesettingslegend'] = 'Websideindstillinger';
$string['usersettingslegend'] = 'Brugerindstillinger';
$string['groupsettingslegend'] = 'Gruppeindstillinger';
$string['searchsettingslegend'] = 'Søgeindstillinger';
$string['institutionsettingslegend'] = 'Institutionsindstillinger';
$string['accountsettingslegend'] = 'Kontoindstillinger';
$string['securitysettingslegend'] = 'Sikkerhedsindstillinger';
$string['generalsettingslegend'] = 'Generelle indstillinger';

$string['groupname'] = 'Gruppenavn';
$string['groupmembers'] = 'Medlemmer';
$string['groupadmins'] = 'Administratorer';
$string['grouptype'] = 'Gruppetype';
$string['groupvisible'] = 'Synlighed';
$string['groupmanage'] = 'Administrer';
$string['groupdelete'] = 'Slet';
$string['managegroupdescription'] = 'Brug formularen nedenfor til at tilføje og fjerne administratorer for denne gruppe.  Hvis du fjerne en gruppeadministratoer er de stadig medlem af gruppen.';

?>
