<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

// IMAP
$string['host'] = 'Hostnavn eller adresse';
$string['wwwroot'] = 'WWW root';

$string['port'] = 'Port nummer'; 
$string['protocol'] = 'Protokol';
$string['changepasswordurl'] = 'Skift kodeord URL';
$string['cannotremove']  = "Vi kan ikke fjerne dette godkendelsesplugin da det er de eneste \nplugin der findes til denne institution.";
$string['cannotremoveinuse']  = "Vi kan ikke fjerne dette godkendelsesplugin da det er i brug af nogle brugere.\nDu skal opdatere deres konto før du kan fjerne dette plugin.";
$string['saveinstitutiondetailsfirst'] = 'Gem venligst institutionsdetaljerne før du sætter godkendelsesplugins op.';

$string['editauthority'] = 'Rediger en autoritet'; // Hvad mon det er? Kontekst?
$string['addauthority']  = 'Tilføj en autoritet';

$string['updateuserinfoonlogin'] = 'Opdater brugerinfo ved login';
$string['updateuserinfoonlogindescription'] = 'Hent brugerinfo fra fjernserveren og opdater den lokale brugerinfo hver gang brugeren logger ind.';
$string['xmlrpcserverurl'] = 'XML-RPC server URL';
$string['ipaddress'] = 'IP adresse';
$string['shortname'] = 'Kort navn for din webside';
$string['name'] = 'Webside navn';
$string['nodataforinstance'] = 'Kunne ikke finde data til godkendelses instans ';
$string['authname'] = 'Autoritetsnavn';
$string['weautocreateusers'] = 'Vi auto-opretter brugere';
$string['theyautocreateusers'] = 'De auto-opretter brugere';
$string['parent'] = 'Overordnet autoritet';
$string['wessoout'] = 'Vi SSOer hos dem'; // Hvad...? "SSO out" må have en anden mening. Kontekst?
$string['weimportcontent'] = 'Vi importerer indhold';
$string['weimportcontentdescription'] = '(kun nogle applikationer)'; // applications kunne også være ansøgninger i denne kontekst.
$string['theyssoin'] = 'De SSOer hos os'; // Prøver lige den her opstilling.
$string['authloginmsg'] = "Indtast en besked, der skal vises når en bruger prøver at logge ind via Maharas login formular";
$string['application'] = 'Applikation'; // Kontekst
$string['cantretrievekey'] = 'Der skete en fejl under forsøget på at hente en public key fra fjernserveren.<br>Undersøg om Applikation og WWW Root felterne er korrekte og at netværksforbinderne er slået til på fjernserver værten.';
$string['ssodirection'] = 'SSO retning';

$string['errorcertificateinvalidwwwroot'] = 'Dette certifikat siger at det er til %s, men du prøver at bruge det til %s.';
$string['errorcouldnotgeneratenewsslkey'] = 'Kunne ikke generere en ny SSL nøgle. Er du sikker på at både openssl og PHP module for openssl er installert på denne maskine?';
$string['errnoauthinstances']   = 'Det ser ikke ud til at vi har godkendelsesplugin instanser sat op til værten på %s';
$string['errornotvalidsslcertificate'] = 'Dette er ikke et gyldigt SSL certifikat';
$string['errnoxmlrpcinstances'] = 'Det ser ikke ud til at vi har XMLRPC godkendelsesplugin instanser sat op til værten på %s';
$string['errnoxmlrpcwwwroot']   = 'Vi har ikke en optegnelse for værten på %s';
$string['errnoxmlrpcuser']      = "Vi var ikke i stand til at godkende dig denne gang. Mulige årsage kan være:

    * Din SSO session kan være udløbet. Gå tilbage til den anden applikation og klik linket for at logge ind i Mahara igen.
    * Du kan mangle tilladelse til at SSO til Mahara. Kontakt din administrator hvis du mener at du burde have lov.";

$string['unabletosigninviasso'] = 'Kune ikke logge ind via SSO';
$string['xmlrpccouldnotlogyouin'] = 'Beklager, vi kunne ikke logge dig ind :('; // Smileyen er den del af den originale tekst.
$string['xmlrpccouldnotlogyouindetail'] = 'Beklager, vi kan ikke logge dig ind i Mahara på dette tidspunkt. Prøv igen snart og hvis problemet stadig er der, så kontakt din administrator';

$string['requiredfields'] = 'Påkrævede profilfelter';
$string['requiredfieldsset'] = 'De påkrævede profilfelter er blevet indstillet';
$string['noauthpluginconfigoptions'] = 'Der er ingen opsætningsindstillinger forbundet med dette plugin';

$string['hostwwwrootinuse'] = 'WWW root er allerede i brug af en anden institution (%s)';

// Error messages for external authentication usernames
$string['duplicateremoteusername'] = 'Dette eksterne godkedelses brugernavn er allerede i brug af brugeren %s. Eksterne godkendelses brugernavne skal være unikke indenfor hver enkelt godkendeslesmetode.';
$string['duplicateremoteusernameformerror'] = 'Eksterne godkendelses brugernavne skal være unikke indenfor hver enkelt godkendeslesmetode.';
?>
