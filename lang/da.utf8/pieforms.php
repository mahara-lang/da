<?php

defined('INTERNAL') || die();

$string['element.bytes.bytes'] = 'Bytes';
$string['element.bytes.kilobytes'] = 'Kilobytes';
$string['element.bytes.megabytes'] = 'Megabytes';
$string['element.bytes.gigabytes'] = 'Gigabytes';
$string['element.bytes.invalidvalue'] = 'Værdi skal være et tal';

$string['element.calendar.invalidvalue'] = 'Forkert angivelse af dato/klokkeslæt';

$string['element.date.or'] = 'eller';
$string['element.date.monthnames'] = 'januar,februar,marts,april,maj,juni,juli,august,september,oktober,november,december';
$string['element.date.notspecified'] = 'Ikke angivet';

$string['element.expiry.days'] = 'Dage';
$string['element.expiry.weeks'] = 'Uger';
$string['element.expiry.months'] = 'Måneder';
$string['element.expiry.years'] = 'År';
$string['element.expiry.noenddate'] = 'Ingen slutdato';

$string['rule.before.before'] = 'Denne kan ikke være efter feltet "%s"';

$string['rule.email.email'] = 'E-mail adressen er invalid';

$string['rule.integer.integer'] = 'Dette felt skal være et heltal';

$string['rule.maxlength.maxlength'] = 'Dette felt må højest være %d karakterer lang';

$string['rule.minlength.minlength'] = 'Dette felt skal mindst være %d karakterer lang';

$string['rule.minvalue.minvalue'] = 'Denne værdi kan ikke være mindre end %d';

$string['rule.regex.regex'] = 'Dette felt er ikke i gyldig form';

$string['rule.required.required'] = 'Dette felt er påkrævet';

$string['rule.validateoptions.validateoptions'] = 'Denne option "%s" er ikke valid';
