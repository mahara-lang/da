<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['aboutdefaultcontent'] = '<h1>Om Mahara</h1>

<p>Oprettet i 2006, Mahara er resultatet af et foretageende finacieret af New Zealand\'s Tertiary Education Commission\'s e-learning Collaborative Development Fund (eCDF), der involverer Massey University, Auckland University of Technology, The Open Polytechnic of New Zealand and Victoria University of Wellington.</p>

<p>Mahara er et fuldt udviklet elektronisk portefølje, weblog, CV bygger og social netværks system,der forbinder brugere og skabe online fællesskaber. Mahara er designet til at give brugerne de værktøjer de skal bruge til at lave et personligt og professionelt lærings og udviklings miljø.</p>

<p>Med betydningnen "tænk" eller "tænkte" på Te Reo Maori, reflekterer navnet samarbejdspartnernes dedikation til at lave en bruger-centreret livslant lærings og udviklings applikation og den overbevisning at teknologiske løsninger ikke kan udvikles uden at man overvejer pædagogik og politik.</p>

<p>Mahara udgives frit som Open Source software (under GNU General Public License). Kort sagt betyder det at du har lov til at kopiere, bruge og redigere Mahara, under forusætninger at du:</p>

<ul>
    <li>gør kildekoden tilgængelig for andre; </li>
    <li>ikke ændrer den originale licens og copyright, og</li>
    <li>bruger samme licens på alle modifikationer og kopier af værket.</li>
</ul>

<p>Hvis du har spørgsmål om Mahara, er du velkommen til at <a href="contact.php">kontakte os</a>.</p>

<p><a href="http://mahara.org">http://mahara.org</a></p>';
$string['homedefaultcontent'] = '<h1>Velkommen til Mahara</h1>

<p>Mahara er et fuldt udviklet elektronisk portefølje, weblog, CV bygger og social netværks system, der forbinder brugere og skaber online fællesskaber. Mahara giver dig de værktøjer du skal bruge til at lave et personligt og professionelt lærings og udviklings miljø.</p>

<p>For mere information kan du læse <a href="about.php">Om</a> Mahara eller alternativt er du velkommen til at <a href="contact.php">kontakte os</a>.</p>';
$string['loggedouthomedefaultcontent'] = '<h1>Velkommen til Mahara</h1>

<p>Mahara er et fuldt udviklet elektronisk portefølje, weblog, CV bygger og social netværks system, der forbinder brugere og skaber online fællesskaber. Mahara giver dig de værktøjer du skal bruge til at lave et personligt og professionelt lærings og udviklings miljø.</p>

<p>For mere information kan du læse <a href="about.php">Om</a> Mahara eller alternativt er du velkommen til at <a href="contact.php">kontakte os</a>.</p>';
$string['privacydefaultcontent'] = '<h1>Beskyttelse af personlige oplysninger</h1>
    
<h2>Introduktion</h2>

<p>Vi er engageret i at beskytte dine personlige oplysninger og give brugerne et sikkert og funktionelt personligt lærings og udviklings miljø. Denne erklæring om personlige oplysinger gælder for Mahara websiden og omhandler dataindsamling og brug.</p>

<h2>Indsamling af personlig information</h2>

<p>Ved at registere ved Mahara vil du blive nødt til at give os en vis mængde personlige oplysninger.  Vi vil ikke afsløre noget af dine personlige oplysninger til andre personer eller organisationer uden din skriftlige tilladelse, med mindre vi har tilladelse eller er påkrævet i forhold til lovgivningen.</p>

<h2>Cookies</h2>

<p>For at bruge Mahara skal du have cookies slået til i din browser.  Bemærk at ingen personlig information er gemt i de cookies som Mahara bruger.</p>

<p>En cookie er en datafil der bliver oprettet på din computer af en webside server. Cookies er ikke programmer, spyware eller viruser og er ikke i stand til at udføre nogle handlinger alene.</p>

<h2>Hvordan vi bruger dine personlige oplysninger</h2>

<p>Vi bruger kun din personlige oplysninger til de formål som du havde til hensigt at bruge dem til.</p>

<p>Som Mahara brugere er I i stand til at vælge hvilke personlige oplysninger, der er tilgængelige for andre.  Som standard, hvis ikke det er en administrator, kursus opretter eller underviser,  kan en bruger kun se dit viste navn.  Dette inkluderer lister i en beskedrapport eller log over besøgende.</p>

<p>Vi kan også indsamle data om systembrug til statisktiske formål, men denne data kan ikke bruges til at identificere individuelle brugere med.</p>

<h2>Opbevaring og sikkerhed omkring dine personlige oplysninger</h2>

<p>Vi tager alle rimelige forholdsregler for at sikre at alle pesonlige oplysning, som vi har, ikke går tabt eller bliver udsat for misbrug, uautoriseret adgang, afsløring, eller ændringer.</p>

<p>For at hjælpe med at beskytte dine personlige oplysinger, unlad at afsløre dit brugernavn og kodeord til nogen udover webside administratoreren.</p>

<h2>Ændringer af erklæringen om personlige oplysinger</h2>

<p>Vi kan nogle gange lave justeringer af vores erklæring om personlige oplysinger for at tilpasse den til ændringer i systemet og som svar på brugerfeedback. Derfor anbefaler vi at du undersøger vores erklæring om personlige oplysinger hver gang du besøger siden.</p>

<h2>Kontakt</h2>

<p>Hvis du har nogle spørgsmål om denne erkæring eller mener at vi ikke har opfyldt det der står, så <a href="contact.php">kontakt os</a> og vi vil tage alle rimelige foranstaltninger for a løse problemet.</p>';
$string['termsandconditionsdefaultcontent'] = '<h1>Vilkår og betingelser</h1>

<p>Ved at bruge [Mahara] accepterer du de vilkår og betingelser der står nedenfor.</p>

<h2>Vores pligter</h2>

<p>[Mahara] webside administratorerne vil tage alle rimelige forholdsregler for at give alle brugere et sikkert og funkstionsdygtigt eketronisk portefølje system.  Hvis til enhver tid du føler at dine rettigheder som bruger er blevet misligholdt eller du har spørgsmål om det nedenstående, så <a href="contact.php">kontakt os</a> øjeblikkeligt.</p>

<p>[Mahara] vil nogle gange ikke være tilgængeligt i korte perisoder når vi implementerer nye system funktioner.  Vi vil gøre hvad vi kan for at give mindst 3 arbejdsdages advarsel før ethvert planlagt driftsstop.</p>

<p>Vi opfordrer dig til at anmelde anstødeligt materiale eller upassende opførsel til <a href="contact.php">webside administratoren</a> øjeblikkeligt.  Vi vil få undersøgt sagen så snart det er muligt.</p>

<p>Webside Administratorer kan få adgang til dit portefølje og dets indhold til hver en tid, men de vil undgå at gøre dette med mindre det er direkte nødvendigt for at yde support på din brug af [Mahara] eller i forbindelse med disse vilkår og betingelser.</p>

<h2>Dine pligter</h2>

<p> Erklæringen om <a href="privacy.php">beskyttelse af personlige oplysninger</a> skal ses som en udvidelse af disse vilkår og betingelser og læses af alle brugere.</p>

<p>Din [Mahara] konto vil udløbe efter et givent stykke tid eller et givent tidsrum af inaktivitet, der er indstillet af webside administratoren. Du vil få en e-mail påmindelse kort før at din konto udløber og vi anbefaler at du gemmer dit portefølje på din personlige computer på dette tidspunkt, så du kan bruge den senere.</p>

<p>Alle filer og indhold du uploader til [Mahara] er dækket af New Zealands copyright lovgivning.  Du er ansvarlig for at være sikker på at du har de passende tiladelser til at reproducere og udgive indhold, der ikke er dit eget.  Plagiat tilfælde vil blive ordnet efter de regler der er på din uddannelsesinstitution.</p>

<p>Du må ikke bruge dit portefølje til at opbevare eller vise anstødelig materiale.  Hvis webside administratoren motager anmeldelser om anstødeligt materiale i dit portefølje, vil din konto blive suspenderet og din andgang til [Mahara] vil blive låst indtil en undersøgse i forhold til reglerne for studerendes handlinger, eller lignende på din uddannelsesinstitution.  Hvis sådanne regler ikke findes vil sagen blive ført videre til den rette person i insitutionens organisation.</p>

<p>Hvis webside administratorer modtager anmeldelser af upassende opførsel fra din side af, i forbindelse med [Mahara], vil din konto blive suspenderet og din andgang til [Mahara] vil blive låst indtil en undersøgse i forhold til reglerne for studerendes handlinger, eller lignende på din uddannelsesinstitution.  Hvis sådanne regler ikke findes vil sagen blive ført videre til den rette person i insitutionens organisation.</p>

<p>Upassende opførsel inkluderer misbrug af systemet til at anmelde anstødeligt materiale, at bevidst uploade filer med virusindhold, at give anstødeligt eller overdreven feedback eller kommentarer på andre brugeres proteføljer og envher opførsel, der kan ses som irriterende eller anstødeligt af webside administratoren.</p>

<p>Enhver udefrakommende kontakt du får som resultat af personlige oplysninger du selv har gjort offentligt tilgængelige via dit portefølje er dit eget ansvar, dog bør enhver kænkende eller anstødelig opførsel fra andre brugere af systemet anmeldes til <a href="contact.php">webside administratoren</a> øjeblikkeligt.  Vi kan nogle gange lave justeringer af vores vilkår og betingelser for at tilpasse dem til ændringer i systemet og som svar på brugerfeedback. Derfor anbefaler vi at du undersøger vores vilkår og betingelser side hver gang du besøger siden.  Vi vil dog gøre brugerne opmærksomme på større ændringer af vilkårene og betingelserne via [Mahara] hjemmesiden.</p>';

$string['uploadcopyrightdefaultcontent'] = 'Ja: Filen jeg forsøger at uploade er min egen, eller jeg har direkte tilladelser til at reproducere og/eller distribuere denne.  Min brug af denne fil overtræder ingen lokal ophavsretslovgivning. Denne fil overholder også de vilkår og betingelser, der gælder for denne webside.';

?>
