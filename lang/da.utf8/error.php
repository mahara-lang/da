<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

// @todo<nigel>: most likely need much better descriptions here for these environment issues
$string['phpversion'] = 'Mahara kan ikke køre på PHP < %s. Opgrader din PHP version eller fly Mahara til en anden vært.';
$string['jsonextensionnotloaded'] = 'Din serveropsætning inkluderer ikke JSON udvidelsen. Mahara skal bruge denne for at sende data til og fra browseren. Undersøg om den er indlæst i php.ini eller installer den hvis den ikke allerede er installleret.';
$string['pgsqldbextensionnotloaded'] = 'Din serveropsætning inkluderer ikke pgsql udvidelsen. Mahara skal bruge denne for at lagre data i en relationel database. Undersøg om den er indlæst i php.ini eller installer den hvis den ikke allerede er installleret.';
$string['mysqldbextensionnotloaded'] = 'Din serveropsætning inkluderer ikke mysql udvidelsen. Mahara skal bruge denne for at lagre data i en relationel database. Undersøg om den er indlæst i php.ini eller installer den hvis den ikke allerede er installleret.';
$string['unknowndbtype'] = 'Din serveropsætning henviser til en ukendt type database. Gyldige værdier er "postgres8" og "mysql5". Skif venligst databasetype indstillingerne i config.php.';
$string['domextensionnotloaded'] = 'Din serveropsætning inkluderer ikke dom udvidelsen. Mahara skal buge denne til at forså XML data fra forskellige kilder.';
$string['xmlextensionnotloaded'] = 'Din serveropsætning inkluderer ikke %s udvidelsen. Mahara skal buge denne til at forså XML data fra forskellige kilder. Undersøg om den er indlæst i php.ini eller installer den hvis den ikke allerede er installleret.';
$string['gdextensionnotloaded'] = 'Din serveropsætning inkluderer ikke gd udvidelsen. Mahara skal bruge denne til at skalere og udføre andre handlinger på uploadede billeder. Undersøg om den er indlæst i php.ini eller installer den hvis den ikke allerede er installleret.';
$string['gdfreetypenotloaded'] = 'Din serveropsætning af gd udvidelsen inkluderer ikke Freetype understøttelse. Mahara skal bruge denne for at kunne lave CAPTCHA billeder. Sørg for at gd er sat op til det.';
$string['sessionextensionnotloaded'] = 'Din serveropsætning inkluderer ikke session udvidelsen. Mahara skal bruge denne for at brugere kan logge ind. Undersøg om den er indlæst i php.ini eller installer den hvis den ikke allerede er installleret.';
$string['curllibrarynotinstalled'] = 'Din serveropsætning inkluderer ikke curl udvidelsen. Mahara skal bruge denne til Moodle integration og til at modtage eksterne feeds. Undersøg om den er indlæst i php.ini eller installer den hvis den ikke allerede er installleret.';
$string['registerglobals'] = 'Du har farlige PHP indstillinger, register_globals er slået til. Mahara prøver at arbejde sig udenom det, men du bør virkelig ordne det';
$string['magicquotesgpc'] = 'Du har farlige PHP indstillinger, magic_quotes_gpc er slået til. Mahara prøver at arbejde sig udenom det, men du bør virkelig ordne det';
$string['magicquotesruntime'] = 'Du har farlige PHP indstillinger, magic_quotes_runtime er slået til. Mahara prøver at arbejde sig udenom det, men du bør virkelig ordne det';
$string['magicquotessybase'] = 'Du har farlige PHP indstillinger, magic_quotes_sybase er slået til. Mahara prøver at arbejde sig udenom det, men du bør virkelig ordne det';

$string['safemodeon'] = 'Det ser ud til at din server kører i safe mode. Mahara understøtter ikke at køre i safe mode. Du bør slå dette fra enten i php.ini filen, eller i din apache config til denne webside.

Hvis du er på en delt vært er det ikke sikkert at du kan få safe mode slået fra, ud over at spørge din vært. Overvej eventuelt at bruge en anden vært.';
$string['apcstatoff'] = 'Det ser ud til at din server kører APC med apc.stat=0. Mahara understøtter ikke denne opsætning. Du skal indstille apc.stat=1 i php.ini filen.

Hvis du er på en delt vært er det ikke sikkert at du kan få apc.stat safe mode slået til, ud over at spørge din vært. Overvej eventuelt at bruge en anden vært.';
$string['datarootinsidedocroot'] = 'Du har indstillet din data root til at være indeni din document root. Dette er et stort sikkerhedsproblem da alle og enhver så er i stand til at bede om session data (til at kapre andre folks logins), eller filer som de ikke har adgang til som andre folk har uploadet. Sæt venligst data root op til at være udenfor document root.';
$string['datarootnotwritable'] = 'Din valgte root mappe, %s, kan ikke skrives til. Det betyder at login sessioner, bruger filer eller enhver anden ting der skal uploades ikke kan gemmes på din server. Opret venligst mappen hvis den ikke eksisterer eller give ejerskab af mappen til web server brugeren hvis den gør.';
$string['couldnotmakedatadirectories'] = 'Af en eller anden årsag kunne core data mapperne ikke oprettes. Det burde ikke kunne ske, da Mahara allerede havde bekræftet at data root mappen var skrivbar. Undersøg tilladelserne til data roo mappen.';

$string['dbconnfailed'] = 'Mahara kunne ikke oprette forbindelse til applikationsdatabasen.

 * Hvis du er Mahara bruger så vent et minut og prøv igen.
 * Hvis du er administratoren så undersøg databaseindstillingerne og bekræft at databasen er tilgængelig.

Den modtagne fejl var::
';
$string['dbnotutf8'] = 'Du bruger ikke en UTF-8 database. Mahara gemmer al data som UTF-8 internt. Du bør rydde og genoprette din database med UTF-8 kodning.';
$string['dbversioncheckfailed'] = 'Din database server version er ikke ny nok til at kære Mahara. Din server er %s %s, men Mahara kræver mindst version %s.';

// general exception error messages
$string['blocktypenametaken'] = "Bloktype %s er allerede taget af et andet plugin (%s)";
$string['artefacttypenametaken'] = "Artefakttype %s er allerede taget af et andet plugin (%s)";
$string['artefacttypemismatch'] = "Artefakttype passer ikke sammen, du prøver at bruge denne %s som en %s";
$string['classmissing'] = "klasse %s til type %s i plugin %s manglede";
$string['artefacttypeclassmissing'] = "Artefakttyper skal alle implementere en klasse.  Mangler %s";
$string['artefactpluginmethodmissing'] =  "Artefakt plugin %s skal implementere %s, men gør det ikke";
$string['blocktypelibmissing'] = 'Mangler lib.php til blok %s i artefakt plugin %s';
$string['blocktypemissingconfigform'] = 'Bloktype %s skal implementere instance_config_form';
$string['versionphpmissing'] = 'Plugin %s %s mangler version.php!';
$string['blocktypeprovidedbyartefactnotinstallable'] = 'Dette vil blive installleret som en del af installationen for artefakt plugin %s';
$string['blockconfigdatacalledfromset'] = 'Configdata bør ikke indstilles direkte, brug PluginBlocktype::instance_config_save i stedet';
$string['invaliddirection'] = 'Ugyldig anvsining %s'; // Anvisning eller retning? Kontekst.
$string['onlyoneprofileviewallowed'] = 'Du kan kun have én profilvisning';
$string['onlyoneblocktypeperview'] = 'Kan ikke placere mere end én %s bloktype i en visning';

// if you change these next two , be sure to change them in libroot/errors.php
// as they are duplicated there, in the case that get_string was not available.
// Jeg kan ikke finde libroot.php?
$string['unrecoverableerror'] = 'En ikke-genoprettelig fejl opstod.  Det betyder sandsynligvis at du er stødt på en fejl i systemet.'; // Øh... ja.
$string['unrecoverableerrortitle'] = '%s - Webside ikke tilgængelig';
$string['parameterexception'] = 'Et påkrævet parameter manglede';

$string['notfound'] = 'Ikke fundet';
$string['notfoundexception'] = 'Siden du leder efer kunne ikke findes';

$string['accessdenied'] = 'Adgang nægtet';
$string['accessdeniedexception'] =  'Du har ikke adgang til at se denne side';

$string['viewnotfoundexceptiontitle'] = 'Visning ikke fundet';
$string['viewnotfoundexceptionmessage'] = 'Du prøvede at tilgå en visning der ikke eksisterer!';
$string['viewnotfound'] = 'Visning med id nummer %s ikke fundet';
$string['youcannotviewthisusersprofile'] = 'Du kan ikke se denne brugers profil';

$string['artefactnotfoundmaybedeleted'] = "Artefakt med id nummer %s ikke fundet (måske er det allerede slettet?)";
$string['artefactnotfound'] = 'Artefakt med id nummer %s ikke fundet';
$string['artefactnotinview'] = 'Artefakt %s er ikke i visning %s';
$string['artefactonlyviewableinview'] = 'Artefakter af denne type kan kun ses i en visning.';
$string['notartefactowner'] = 'Du ejer ikke dette artefakt';

$string['blockinstancednotfound'] = 'Blokinstans med id nummer %s ikke fundet';
$string['interactioninstancenotfound'] = 'Activitetsinstans med id nummer %s ikke fundet';

$string['invalidviewaction'] = 'Ugylding visningsstyrings handling: %s';

$string['missingparamblocktype'] = 'Prøv at vælge den bloktype du vil tilføje først';
$string['missingparamcolumn'] = 'Mangler kolonne specifikation';
$string['missingparamorder'] = 'Mangler rækkefølge specifikation';
$string['missingparamid'] = 'Mangler id';
?>
