<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['pluginname'] = 'Samlinger';

$string['about'] = 'Om';
$string['access'] = 'Adgang';
$string['accesscantbeused'] = 'Adgangstilsidesættelse ikke gemt. Den valgte visningsadgang (Hemmelig URL) kan ikke bruges på flere visninger.';
$string['accessoverride'] = 'Adgangstilsidesættelse';
$string['accesssaved'] = 'Samlingsadgang er blevet gemt';
$string['accessignored'] = 'Nogle hemmelige URL adgangstyper blev ignoreret.';
$string['add'] = 'Tilføj';
$string['addviews'] = 'Tilføj visninger';
$string['addviewstocollection'] = 'Tilføj visninger til samling';
$string['back'] = 'Tilbage';
$string['collection'] = 'samling';
$string['Collection'] = 'Samlinge';
$string['collections'] = 'Samlinger';
$string['collectionaccess'] = 'Samlingsadgang';
$string['collectionaccesseditedsuccessfully'] = 'Samlingsadgang er blevet gemt';
$string['collectioneditaccess'] = 'Du redigerer adgangen til %d visninger i denne samling';
$string['collectionconfirmdelete'] = 'Visninger i samlingen bliver ikke slettet.  Er du sikker på at du vil slette denne samling?';
$string['collectioncreatedsuccessfully'] = 'Samlingen er blevet oprettet.';
$string['collectiondeleted'] = 'Samlingen er blevet slettet.';
$string['collectiondescription'] = 'En samling er et sæt visninger der er linket til hinanden og har de samme adgangstilladelser. Du kan lave så mange samlinger som du vil, men en visning kan kun være en del af én samling.';
$string['confirmcancelcreatingcollection'] = 'Denne samling er ikke færdig. Vil du annullere alligevel?';
$string['collectionsaved'] = 'Samlingen blev gemt.';
$string['created'] = 'Oprettet';
$string['deletecollection'] = 'Slet samling';
$string['deletespecifiedcollection'] = 'Slet samlingen \'%s\'';
$string['deletingcollection'] = 'Sletter samling';
$string['deleteview'] = 'Fjern visning fra smaling';
$string['description'] = 'Samlingsbeskrivelse';
$string['editcollection'] = 'Rediger samling';
$string['editingcollection'] = 'Redigerer samling';
$string['edittitleanddesc'] = 'Rediger titel og beskrivelse';
$string['editviews'] = 'Rediger samlingens visninger';
$string['editviewaccess'] = 'Rediger visningsadgang';
$string['editaccess'] = 'REdiger smalingsadgang';
$string['emptycollectionnoeditaccess'] = 'Du kan ikke redigerer adgangen til tomme samlinger.  Tilføj nogle visninger først.';
$string['emptycollection'] = 'Tom samling';
$string['incorrectaccesstype'] = 'Bemærk: adgangstypen \'hemmelig URL\' er ikke tilgængelig som en adgangstilsidesættelses type.  Dette ignoreres og de eksisterende visningsadgangs typer er bibeholdt.  Men hvis den valgt vising havde andre adgangstilsidesættelser er disse blevet brugt og de eksisterende adgangstyper på visningerne er blevet fjernet.';
$string['manageviews'] = 'Administrer visninger';
$string['mycollections'] = 'Mine samlinger';
$string['name'] = 'Samlingsnavn';
$string['newcollection'] = 'Ny samling';
$string['nocollectionsaddone'] = 'Ingen samlinger endnu. %sLav en%s!';
$string['nooverride'] = 'Ingen tilsidesættelse';
$string['nooverridesaved'] = 'Ingen adgangstilsidesættelse for de valgte visninger.';
$string['noviewsavailable'] = 'Der er ingen tilgængelige visninger at tilføje.';
$string['noviewsaddsome'] = 'Ingen visninger i samlingen. %sTilføj nogen!%s';
$string['noviews'] = 'Ingen visninger.';
$string['overrideaccess'] = 'Tilsidesæt adgangs';
$string['potentialviews'] = 'Potentielle visninger';
$string['saveapply'] = 'Anvend og gem';
$string['savecollection'] = 'Gem samling';
$string['update'] = 'Opdater';
$string['usecollectionname'] = 'Brug samlingsnavn?';
$string['usecollectionnamedesc'] = 'Slå dette til hvis du ønsker at bruger samlingens navn frem for blokkens titel.';
$string['viewaddedtocollection'] = 'Visning tilføjet til samling. Samling opdateret til at inkludere adgang fra den nye visning.';
$string['viewcollection'] = 'Visningssamling detaljer';
$string['viewcount'] = 'Visninger';
$string['viewremovedsuccessfully'] = 'Visningen blev fjernet.';
$string['viewnavigation'] = 'Vis navigationslinje'; // Kotekst? Er det "View" eller view der menes?
$string['viewnavigationdesc'] = 'Tilføj en vandret navigationslinje til alle visninger i denne samling som standard.';
$string['views'] = 'Visninger';
$string['viewsaddedtocollection'] = 'Visning tilføjet til samling. Samling opdateret til at inkludere adgang fra den nye visning.';
$string['viewstobeadded'] = 'Visninger der skal tilføjes';
$string['viewconfirmremove'] = 'Er du sikker på at du vil fjerne denne visning fra smaliingen?';
?>
