<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['createview']             = 'Opret visning';
$string['edittitle']              = 'Rediger titel';
$string['edittitleanddescription'] = 'Rediger titel og beskrivelse';
$string['editcontent']            = 'Rediger indhold';
$string['editcontentandlayout']   = 'Rediger indhold og layout';
$string['editaccess']             = 'Rediger adgang';
$string['next']                   = 'Næste';
$string['back']                   = 'Tilbage';
$string['title']                  = 'Visningstitel'; // Kontekst?
$string['description']            = 'Visningsbeskrivelse';
$string['startdate']              = 'Adgang start dato/tid';
$string['stopdate']               = 'Adgang stop dato/tid';
$string['stopdatecannotbeinpast'] = 'Stopdatoen kan ikke være i fortiden';
$string['startdatemustbebeforestopdate'] = 'Startdatoen skal være før stopdatoen';
$string['unrecogniseddateformat'] = 'Kunne ikke genkende datoformat';
$string['allowcommentsonview']    = 'Hvis slået til kan brugerne kommentere på visningen.';
$string['ownerformat']            = 'Navnevinsing format';
$string['ownerformatdescription'] = 'Hvordan ønsker du at folk der ser på din visning skal se dit navn?';
$string['profileviewtitle']       = 'Profilvisning';
$string['dashboardviewtitle']  = 'Startside visning';
$string['editprofileview']        = 'Rediger profilvisning';
$string['grouphomepageviewtitle'] = 'Gruppe hjemmeside visning';

// my views
$string['artefacts'] = 'Artefakter';
$string['myviews'] = 'Mine visninger';
$string['groupviews'] = 'Gruppevisninger';
$string['institutionviews'] = 'Institutionsvisninger';
$string['reallyaddaccesstoemptyview'] = 'Din visning indeholder ingen blokke.  Vil du virkelig give disse brugere adgang til at se den?';
$string['viewdeleted'] = 'Visning slettet';
$string['viewsubmitted'] = 'Visning indsendt';
$string['deletethisview'] = 'Slet denne visning';
$string['submitthisviewto'] = 'Indsend denne visning til';
$string['forassessment'] = 'til vurdering';
$string['accessfromdate2'] = 'Ingen andre kan se denne visning før %s';
$string['accessuntildate2'] = 'Ingen andre kan se denne visning efter %s';
$string['accessbetweendates2'] = 'Ingen andre kan se denne visning før %s eller efter %s';
$string['artefactsinthisview'] = 'Artefakter i denne visning';
$string['whocanseethisview'] = 'Hvem kan se denne visning?';
$string['view'] = 'visning';
$string['views'] = 'visninger';
$string['View'] = 'Visning';
$string['Views'] = 'Visninger';
$string['viewsubmittedtogroup'] = 'Denne visning er blevet indsendt til <a href="%s">%s</a>';
$string['viewsubmittedtogroupon'] = 'Denne visning er blevet indsendt til <a href="%s">%s</a> på den %s'; // Kontekst? Skal passe på datoformatet.
$string['nobodycanseethisview2'] = 'Kun du kan se denne visning';
$string['noviews'] = 'Ingen visninger.';
$string['youhavenoviews'] = 'Du har ingen visninger.';
$string['youhaveoneview'] = 'Du har 1 visning.';
$string['youhaveviews']   = 'Du har %s visninger.';
$string['viewsownedbygroup'] = 'Visninger ejet af denne gruppe';
$string['viewssharedtogroup'] = 'Visninger delt med denne gruppe';
$string['viewssharedtogroupbyothers'] = 'Visninger delt med denne gruppe af andre';
$string['viewssubmittedtogroup'] = 'Visninger indsendt ti denne gruppe';
$string['submitaviewtogroup'] = 'Indsend en visning til denne gruppe';
$string['youhavesubmitted'] = 'Du har indsendt <a href="%s">%s</a> til denne gruppe';
$string['youhavesubmittedon'] = 'Du har indsendt <a href="%s">%s</a> til denne gruppe på den %s';

// access levels
$string['public'] = 'Offentlig';
$string['loggedin'] = 'Brugere logget ind';
$string['friends'] = 'Venner';
$string['groups'] = 'Grupper';
$string['users'] = 'Brugere';
$string['friendslower'] = 'venner';
$string['grouplower'] = 'gruppe';
$string['tutors'] = 'undervisere';
$string['loggedinlower'] = 'brugere logget ind';
$string['publiclower'] = 'ofeentlig';
$string['everyoneingroup'] = 'Alle i gruppen';
$string['token'] = 'Hemmelig URL';
$string['peoplewiththesecreturl'] = 'Folk med den hemelige URL';

// view user
$string['inviteusertojoingroup'] = 'Inviter denne bruger til at blive medlem af en gruppe';
$string['addusertogroup'] = 'Tilføj denne bruger til en gruppe';

// view view
$string['addedtowatchlist'] = 'Denne visning er blevet tilføjet til din overvågningsliste';
$string['attachment'] = 'Vedhæftning';
$string['removedfromwatchlist'] = 'Denne visning er blevet fjernet fra din overvågningsliste';
$string['addtowatchlist'] = 'Tilføj visning til overvågningsliste';
$string['removefromwatchlist'] = 'Fjern visning fra overvågningsliste';
$string['alreadyinwatchlist'] = 'Denne visning er ellerede på din overvågningsliste';
$string['attachedfileaddedtofolder'] = "Den vedhæftede fil %s er blevet tilføjet til din '%s' mappe.";
$string['complaint'] = 'Klage';
$string['date'] = 'Dato';
$string['notifysiteadministrator'] = 'Underret Webside administrator';
$string['print'] = 'Udskriv';
$string['reportobjectionablematerial'] = 'Anmeld anstødeligt indhold';
$string['reportsent'] = 'Din anmedlelse er blevet sendt';
$string['viewobjectionableunmark'] = 'Denne visning, eller noget på den, er blevet anmledt som anstødelig indhold.  Hvis dette ikke længere er tilfældet kan du klikke på knappen til at fjerne denne anmeldelse og give besked om det til di andre andministratorer.';
$string['notobjectionable'] = 'Ikke anstødeligt';
$string['viewunobjectionablesubject'] = 'Visningen %s blev markeret som ikke anstdeligt af %s';
$string['viewunobjectionablebody'] = '%s har undersøgt %s by %s og markeret at det ikke længere indeholder anstødeligt indhold.';
$string['updatewatchlistfailed'] = 'Opdatering af din overvågninsliste mislykkedes';
$string['watchlistupdated'] = 'Din overvågningsliste er blevet opdateret';
$string['editmyview'] = 'Redger min visning';
$string['backtocreatemyview'] = 'Tilbage til rediger min visning';
$string['viewvisitcount'] = '%d sidebesøg fra %s til %s';

$string['friend'] = 'Ven';
$string['profileicon'] = 'Profil-ikon';

// general views stuff
$string['Added'] = 'Tilføjet';
$string['allviews'] = 'Alle visninger';

$string['submitviewconfirm'] = 'Hvis du indsender \'%s\' til \'%s\' til bedømmelse vil du ikke være i stand til at redigere visningen indtil unerviseren er færdig med at bedømme visningen.  Er du sikker på at du vil indsende denne visning nu?';
$string['viewsubmitted'] = 'Visning sendt ind';
$string['submitviewtogroup'] = 'Indsend \'%s\' til \'%s\' til bedømmelse';
$string['cantsubmitviewtogroup'] = 'Du kan ikke sende denne vising ind til denne gruppe til bedømmelse';

$string['cantdeleteview'] = 'Du kan ikke slette denne visning';
$string['deletespecifiedview'] = 'Slet visning "%s"';
$string['deleteviewconfirm'] = 'Er du sikker på at du vil slette denne visning? Du kan ikke fortryde det.';
$string['deleteviewconfirmnote'] = '<p><strong>Bemærk:</strong> alle de indholdblokke der er blevet tilføjet til visningen vil ikke blive slettet. Al feedback givet i forhold til visningen vil dog blive slettet.  Overvej om du vil tage backup af visningen ved at eksportere den først.</p>';

$string['editaccessdescription'] = '<p>Som standard kan du se din %s. Du kan dele din %s med andre ved at tilføje adgangsregler på denne side.</p>
<p>Når du er klar, gå ned og tryk Gem for at fortsætte.</p>';

$string['overridingstartstopdate'] = 'Tilsidesæt Start/Stop Datoer';
$string['overridingstartstopdatesdescription'] = 'Hvis du vil, kan du tilsidesætte start og/eller slut datoerne. Andre folk kan ikke se din vising før start datoen og efter stopdatoen, uanset hvad de andre adgangstilladelse du måtte have givet.';

$string['emptylabel'] = 'Klik her for at tilføje tekst til mærkaten';
$string['empty_block'] = 'Vælg et artefakt fra træet til venstre som du vil placere her';

$string['viewinformationsaved'] = 'Visningsinformationen er blevet gemt';

$string['canteditdontown'] = 'Du kan ikke redigere denne visning da du ikke ejer den';
$string['canteditsubmitted'] = 'Du kan ikke redigere denne visning da den er blevet insendt til bedømmelse til "%s". Du er nødt til at vente indtil at en udnerviser frigiver din visning igen.';
$string['Submitted'] = 'Indsendt';
$string['submittedforassessment'] = 'Indsendt til bedømmelse';

$string['addtutors'] = 'Tilføj undervisere';
$string['viewcreatedsuccessfully'] = 'Visningen er blevet oprettet';
$string['viewaccesseditedsuccessfully'] = 'Visningsadgang er blevet gemt';
$string['viewsavedsuccessfully'] = 'Visningen er blevet gemt';

$string['invalidcolumn'] = 'Kolonne %s er udenfor rækkevide'; // Kontekst?

$string['confirmcancelcreatingview'] = 'Denne visning er ikke færdig. Er du sikker på at ville annullere?';

// view control stuff

$string['editblockspagedescription'] = '<p>Træk og slip indholdsblokke fra fanerne nedenfor for at oprette din visning.</p>';
$string['displaymyview'] = 'Vis min visning';
$string['editthisview'] = 'Rediger denne visning';

$string['success.addblocktype'] = 'Blokken er blevet tilføjet';
$string['err.addblocktype'] = 'Kunne ikke tilføje blokken til din visning';
$string['success.moveblockinstance'] = 'Blokken er blevet flyttet';
$string['err.moveblockinstance'] = 'Kunne ikke flytte blokken til den valgte placering';
$string['success.removeblockinstance'] = 'Blokken er blevet slettet';
$string['err.removeblockinstance'] = 'Kunne ikke slette blokken';
$string['success.addcolumn'] = 'Kolonnen er blevet tilføjet';
$string['err.addcolumn'] = 'Tilføjelse af kolonnen mislykkedes';
$string['success.removecolumn'] = 'Kolonnen er blevet slettet';
$string['err.removecolumn'] = 'Sletning af kolonnen mislykkedes';
$string['success.changetheme'] = 'Temaet er blevet opdateret';
$string['err.changetheme'] = 'Kunne ikke opdatere tema';

$string['confirmdeleteblockinstance'] = 'Er du sikker på at du vil slette denne blok?';
$string['blockinstanceconfiguredsuccessfully'] = 'Blokkens indstilinger er blevet opdateret';
$string['blockconfigurationrenderingerror'] = 'Opsætningen mislykkedes da bloken ikke kunne vises.';

$string['blocksintructionnoajax'] = 'Vælg en blok og vælg hvor du vil tilføje den til din visning. Du kan flytte blokke ved hjælp af pileknapperne i dens titellinje';
$string['blocksinstructionajax'] = 'Dette område viser et eksempel på hvordan din visning kommer til at se ud.<br>Træk blokke under denne linje for at tilføje dem til dit visningslayout. Du kan trække blokke rundt på dit visningslayout for at placere dem, hvor du vil have dem.';

$string['addnewblockhere'] = 'Tilføj ny blok her';
$string['add'] = 'Tilføj';
$string['addcolumn'] = 'Tilføj kolonne';
$string['remove'] = 'Fjern';
$string['removecolumn'] = 'Fjern denne kolonne';
$string['moveblockleft'] = "Flyt %s blokken til venstre";
$string['movethisblockleft'] = "Flyt denne blok til venstre";
$string['moveblockdown'] = "Flyt %s blokken ned";
$string['movethisblockdown'] = "Flyt denne blok ned";
$string['moveblockup'] = "Flyt %s blokken op";
$string['movethisblockup'] = "Flyt denne blok op";
$string['moveblockright'] = "Flyt %s blokken til højre";
$string['movethisblockright'] = "Flyt denne blok til højre";
$string['Configure'] = 'Indstil';
$string['configureblock'] = 'Indstil %s blokken';
$string['configurethisblock'] = 'Indstil denne blok';
$string['removeblock'] = 'Fjern %s blokken';
$string['removethisblock'] = 'Fjern denne blok';
$string['blocktitle'] = 'Bloktitel';

$string['changemyviewlayout'] = 'Skift mit visningslayout';
$string['viewcolumnspagedescription'] = 'Først skal du vælge antallet af kolonner i din visning. i det næste trin vil du være i stand til at ændre kolonnebredden.';
$string['viewlayoutpagedescription'] = 'Vælge hvordan du du ønsker at kolonnerne i din visning skal være opstillet.';
$string['changeviewlayout'] = 'Skift opsætningen for kolonner på min visning';
$string['backtoyourview'] = 'Tilbage til min visning';
$string['viewlayoutchanged'] = 'Visningsopstilling ændret';
$string['numberofcolumns'] = 'Antal kolonner';


$string['by'] = 'af';
$string['viewtitleby'] = '%s af <a href="%s">%s</a>';
$string['in'] = 'i';
$string['noblocks'] = 'Desværre, ingen blokke i denne kategori :(';
$string['Preview'] = 'Eksempel';
$string['timeofsubmission'] = 'Indsendelsestid';

$string['50,50'] = $string['33,33,33'] = $string['25,25,25,25'] = 'Ens bredder';
$string['67,33'] = 'Større venstre kolonne';
$string['33,67'] = 'Større højre kolonne';
$string['25,50,25'] = 'Større midterkolonne';
$string['15,70,15'] = 'Meget større midterkolonne';
$string['20,30,30,20'] = 'Større midterkolonner';
$string['noviewlayouts'] = 'Der er ingen visingsopstillinger for en visning med %s kolonner';
$string['cantaddcolumn'] = 'Du kan ikke tilføje flere kolonner til denne visning';
$string['cantremovecolumn'] = 'Du kan ikke fjerne den sidste kolonne fra denne visning';

$string['blocktypecategory.feeds'] = 'Eksterne feeds';
$string['blocktypecategory.fileimagevideo'] = 'Filer, billeder og film';
$string['blocktypecategory.general'] = 'Generelt';

$string['notitle'] = 'Ingen titel';
$string['clickformoreinformation'] = 'Klik for mere information g for at give feedback';

$string['Browse'] = 'Gennemse';
$string['Search'] = 'Søg';
$string['noartefactstochoosefrom'] = 'Desværre, der er ingen artefakter at vælge imellem';

$string['access'] = 'Adgang';
$string['noaccesstoview'] = 'Du har ikke tilladelse til at få adgang til denne visning';

$string['changeviewtheme'] = 'Temaet do har valgt til denne visning er ikke længere tilgængeligt for dig.  Vælg venligst et andet tema.';

// Templates
$string['Template'] = 'Skabelon';
$string['allowcopying'] = 'Tillad kopiering';
$string['templatedescription'] = 'Slå dette til hvis du vil tillade at folk, der kan se din visning, skal være i stand til at lave deres egne kopier af den, sammen med alle filer og mapper som den indeholder.';
$string['templatedescriptionplural'] = 'Slå dette til hvis du vil tillade at folk, der kan se dine visninger, skal være i stand til at lave deres egne kopier af dem, sammen med alle filer og mapper som de indeholder.';
$string['choosetemplatepagedescription'] = '<p>Her kan du søge efter visninger som du har lov til at kopiere som begyndelsespunkt for en ny visning. Du kan se et eksempel på hver visning ved at klikke på dens navn. Når du har fundet den visning som du vil kopiere klik på den matchende "Kopier visning" knap for at lave en kopi og begynde at tilpasse den.</p>';
$string['choosetemplategrouppagedescription'] = '<p>Her kan du søge efter visninger som denne gruppe har lov til at kopiere som begyndelsespunkt for en ny visning. Du kan se et eksempel på hver visning ved at klikke på dens navn. Når du har fundet den visning som du vil kopiere klik på den matchende "Kopier visning" knap for at lave en kopi og begynde at tilpasse den.</p><p><strong>Bemærk:</strong> Grupper kan i øjeblikket ikke lave kopier af blogs og blogindlæg.</p>';
$string['choosetemplateinstitutionpagedescription'] = '<p>Her kan du søge efter visninger som denne institution har lov til at kopiere som begyndelsespunkt for en ny visning. Du kan se et eksempel på hver visning ved at klikke på dens navn. Når du har fundet den visning som du vil kopiere klik på den matchende "Kopier visning" knap for at lave en kopi og begynde at tilpasse den.</p><p><strong>Bemærk:</strong> Institutioner kan i øjeblikket ikke lave kopier af blogs og blogindlæg.</p>';
$string['copiedblocksandartefactsfromtemplate'] = 'Kopierede %d blokke og %d artefakter fra %s';
$string['filescopiedfromviewtemplate'] = 'Filer kopieret fra %s';
$string['viewfilesdirname'] = 'viewfiles';
$string['viewfilesdirdesc'] = 'Filer fra kopierede visninger';
$string['thisviewmaybecopied'] = 'Kopiering er tilladt';
$string['copythisview'] = 'Kopier denne visning';
$string['copyview'] = 'Kopier visning';
$string['createemptyview'] = 'Opret tom visning';
$string['copyaview'] = 'Kopier en visning';
$string['Untitled'] = 'Unavngivet';
$string['copyfornewusers'] = 'Kopier til nye brugere';
$string['copyfornewusersdescription'] = 'Når en ny bruger bliver oprettet, bliver det oprettet en personlig kopi af denne visning i deres portefølje automatisk.';
$string['copyfornewmembers'] = 'Kopier til nye institutionsmedlemmer';
$string['copyfornewmembersdescription'] = 'Opret en personling kopi af denne vising for alle nye medlemmer af %s.';
$string['copyfornewgroups'] = 'Kopier til nye grupper';
$string['copyfornewgroupsdescription'] = 'Lav en kopi af denne visning i alle nye gruppe med disse gruppetyper:';
$string['searchviews'] = 'Søg efter visninger';
$string['searchowners'] = 'Søg efter ejere';
$string['owner'] = 'ejer';
$string['Owner'] = 'Ejer';
$string['owners'] = 'ejere';
$string['show'] = 'vis';
$string['searchviewsbyowner'] = 'Søg efter visninger efter ejer:';
$string['selectaviewtocopy'] = 'Vælg den visning du vil kopiere:';
$string['listviews'] = 'Opstil visninger'; // Hmm, oversættelse af List (vb). Kunne måske være bedre.
$string['nocopyableviewsfound'] = 'Ingen visninger du kan kopiere';
$string['noownersfound'] = 'Ingen ejere fundet';
$string['viewsby'] = 'Visninger af %s';
$string['Preview'] = 'Eksempel';
$string['viewscopiedfornewusersmustbecopyable'] = 'Du skal tillade kopiering før du kan indstille en visning til at blive kopieret til nye brugere.';
$string['viewscopiedfornewgroupsmustbecopyable'] = 'Du skal tillade kopiering før du kan indstille en visning til at blive kopieret til nye grupper.';
$string['copynewusergroupneedsloggedinaccess'] = 'Visninger kopieret til nye brugere skal give adgang til brugere, der er logget ind.';
$string['viewcopywouldexceedquota'] = 'At kopiere denne vising ville overskride din filkvote.';

$string['blockcopypermission'] = 'Blok kopiereings tilladelse';
$string['blockcopypermissiondesc'] = 'Hvis du tillader at andre brugere kan kopiere denne visning kan du vælge hvordan denne blok bliver kopieret';

// View types
$string['dashboard'] = 'Startside';
$string['profile'] = 'Profil';
$string['portfolio'] = 'Portefølje';
$string['grouphomepage'] = 'Gruppehjemmeside';

$string['grouphomepagedescription'] = 'Gruppehjemmesidevisningen er det indhold, der vises på Om fanen for denne gruppe';

?>
