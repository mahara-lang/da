<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

// General form strings
$string['add']     = 'Tilføj';
$string['cancel']  = 'Annuller';
$string['delete']  = 'Slet';
$string['edit']    = 'Rediger';
$string['editing'] = 'Redigerer';
$string['save']    = 'Gem';
$string['submit']  = 'Indsend';
$string['update']  = 'Opdater';
$string['change']  = 'Skift';
$string['send']    = 'Send';
$string['go']      = 'Gå'; // Som i "Gå til", hvis det er "GO!" så skal der stå "Afsted"! Kontekst?
$string['default'] = 'Standard';
$string['upload']  = 'Upload';
$string['complete']  = 'Færdig';
$string['Failed']  = 'Mislykket';
$string['loading'] = 'Indlæser...';
$string['showtags'] = 'Vis mine tags';
$string['errorprocessingform'] = 'Der skete en fejl under indsendelsen af denne formular. Unersøg de markerede felter og prøv igen.';
$string['description'] = 'Beskrivelse';
$string['remove']  = 'Fjern';
$string['Close'] = 'Luk';
$string['Help'] = 'Hjælp';
$string['applychanges'] = 'Anvend ændringer';

$string['no']     = 'Nej';
$string['yes']    = 'Ja';
$string['none']   = 'Ingen';
$string['at'] = 'ved'; // Kontekst?
$string['From'] = 'Fra';
$string['To'] = 'Til';
$string['All'] = 'Alle';

$string['enable'] = 'Tænd';
$string['disable'] = 'Sluk';
$string['pluginenabled'] = 'Plugin slået til';
$string['plugindisabled'] = 'Plugin slået fra';
$string['pluginnotenabled'] = 'Plugin ikke slået til.  Du skal slå %s pluginet til først.';

$string['next']      = 'Næste';
$string['nextpage']  = 'Næste side';
$string['previous']  = 'Forrige';
$string['prevpage']  = 'Forrige side';
$string['first']     = 'Første';
$string['firstpage'] = 'Første side';
$string['last']      = 'Sidste';
$string['lastpage']  = 'Sidste side';

$string['accept'] = 'Accepter';
$string['memberofinstitutions'] = 'Medlem af %s';
$string['reject'] = 'Afvis';
$string['sendrequest'] = 'Send anmodning';
$string['reason'] = 'Årsag';
$string['select'] = 'Vælg';

// Tags
$string['tags'] = 'Tags';
$string['tagsdesc'] = 'Skriv tags adskilt af kommaer for denne genstand.';
$string['tagsdescprofile'] = 'Skriv tags adskilt af kommaer for denne genstand. Genstande med et \'profile\' tag bliver vist i dit sidepanel.';
$string['youhavenottaggedanythingyet'] = 'Du har ikke tagget noget endnu';
$string['mytags'] = 'Mine tags';
$string['Tag'] = 'Tag';
$string['itemstaggedwith'] = 'Genstande med "%s" som tag';
$string['numitems'] = '%s genstande';
$string['searchresultsfor'] = 'Søgeresultater for';
$string['alltags'] = 'Alle tags';
$string['sortalpha'] = 'Sorter tags alfabtisk';
$string['sortfreq'] = 'Sorter tags efter hyppighed';
$string['sortresultsby'] = 'Sorter resultater efter:';
$string['date'] = 'Dato';
$string['dateformatguide'] = 'Brug formatet ÅÅÅÅ/MM/DD'; // Ændrer vi det? Kan vi det?
$string['datetimeformatguide'] = 'Brug formatet ÅÅÅÅ/MM/DD TT:MM';
$string['filterresultsby'] = 'Filtrer resultater efter:';
$string['tagfilter_all'] = 'Alle';
$string['tagfilter_file'] = 'Filer';
$string['tagfilter_image'] = 'Billeder';
$string['tagfilter_text'] = 'Tekst';
$string['tagfilter_view'] = 'Visninger';
$string['edittags'] = 'Rediger tags';
$string['selectatagtoedit'] = 'Vælg det tag du vil redigere';
$string['edittag'] = 'Rediger <a href="%s">%s</a>';
$string['editthistag'] = 'Rediger dette tag';
$string['edittagdescription'] = 'Alle genstande i din portefølje med tagget "%s" vil blive opdateret';
$string['deletetag'] = 'Slet <a href="%s">%s</a>';
$string['confirmdeletetag'] = 'Er du sikker på at du vil slette dette gat fra alle enstande i din portefølje?';
$string['deletetagdescription'] = 'Fjern dette tag fra alle genstande i din portefølje';
$string['tagupdatedsuccessfully'] = 'Tagget er blevet opdateret';
$string['tagdeletedsuccessfully'] = 'Tagget er blevet slettet';

$string['selfsearch'] = 'Søg min portefølje';

// Quota strings
$string['quota'] = 'Kvote';
$string['quotausage'] = 'Du har brugt <span id="quota_used">%s</span> af din <span id="quota_total">%s</span> kvote.';

$string['updatefailed'] = 'Opdatering mislykkedes';

$string['strftimenotspecified']  = 'Ikke angivet';

// profile sideblock strings
$string['invitedgroup'] = 'gruppeinvitation';
$string['invitedgroups'] = 'gruppeinvitationer';
$string['logout'] = 'Log ud';
$string['pendingfriend'] = 'afventende ven';
$string['pendingfriends'] = 'afventende venner';
$string['profile'] = 'profil';
$string['views'] = 'Visninger';

// Online users sideblock strings
$string['onlineusers'] = 'Online brugere';
$string['lastminutes'] = 'De seneste %s minutter';

// Links and resources sideblock
$string['linksandresources'] = 'Links og ressourcer';

// auth
$string['accesstotallydenied_institutionsuspended'] = 'Din institution %s, er blevet suspenderet.  indtil at den bliver afsuspenderet vil du ikke kunne logge ind i %s.
Kontakt venligst din institution hvis du ønsker hjælp.';
$string['accessforbiddentoadminsection'] = 'Du har ikke tilladelse til få adgang til administrationssektionen';
$string['accountdeleted'] = 'Beklager, din konto er blevet slettet';
$string['accountexpired'] = 'Beklager, din konto er udløbet';
$string['accountcreated'] = '%s: Ny konto';
$string['accountcreatedtext'] = 'Kære %s,

En ny konto er blevet oprettet for dig på %s. Dine detaljer er følgende:

Brugernavn: %s
Kodeord: %s

Besøg %s for at komme i gang!

Venlig hilsen, %s webside administrator';
$string['accountcreatedchangepasswordtext'] = 'Kære %s,

En ny konto er blevet oprettet for dig på %s. Dine detaljer er følgende:

Brugernavn: %s
Kodeord: %s

Når du logger ind første gang, vil du blive bedt om at skifte kodeord.

Besøg %s for at komme i gang!

Venlig hilsen, %s webside administrator';
$string['accountcreatedhtml'] = '<p>Kære %s</p>

En ny konto er blevet oprettet for dig på <a href="%s">%s</a>. Dine detaljer er følgende:</p>

<ul>
    <li><strong>Brugernavn:</strong> %s</li>
    <li><strong>Kodeord:</strong> %s</li>
</ul>

<p>Besøg <a href="%s">%s</a> for at komme i gang!</p>

<p>Venlig hilsen, %s webside administrator</p>
';
$string['accountcreatedchangepasswordhtml'] = '<p>Kære %s</p>

En ny konto er blevet oprettet for dig på <a href="%s">%s</a>. Dine detaljer er følgende:</p>

<ul>
    <li><strong>Brugernavn:</strong> %s</li>
    <li><strong>Kodeord:</strong> %s</li>
</ul>

<p>Når du logger ind første gang, vil du blive bedt om at skifte kodeord.</p>

<p>Besøg <a href="%s">%s</a> for at komme i gang!</p>

<p>Venlig hilsen, %s webside administrator</p>
';
$string['accountexpirywarning'] = 'Konto udløbs andvarsel';
$string['accountexpirywarningtext'] = 'Kære %s,

Din konto på %s udløber indenfor %s.

Vi anbefaler at du kemmer indholdet af dinportefølje ned Eksport værktøjet. Instruktioner i hvordan man bruger dette værktøj kan findes i bruger guiden.

Hvis du ønsker at forlænge din kontoadgang eller har spørgsmål om det ovenstående, kontakt os da venligst:

%s

Venlig hilsen, %s webside administrator';
$string['accountexpirywarninghtml'] = '<p>Kære %s,</p>
    
<p>Din konto på %s udløber indenfor %s.</p>

<p>Vi anbefaler at du kemmer indholdet af dinportefølje ned Eksport værktøjet. Instruktioner i hvordan man bruger dette værktøj kan findes i bruger guiden.</p>

<p>Hvis du ønsker at forlænge din kontoadgang eller har spørgsmål om det ovenstående, <a href="%s">kontakt os</a> da venligst.</p>

<p>Venlig hilsen, %s webside administrator</p>';
$string['institutionmembershipexpirywarning'] = 'Institutionsmedlemskab udløbs advarsel';
$string['institutionmembershipexpirywarningtext'] = 'Kære %s,

Dit medlemskab af %s på %s udløber indenfor %s.

Hvis du ønsker at forlænge dit medlemskab eller har spørgsmål om det ovenstående, kontakt os da venligst:

%s

Venlig hilsen, %s webside administrator';
$string['institutionmembershipexpirywarninghtml'] = '<p>Kære %s,</p>

<p>Dit medlemskab af %s på %s udløber indenfor %s..</p>

<p>Hvis du ønsker at forlænge dit medlemskab eller har spørgsmål om det ovenstående, <a href="%s">kontakt os</a> da venligst.</p>

<p>Venlig hilsen, %s webside administrator</p>';
$string['institutionexpirywarning'] = 'Institution udløbs advarsel';
$string['institutionexpirywarningtext_institution'] = 'Kære %s,

%ss medlemskab af %s udløber indenfor %s.

Hvis du ønsker at forlænge din organisations medlemskab eller har spørgsmål om det ovenstående, kontakt os da venligst:

%s

Venlig hilsen, %s webside administrator';
$string['institutionexpirywarninghtml_institution'] = '<p>Kære %s,</p>

<p>%ss medlemskab af %s udløber indenfor %s.</p>

<p>Hvis du ønsker at forlænge din organisations medlemskab eller har spørgsmål om det ovenstående, <a href="%s">kontakt os</a> da venligst.</p>

<p>Venlig hilsen, %s webside administrator</p>';
$string['institutionexpirywarningtext_site'] = 'Kære %s,

Institutionen \'%s\' udløber indenfor %s.

Det kan være at du vil kontakte dem for at forlænge deres medlemskab af %s.

Venlig hilsen, %s webside administrator';
$string['institutionexpirywarninghtml_site'] = '<p>Kære %s,</p>

<p>Institutionen \'%s\' udløber indenfor %s.</p>

<p>Det kan være at du vil kontakte dem for at forlænge deres medlemskab af %s.</p>

<pVenlig hilsen, %s webside administrator</p>';
$string['accountinactive'] = 'Beklager, din konto er inaktiv i øjeblikket';
$string['accountinactivewarning'] = 'Kontoinaktivitets advarsel';
$string['accountinactivewarningtext'] = 'Kære %s,

Din konto på %s bliver inaktiv indenfor %s.

Når kontoen er inaktiv vil du ikke være i stand til at logge ind for at en administrator har genaktiveret den.

Du kan forhindre at din konto bliver inaktiv ved at logge ind.

Venlig hilsen, %s webside administrator';
$string['accountinactivewarninghtml'] = '<p>Kære %s,</p>

<p>Din konto på %s bliver inaktiv indenfor %s.</p>

<p>Når kontoen er inaktiv vil du ikke være i stand til at logge ind for at en administrator har genaktiveret den.</p>

<p>Du kan forhindre at din konto bliver inaktiv ved at logge ind.</p>

<p>Venlig hilsen, %s webside administrator</p>';
$string['accountsuspended'] = 'Din konto har været suspenderet siden %s. Grunden til din suspendering er:<blockquote>%s</blockquote>';
$string['youraccounthasbeensuspended'] = 'Din konto er blevet suspenderet';
$string['youraccounthasbeenunsuspended'] = 'Din konto er blevet afsuspenderet';
$string['changepassword'] = 'Skift kodeord';
$string['changepasswordinfo'] = 'Du er nødt til at skifte dit kodeord før du kan fortsætte.';
$string['chooseusernamepassword'] = 'Vælg dit brugernavn og kodeord';
$string['chooseusernamepasswordinfo'] = 'Du skal bruge et brugernavn og kodeord for at kunne logge ind i %s.  Vælg dem venligst nu.';
$string['confirmpassword'] = 'Bekræft kodeord';
$string['javascriptnotenabled'] = 'Din browser har ikke javascript slået til for denne side. Mahara kræver at javascript er slået til for at du kan logge ind';
$string['cookiesnotenabled'] = 'Din browser har ikke cookies slået til eller blokere cookies fa denne side. Mahara kræver at cookies er slået til for at du kan logge ind';
$string['institution'] = 'Institution';
$string['loggedoutok'] = 'Du er nu blevet logget ud';
$string['login'] = 'Log ind';
$string['loginfailed'] = 'Du har ikke given den korrekte information for at kunne logge ind. Berkæft at dit brugernavn og kodeord er korrekte.';
$string['loginto'] = 'Log ind i %s';
$string['newpassword'] = 'Nyt kodeord';
$string['nosessionreload'] = 'Genindløs siden for at logge ind';
$string['oldpassword'] = 'Nuværende kodeord';
$string['password'] = 'Kodeord';
$string['passworddescription'] = ' ';
$string['passwordhelp'] = 'Det kodeord du bruger for at få adgang til systemet';
$string['passwordnotchanged'] = 'Du skiftede ikke dit kodeord, vælg venligst et nyt kodeord';
$string['passwordsaved'] = 'Dit nye kodeoerd er blevet gemt';
$string['passwordsdonotmatch'] = 'Kodeordene er ikke ens';
$string['passwordtooeasy'] = 'Dit kodeord er for nemt at gætte! Vælg venligst et sværere kodeord.';
$string['register'] = 'Registrer';
$string['sessiontimedout'] = 'Din session er udløbet. Indtast dine logind oplysninger for at forsætte';
$string['sessiontimedoutpublic'] = 'Din session er udløbet. Du kan <a href="%s">logge ind</a> for at fortsætte med at browse';
$string['sessiontimedoutreload'] = 'Din session er udløbet. Genindlæs siden for at logge ind igen';
$string['username'] = 'Brugernavn';
$string['preferredname'] = 'Vist navn';
$string['usernamedescription'] = ' ';
$string['usernamehelp'] = 'Det brugernavn du er blevet givet for at have adgang til dette system.';
$string['youaremasqueradingas'] = 'Du er forklædt som %s.';
$string['yournewpassword'] = 'Dit nye kodeord. Kodeord skal være på mindst seks tegn og indeholde som minimum ét tal og to bogstaver';
$string['yournewpasswordagain'] = 'Dit nye kodeord igen';
$string['invalidsesskey'] = 'Ugyldig session nøgle';
$string['cannotremovedefaultemail'] = 'Du kan ikke fjerne din primære e-mail-adresse';
$string['emailtoolong'] = 'E-mail-adresser kan ikke være længere end 255 tegn';
$string['mustspecifyoldpassword'] = 'Du skal angive dit nuværende kodeord';
$string['Site'] = 'Webside';

// Misc. register stuff that could be used elsewhere
$string['emailaddress'] = 'E-mail-adresse';
$string['emailaddressdescription'] = ' ';
$string['firstname'] = 'Fornavn';
$string['firstnamedescription'] = ' ';
$string['lastname'] = 'Efternavn';
$string['lastnamedescription'] = ' ';
$string['studentid'] = 'Studienummer';
$string['displayname'] = 'Vist navn';
$string['fullname'] = 'Fuldt navn';
$string['registerwelcome'] = 'Velkommen! For at bruge siden er du nødt til at registere dig.';
$string['registeragreeterms'] = 'Du skal også acceptere sidens <a href="terms.php">vilkår og betingelser</a>.';
$string['registerprivacy'] = 'Den data vi indsamler her, vil blive lagret som angivet i vores <a href="privacy.php">erklæring om beskyttelse af personlige oplysninger</a>.';
$string['registerstep3fieldsoptional'] = '<h3>Vælg et valgfrit profilbillede</h3><p>Du er nu blevet registeret hos %s! Du kan nu vælge et valgfrit profilbillede der vil blive vist som din avatar.</p>';
$string['registerstep3fieldsmandatory'] = '<h3>Udfyld de påkrævede profil felter</h3><p>De følgende felter er påkrævet. Du skal udfylde dem all for at fuldende din registrering.</p>';
$string['registeringdisallowed'] = 'Beklager, du kan ikke registere dig til dette system i øjeblikket';
$string['membershipexpiry'] = 'medlemskab udløber';
$string['institutionfull'] = 'Den valgte institution accepterer ikke flere registreringer.';
$string['registrationnotallowed'] = 'Den valgte institution tillader ikke selv-registrering.';
$string['registrationcomplete'] = 'Tak fordi du registerede dig hos %s';
$string['language'] = 'Sprog';

// Forgot password
$string['cantchangepassword'] = 'Beklager, du kan ikke ændre dit kodeord via dette interface - brug din institutions interface i stedet';
$string['forgotusernamepassword'] = 'Glemt dit brugernavn eller kodeord?';
$string['forgotusernamepasswordtext'] = '<p>Hvis du har glemt dit brugernavn eller kodeord, indtast den e-mail-adresse der er på din profil for at sende dig en mail, som du kan bruge til at skifte dit kodeord med.</p>
<p>Hvis du kender dit brugernavn, men hat glemt dit kodeord, kan du også indtaste dit brugernavn i stedet.</p>';
$string['lostusernamepassword'] = 'Glemt brugernavn/kodeord';
$string['emailaddressorusername'] = 'E-mail-adresse eller brugernavn';
$string['pwchangerequestsent'] = 'Du skulle om et øjeblik modtage en e-mail med et link som du kan bruge til at skifte koderordet på din konto.';
$string['forgotusernamepasswordemailsubject'] = 'Brugernavn/Kodeord detaljer til %s';
$string['forgotusernamepasswordemailmessagetext'] = 'Kære %s,

Der er blevet indgivet in anmodning for glemt brugernavn/kodeord for din konto på %s.

Dit brugernavn er %s.

Ønsker du at nulstille dit kodeord, så følg nedenstående link:

%s

Hvis du ikke har anmodet om at få nulstillet dit kodeord, så ignorer denne e-mail.

Hvis du har spørgsmål om det ovenstående, kontakt os da venligst:

%s

Venlig hilsen, %s webside administrator';
$string['forgotusernamepasswordemailmessagehtml'] = '<p>Kære %s,</p>

<p>Der er blevet indgivet in anmodning for glemt brugernavn/kodeord for din konto på %s.</p>

<p>Dit brugernavn er <strong>%s</strong>.</p>

<p>Ønsker du at nulstille dit kodeord, så følg nedenstående link:</p>

<p><a href="%s">%s</a></p>

<p>Hvis du ikke har anmodet om at få nulstillet dit kodeord, så ignorer denne e-mail.</p>

<p>Hvis du har spørgsmål om det ovenstående, <a href="%s">kontakt os</a> da venligst.</p>

<p>Venlig hilsen, %s webside administrator</p>';
$string['forgotpassemailsendunsuccessful'] = 'Beklager, det ser ud til at e-mail ikke blev sendt. Det er vores fejl, prøv igen om et øjeblik.';
$string['forgotpassemaildisabled'] = 'Beklager, e-mail er slået fra til den e-mail-adresse eller det brugernavn du indtastede. Kontakt venligst en administrator for at få nulstillet dit kodeord.';
$string['forgotpassnosuchemailaddressorusername'] = 'Den e-mail-adresse eller det brugernavn du indtastede matcher ikke nogen bruger på denne side';
$string['forgotpasswordenternew'] = 'Indtast venligst dit nye kodeord for at fortsætte';
$string['nosuchpasswordrequest'] = 'Ingen kodeordsanmodning fundet';
$string['passwordchangedok'] = 'Dit kodeord er blevet ændret';

// Reset password when moving from external to internal auth.
$string['noinstitutionsetpassemailsubject'] = '%s: medlemskab af %s';
$string['noinstitutionsetpassemailmessagetext'] = 'Kære %s,

Du er ikke længere medlem af %s.
Du kan fortsætte på %s med dit nuværende brugernavn, men du skal vælge et nyt kodeord til din konto.

Følg venligst det nedenstående link for at fortsætte med at nulstille dit kodeord.

%sforgotpass.php?key=%s

Hvis du har spørgsmål om det ovenstående, kontakt os da venligst:

%scontact.php

Venlig hilsen, %s webside administrator

%sforgotpass.php?key=%s';
$string['noinstitutionsetpassemailmessagehtml'] = '<p>Kære %s,</p>

<p>Du er ikke længere medlem af %s.</p>
<p>Du kan fortsætte på %s med dit nuværende brugernavn, men du skal vælge et nyt kodeord til din konto.</p>

<p>Følg venligst det nedenstående link for at fortsætte med at nulstille dit kodeord.</p>

<p><a href="%sforgotpass.php?key=%s">%sforgotpass.php?key=%s</a></p>

<p>Hvis du har spørgsmål om det ovenstående, <a href="%s">kontakt os</a> da venligst.</p>

<p>Venlig hilsen, %s webside administrator</p>

<p><a href="%sforgotpass.php?key=%s">%sforgotpass.php?key=%s</a></p>';
$string['debugemail'] = 'BEMÆRK: Denne e-mail skulle sendes til %s <%s>, men er blevet sendt til dig som følge af "sendallemailto" indstillingerne.';
$string['divertingemailto'] = 'Sender e-mail til %s';


// Expiry times
$string['noenddate'] = 'Ingen slutdato';
$string['day']       = 'dag';
$string['days']      = 'dage';
$string['weeks']     = 'uger';
$string['months']    = 'måneder';
$string['years']     = 'år';
// Boolean site option

// Site content pages
$string['sitecontentnotfound'] = '%s tekst er ikke tilgængeligt';

// Contact us form
$string['name']                     = 'Navn';
$string['email']                    = 'E-mail';
$string['subject']                  = 'Emne';
$string['message']                  = 'Besked';
$string['messagesent']              = 'Din besked er blevet sendt';
$string['nosendernamefound']        = 'Intet afsendernavn blev givet';
$string['emailnotsent']             = 'Sending af kontakt e-mail mislykkedes. Fejlmeddelelse: "%s"';

// mahara.js
$string['namedfieldempty'] = 'Det påkrævede felt "%s" er tomt';
$string['processing']     = 'Arbejder';
$string['requiredfieldempty'] = 'Et påkrævet felt er tomt';
$string['unknownerror']       = 'Der skete en ukendt fejl (0x20f91a0)';

// menu
$string['home']        = 'Hjem';
$string['myportfolio'] = 'Min portefølje';
$string['myviews']       = ' Mine visninger';
$string['settings']    = 'Indstillinger';
$string['myfriends']          = 'Mine venner';
$string['findfriends']        = 'Find venner';
$string['groups']             = 'Grupper';
$string['mygroups']           = 'Mine grupper';
$string['findgroups']         = 'Find grupper';
$string['returntosite']       = 'Tilbage til hovedsiden';
$string['siteadministration'] = 'Webside administration';
$string['useradministration'] = 'Bruger administration';

$string['unreadmessages'] = 'ulæste beskeder';
$string['unreadmessage'] = 'ulæst besked';

$string['siteclosed'] = 'Siden er midlertidgt lukket på grund af en database opgradering.  Kun webside administratorer kan logge ind.';
$string['siteclosedlogindisabled'] = 'Siden er midlertidgt lukket på grund af en database opgradering.  <a href="%s">Udfør opdateringen nu.</a>';

// footer
$string['termsandconditions'] = 'Vilkår og betingelser';
$string['privacystatement']   = 'Beskyttelse af personlige oplysninger';
$string['about']              = 'Om';
$string['contactus']          = 'Kontakt os';

// my account
$string['account'] =  'Min konto';
$string['accountprefs'] = 'Præferencer';
$string['preferences'] = 'Præferencer';
$string['activityprefs'] = 'Aktivitetsprøferencer';
$string['changepassword'] = 'Skift kodeord';
$string['notifications'] = 'Beskeder';
$string['inbox'] = 'Indbakke';
$string['gotoinbox'] = 'Gå til indbakke';
$string['institutionmembership'] = 'Institutionsmedlemskab';
$string['institutionmembershipdescription'] = 'Hvis du er medlem af nogle institutioner vil de være angivet her. Du kan også anmode institutioner om medlemskab og acceptere eller afvise invitationer fra institutioner.';
$string['youareamemberof'] = 'Du er medlem af %s';
$string['leaveinstitution'] = 'Forlad institution';
$string['reallyleaveinstitution'] = 'Er du sikker på at du vil forlade denne institution?';
$string['youhaverequestedmembershipof'] = 'Du har anmodet om medlemskab af %s';
$string['cancelrequest'] = 'Annuller anmodning';
$string['youhavebeeninvitedtojoin'] = 'Du er blevet inviteret til at blive medlem af %s';
$string['confirminvitation'] = 'Bekræft invitation';
$string['joininstitution'] = 'Bliv medlem af institution';
$string['decline'] = 'Afvis';
$string['requestmembershipofaninstitution'] = 'Anmod om medlemskab af en institution';
$string['optionalinstitutionid'] = 'Institutions ID (valgfrit)';
$string['institutionmemberconfirmsubject'] = 'Institutionsmedlemskab bekræftet';
$string['institutionmemberconfirmmessage'] = 'Du er blevet tilføjet som medlem af %s.';
$string['institutionmemberrejectsubject'] = 'Institutionsmedlemskab anmodning afvist';
$string['institutionmemberrejectmessage'] = 'Din anmodning om medlemskab af %s blev afvist.';
$string['Memberships'] = 'Medlemskaber';
$string['Requests'] = 'Anmodninger';
$string['Invitations'] = 'Invitationer';

$string['config'] = 'Opsætning';

$string['sendmessage'] = 'Send besked';
$string['spamtrap'] = 'Spam fælde';
$string['formerror'] = 'Der skete en fejl med indlæsning af din indsendelse. Prøv igen.';
$string['formerroremail'] = 'Kontakt os på %s hvis du fortsat har problemer.';

$string['notinstallable'] = 'Kan ikke installeres!';
$string['installedplugins'] = 'Installerede plugins';
$string['notinstalledplugins'] = 'Ikke-installerede plugins';
$string['plugintype'] = 'Plugin type';

$string['settingssaved'] = 'Indstillinger gemt';
$string['settingssavefailed'] = 'Indstillingerne blev ikke gemt!';

$string['width'] = 'Bredde';
$string['height'] = 'Højde';
$string['widthshort'] = 'b';
$string['heightshort'] = 'h';
$string['filter'] = 'Filtrer';
$string['expand'] = 'Udvid';
$string['collapse'] = 'Luk sammen'; // Kan ikke lide den oversættelse...
$string['more...'] = 'Mere...';
$string['nohelpfound'] = 'Der blev ikke fundet hjælp til dette punkt';
$string['nohelpfoundpage'] = 'Der blev ikke fundet hjælp til denne side';
$string['couldnotgethelp'] = 'Der skete end fejl under indlæsning af hjælpesiden';
$string['profileimage'] = 'Profilbillede';
$string['primaryemailinvalid'] = 'Din primære e-mail-adresse er udgyldig';
$string['addemail'] = 'Tilføj e-mail-adresse';

// Search
$string['search'] = 'Søg';
$string['searchusers'] = 'Søg brugere';
$string['Query'] = 'Forespørgsel';
$string['query'] = 'forespørgsel';
$string['querydescription'] = 'Ord der skal søges efter';
$string['result'] = 'resultat';
$string['results'] = 'resultater';
$string['Results'] = 'Resultater';
$string['noresultsfound'] = 'Ingen resultater fundet';
$string['users'] = 'Brugere';

// artefact
$string['artefact'] = 'artefakt';
$string['Artefact'] = 'Artefakt';
$string['Artefacts'] = 'Artefakter';
$string['artefactnotfound'] = 'Kunne ikke finde et artefakt med id %s';
$string['artefactnotrendered'] = 'Artefakt vises ikke';
$string['nodeletepermission'] = 'Du har ikke tilladelse til at slette dette artefakt';
$string['noeditpermission'] = 'Du har ikke tilladelse til at redigere dette artefakt';
$string['Permissions'] = 'Tilladelser';
$string['republish'] = 'Udgiv';
$string['view'] = 'Vis'; // Går ud fra det er udsagnsord her...
$string['artefactnotpublishable'] = 'Artefaktet %s kan ikke udgives på visningen %s';

$string['belongingto'] = 'Tilhører';
$string['allusers'] = 'Alle brugere';
$string['attachment'] = 'Vedhæftning';

// Upload manager
$string['quarantinedirname'] = 'quarantine';
$string['clammovedfile'] = 'Filen er blevet flyttet til en karantæne mappe.';
$string['clamdeletedfile'] = 'Filen er blevet slettet';
$string['clamdeletedfilefailed'] = 'Filen kunne ikke slettes';
$string['clambroken'] = 'Din administrator har slået virusscanning til på filupload, men har indstillet noget forkert.  Din fil er IKKE blevet uploadet. Din administrator har fået en e-mail om problemet så det kan blive løst.  Prøv at uploade filen igen senere.';
$string['clamemailsubject'] = '%s :: Clam AV besked';
$string['clamlost'] = 'Clam AV er indstillet til at køre på uloadede filer, men den angivne sti til Clam AV, %s, er ugyldig.';
$string['clamfailed'] = 'Clam AV blev ikke kørt.  Det sendte fejbeskeden %s. Her her Clams output:';
$string['clamunknownerror'] = 'Der skete en ukendt fejl med Clam.';
$string['image'] = 'Billede';
$string['filenotimage'] = 'Den uploadede fil er ikke et gyldigt billede. Det skal være en PNG, JPEG eller GIF fil.';
$string['uploadedfiletoobig'] = 'Filen var for stor. Spørg din administrator, hvis du ønsker yderligere information.';
$string['notphpuploadedfile'] = 'Filen gik tabt under upload. Det burde ikke kunne ske. Kontakt din administrator, hvis du ønsker yderligere information.';
$string['virusfounduser'] = 'Filen du uploadede, %s, er bleve virusscannet, og fundet som inficeret! Din filupload blev IKKE gennemført.';
$string['fileunknowntype'] = 'Den uploadede fils type kune ikke genkendes. Din fil kan være korrupt, eller det kan være et opsætningsproblem. Kontakt venligst din administrator.';
$string['virusrepeatsubject'] = 'Advarsel: %s har uploadet virus gentagne gange.';
$string['virusrepeatmessage'] = 'Brugeren %s har uploadet flere filer der er blevet scannet af en virusscanner og fundet inficerede.';

$string['phpuploaderror'] = 'Der skete en fejl under uplad af filen: %s (Fejlkode %s)';
$string['phpuploaderror_1'] = 'Den uploadede fil overskrider upload_max_filesize indtillingen i php.ini.';
$string['phpuploaderror_2'] = 'Den uploadede fil overskrider MAX_FILE_SIZE indstillingen, der blev opsat i HTML formen.';
$string['phpuploaderror_3'] = 'Den uploadede fil bev un delvis uploadet.';
$string['phpuploaderror_4'] = 'Ingen fil blev uploadet.';
$string['phpuploaderror_6'] = 'Mangler en midlertidig mappe.';
$string['phpuploaderror_7'] = 'Kunne ikke skrive fil til disk.';
$string['phpuploaderror_8'] = 'Filupload afbrudt af en udvidelse.';
$string['adminphpuploaderror'] = 'En filuploadnings fejl opstod sandsynligvis på grund af serveropsætningen.';

$string['youraccounthasbeensuspended'] = 'Din konto er blevet suspenderet';
$string['youraccounthasbeensuspendedtext2'] = 'Din konto hos %s er blevet suspenderet af %s.'; // @todo: more info?
$string['youraccounthasbeensuspendedreasontext'] = "Din konto hos %s er blevet suspenderet af %s. Begrundelse:\n\n%s";
$string['youraccounthasbeenunsuspended'] = 'Din konto er blevet afsuspenderet';
$string['youraccounthasbeenunsuspendedtext2'] = 'Din konto hos %s er blevet afsuspenderet. Du kan nu igen logge ind og bruge siden.'; // can't provide a login link because we don't know how they log in - it might be by xmlrpc

// size of stuff
$string['sizemb'] = 'MB';
$string['sizekb'] = 'KB';
$string['sizegb'] = 'GB';
$string['sizeb'] = 'b';
$string['bytes'] = 'bytes';

// countries

$string['country.af'] = 'Afghanistan';
$string['country.ax'] = 'Ålandsøerne';
$string['country.al'] = 'Albanien';
$string['country.dz'] = 'Algeriet';
$string['country.as'] = 'Amerikansk Samoa';
$string['country.ad'] = 'Andorra';
$string['country.ao'] = 'Angola';
$string['country.ai'] = 'Anguilla';
$string['country.aq'] = 'Antarktis';
$string['country.ag'] = 'Antigua og Barbuda';
$string['country.ar'] = 'Argentina';
$string['country.am'] = 'Armenien';
$string['country.aw'] = 'Aruba';
$string['country.au'] = 'Australien';
$string['country.at'] = 'Østrig';
$string['country.az'] = 'Aserbajdsjan'; // Virkelig? Det staves sådan på dansk?
$string['country.bs'] = 'Bahamas';
$string['country.bh'] = 'Bahrain';
$string['country.bd'] = 'Bangladesh';
$string['country.bb'] = 'Barbados';
$string['country.by'] = 'Hviderusland';
$string['country.be'] = 'Belgien';
$string['country.bz'] = 'Belize';
$string['country.bj'] = 'Benin';
$string['country.bm'] = 'Bermuda';
$string['country.bt'] = 'Bhutan';
$string['country.bo'] = 'Bolivia';
$string['country.ba'] = 'Bosnien og Hercegovina';
$string['country.bw'] = 'Botswana';
$string['country.bv'] = 'Bouvetøen';
$string['country.br'] = 'Brasilien';
$string['country.io'] = 'British Indian Ocean Territory'; // Det hedder det også på dansk?
$string['country.bn'] = 'Brunei Darussalam';
$string['country.bg'] = 'Bulgarien';
$string['country.bf'] = 'Burkina Faso';
$string['country.bi'] = 'Burundi';
$string['country.kh'] = 'Cambodja';
$string['country.cm'] = 'Cameroun';
$string['country.ca'] = 'Canada';
$string['country.cv'] = 'Kap Verde';
$string['country.ky'] = 'Caymanøerne';
$string['country.cf'] = 'Centralafrikanske Republik';
$string['country.td'] = 'Tchad';
$string['country.cl'] = 'Chile';
$string['country.cn'] = 'Kina';
$string['country.cx'] = 'Christmas Island';
$string['country.cc'] = 'Cocosøerne (Keelingøerne)';
$string['country.co'] = 'Colombia';
$string['country.km'] = 'Comorerne';
$string['country.cg'] = 'Congo';
$string['country.cd'] = 'Demokratiske Republik Congo';
$string['country.ck'] = 'Cookøerne';
$string['country.cr'] = 'Costa Rica';
$string['country.ci'] = 'Elfenbenskysten';
$string['country.hr'] = 'Kroatien';
$string['country.cu'] = 'Cuba';
$string['country.cy'] = 'Cypern';
$string['country.cz'] = 'Tjekkiet';
$string['country.dk'] = 'Danmark';
$string['country.dj'] = 'Djibouti';
$string['country.dm'] = 'Dominica';
$string['country.do'] = 'Dominikanske Republik';
$string['country.ec'] = 'Ecuador';
$string['country.eg'] = 'Egypten';
$string['country.sv'] = 'El Salvador';
$string['country.gq'] = 'Ækvatorialguinea';
$string['country.er'] = 'Eritrea';
$string['country.ee'] = 'Estland';
$string['country.et'] = 'Etiopien';
$string['country.fk'] = 'Falklandsøerne (Malvinerne)';
$string['country.fo'] = 'Færøerne';
$string['country.fj'] = 'Fiji';
$string['country.fi'] = 'Finland';
$string['country.fr'] = 'Frankrig';
$string['country.gf'] = 'Fransk Guyana';
$string['country.pf'] = 'Fransk Polynesien';
$string['country.tf'] = 'Franske Sydterritorier';
$string['country.ga'] = 'Gabon';
$string['country.gm'] = 'Gambia';
$string['country.ge'] = 'Georgien';
$string['country.de'] = 'Tyskland';
$string['country.gh'] = 'Ghana';
$string['country.gi'] = 'Gibraltar';
$string['country.gr'] = 'Grækenland';
$string['country.gl'] = 'Grønland';
$string['country.gd'] = 'Grenada';
$string['country.gp'] = 'Guadeloupe';
$string['country.gu'] = 'Guam';
$string['country.gt'] = 'Guatemala';
$string['country.gg'] = 'Guernsey';
$string['country.gn'] = 'Guinea';
$string['country.gw'] = 'Guinea-Bissau';
$string['country.gy'] = 'Guyana';
$string['country.ht'] = 'Haiti';
$string['country.hm'] = 'Heard- og McDonaldøerne';
$string['country.va'] = 'Vatikanstaten';
$string['country.hn'] = 'Honduras';
$string['country.hk'] = 'Hongkong';
$string['country.hu'] = 'Ungarn';
$string['country.is'] = 'Island';
$string['country.in'] = 'Indien';
$string['country.id'] = 'Indonesien';
$string['country.ir'] = 'Iran, Den Islamiske Republik';
$string['country.iq'] = 'Irak';
$string['country.ie'] = 'Irland';
$string['country.im'] = 'Isle of Man';
$string['country.il'] = 'Israel';
$string['country.it'] = 'Italien';
$string['country.jm'] = 'Jamaica';
$string['country.jp'] = 'Japan';
$string['country.je'] = 'Jersey';
$string['country.jo'] = 'Jordan';
$string['country.kz'] = 'Kasakhstan';
$string['country.ke'] = 'Kenya';
$string['country.ki'] = 'Kiribati';
$string['country.kp'] = 'Korea, Den Demokratiske Folkerepublik';
$string['country.kr'] = 'Korea, Republikken';
$string['country.kw'] = 'Kuwait';
$string['country.kg'] = 'Kirgisistan';
$string['country.la'] = 'Laos, Demokratiske Folkerepublik';
$string['country.lv'] = 'Letland';
$string['country.lb'] = 'Libanon';
$string['country.ls'] = 'Lesotho';
$string['country.lr'] = 'Liberia';
$string['country.ly'] = 'Libyen';
$string['country.li'] = 'Liechtenstein';
$string['country.lt'] = 'Litauen';
$string['country.lu'] = 'Luxembourg';
$string['country.mo'] = 'Macao';
$string['country.mk'] = 'Makedonien'; // Ikke så nem at oversætte som den burde have været.
$string['country.mg'] = 'Madagaskar';
$string['country.mw'] = 'Malawi';
$string['country.my'] = 'Malaysia';
$string['country.mv'] = 'Maldiverne';
$string['country.ml'] = 'Mali';
$string['country.mt'] = 'Malta';
$string['country.mh'] = 'Marshalløerne';
$string['country.mq'] = 'Martinique';
$string['country.mr'] = 'Mauretanien';
$string['country.mu'] = 'Mauritius';
$string['country.yt'] = 'Mayotte';
$string['country.mx'] = 'Mexico';
$string['country.fm'] = 'Mikronesiens Forenede Stater';
$string['country.md'] = 'Moldova, Republikken';
$string['country.mc'] = 'Monaco';
$string['country.mn'] = 'Mongoliet';
$string['country.ms'] = 'Montserrat';
$string['country.ma'] = 'Marokko';
$string['country.mz'] = 'Mozambique';
$string['country.mm'] = 'Myanmar';
$string['country.na'] = 'Namibia';
$string['country.nr'] = 'Nauru';
$string['country.np'] = 'Nepal';
$string['country.nl'] = 'Holland';
$string['country.an'] = 'Nederlandske Antiller';
$string['country.nc'] = 'Ny Kaledonien';
$string['country.nz'] = 'New Zealand';
$string['country.ni'] = 'Nicaragua';
$string['country.ne'] = 'Niger';
$string['country.ng'] = 'Nigeria';
$string['country.nu'] = 'Niue';
$string['country.nf'] = 'Norfolk Island';
$string['country.mp'] = 'Nordmarianerne';
$string['country.no'] = 'Norge';
$string['country.om'] = 'Oman';
$string['country.pk'] = 'Pakistan';
$string['country.pw'] = 'Palau';
$string['country.ps'] = 'Palæstinensiske områder, besatte';
$string['country.pa'] = 'Panama';
$string['country.pg'] = 'Papua Ny Guinea';
$string['country.py'] = 'Paraguay';
$string['country.pe'] = 'Peru';
$string['country.ph'] = 'Filippinerne';
$string['country.pn'] = 'Pitcairn';
$string['country.pl'] = 'Polen';
$string['country.pt'] = 'Portugal';
$string['country.pr'] = 'Puerto Rico';
$string['country.qa'] = 'Qatar';
$string['country.re'] = 'Réunion';
$string['country.ro'] = 'Romænien';
$string['country.ru'] = 'Rusland';
$string['country.rw'] = 'Rwanda';
$string['country.sh'] = 'Sankt Helena';
$string['country.kn'] = 'Saint Kitts og Nevis';
$string['country.lc'] = 'Saint Lucia';
$string['country.pm'] = 'Saint-Pierre og Miquelon';
$string['country.vc'] = 'Saint Vincent og Grenadinerne';
$string['country.ws'] = 'Samoa';
$string['country.sm'] = 'San Marino';
$string['country.st'] = 'Sao Tome og Principe';
$string['country.sa'] = 'Saudi-Arabien';
$string['country.sn'] = 'Senegal';
$string['country.cs'] = 'Serbien og Montenegro';
$string['country.sc'] = 'Seychellerne';
$string['country.sl'] = 'Sierra Leone';
$string['country.sg'] = 'Singapore';
$string['country.sk'] = 'Slovakiet';
$string['country.si'] = 'Slovenien';
$string['country.sb'] = 'Salomonøerne';
$string['country.so'] = 'Somalia';
$string['country.za'] = 'Sydafrika';
$string['country.gs'] = 'South Georgia og South Sandwich Islands';
$string['country.es'] = 'Spanien';
$string['country.lk'] = 'Sri Lanka';
$string['country.sd'] = 'Sudan';
$string['country.sr'] = 'Surinam';
$string['country.sj'] = 'Svalbard og Jan Mayen';
$string['country.sz'] = 'Swaziland';
$string['country.se'] = 'Sverige';
$string['country.ch'] = 'Schweiz';
$string['country.sy'] = 'Syrien';
$string['country.tw'] = 'Taiwan, kinesisk provins';
$string['country.tj'] = 'Tadsjikistan';
$string['country.tz'] = 'Tanzania';
$string['country.th'] = 'Thailand';
$string['country.tl'] = 'Østtimor';
$string['country.tg'] = 'Togo';
$string['country.tk'] = 'Tokelau';
$string['country.to'] = 'Tonga';
$string['country.tt'] = 'Trinidad og Tobago';
$string['country.tn'] = 'Tunisien';
$string['country.tr'] = 'Tyrkiet';
$string['country.tm'] = 'Turkmenistan';
$string['country.tc'] = 'Turks- og Caicosøerne';
$string['country.tv'] = 'Tuvalu';
$string['country.ug'] = 'Uganda';
$string['country.ua'] = 'Ukraine';
$string['country.ae'] = 'Forenede Arabiske Emirater';
$string['country.gb'] = 'Storbritannien';
$string['country.us'] = 'USA';
$string['country.um'] = 'USAs ydre småøer';
$string['country.uy'] = 'Uruguay';
$string['country.uz'] = 'Usbekistan';
$string['country.vu'] = 'Vanuatu';
$string['country.ve'] = 'Venezuela';
$string['country.vn'] = 'Vietnam';
$string['country.vg'] = 'Britiske Jomfruøer';
$string['country.vi'] = 'Amerikanske Jomfruøer';
$string['country.wf'] = 'Wallis og Futuna';
$string['country.eh'] = 'Vestsahara';
$string['country.ye'] = 'Yemen';
$string['country.zm'] = 'Zambia';
$string['country.zw'] = 'Zimbabwe';

$string['nocountryselected'] = 'Intet land valgt';

// general stuff that doesn't really fit anywhere else
$string['system'] = 'System';
$string['done'] = 'Færdig';
$string['back'] = 'Tilbage';
$string['backto'] = 'Tilbage til %s';
$string['alphabet'] = 'A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,Æ,Ø,Å'; // Nem oversættelse, hvad bruges det til? Skal AA tilføjes for at eksempelvis Aalborg kommer til sidst?
$string['formatpostbbcode'] = 'Du kan fomatere dit indlæg med BBCode. %sLær mere%s';
$string['Created'] = 'Oprettet';
$string['Updated'] = 'Opdateret';
$string['Total'] = 'I alt';
$string['Visits'] = 'Besøg';

// import related strings (maybe separated later)
$string['importedfrom'] = 'Importeret fra %s';
$string['incomingfolderdesc'] = 'Filer importeret fra andre værter på netværket';
$string['remotehost'] = 'Fjernvært %s';

$string['Copyof'] = 'Kopi af %s';

// Profile views
$string['loggedinusersonly'] = 'Tillad kun brugere der er logget ind';
$string['allowpublicaccess'] = 'Tillad offentlig adgang';
$string['thisistheprofilepagefor'] = 'Dette er profilsiden for %s';
$string['viewmyprofilepage']  = 'Vis profilside';
$string['editmyprofilepage']  = 'Rediger profilside';
$string['usersprofile'] = "%ss profil";
$string['profiledescription'] = 'Din profilvisning er hvad andre ser når de klikker på dit navn eller profil-ikon.';

// Dashboard views
$string['mydashboard'] = 'Min startside';
$string['editdashboard'] = 'Rediger';
$string['usersdashboard'] = "%ss startside";
$string['dashboarddescription'] = 'Din startside visning er hvad du ser på hjemmesiden når du lige er logget ind. Kun du har adgang til den.';
$string['topicsimfollowing'] = "Emner jeg følger med i";
$string['recentactivity'] = 'Nylig aktivitet';
$string['mymessages'] = 'Mine beskeder';

$string['pleasedonotreplytothismessage'] = "Svar venligst ikke på denne besked.";
$string['deleteduser'] = 'Slettet bruger';

$string['theme'] = 'Tema';
$string['choosetheme'] = 'Vælg tema...';

// Home page info block
$string['Hide'] = 'Skjul';
$string['createcollect'] = 'Opret og indsaml';
$string['createcollectsubtitle'] = 'Udvikl din portefølje';
$string['updateyourprofile'] = 'Opdater din <a href="%s">Profil</a>';
$string['uploadyourfiles'] = 'Upload dine <a href="%s">Filer</a>';
$string['createyourresume'] = 'Opret dit <a href="%s">CV</a>';
$string['publishablog'] = 'Udgiv en <a href="%s">Blog</a>';
$string['Organise'] = 'Organiser';
$string['organisesubtitle'] = 'Fremvis din portefølje med visninger';
$string['organisedescription'] = 'Organiser din portefølje som <a href="%s">visninger.</a> Opret forskellige visninger til forskelligt publikum - du vælger selv hvilke elementer du vil inkludere.';
$string['sharenetwork'] = 'Deling og netværk';
$string['sharenetworksubtitle'] = 'Mød venner og meld dig ind i grupper';
$string['findfriendslinked'] = 'Find <a href="%s">venner</a>';
$string['joingroups'] = 'Meld dig ind i <a href="%s">grupper</a>';
$string['sharenetworkdescription'] = 'Du kan finjustere hvem der har adgang til hver visning, og hvor længe.';
$string['howtodisable'] = 'Du har skjult informationsboksen.  Du kan styre dens synlighed i <a href="%s">indstillingerne</a>.';
?>
