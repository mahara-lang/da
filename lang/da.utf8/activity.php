<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage notification-internal
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['typemaharamessage'] = 'Systembesked';
$string['typeusermessage'] = 'Besked fra andre brugere';
$string['typewatchlist'] = 'Overvågningsliste';
$string['typeviewaccess'] = 'Ny visnings adgang';
$string['typecontactus'] = 'Kontakt os';
$string['typeobjectionable'] = 'Anstødeligt indhold';
$string['typevirusrepeat'] = 'Gentagen virus upload';
$string['typevirusrelease'] = 'Virus flag udløst';
$string['typeadminmessages'] = 'Administrationsbesked';
$string['typeinstitutionmessage'] = 'Institutionsbesked';
$string['typegroupmessage'] = 'Gruppebesked';

$string['type'] = 'Aktivitetstype';
$string['attime'] = 'klokken'; // [Aktivitet] klokken [tid] er hvad jeg regner med her...
$string['prefsdescr'] = 'Uanset hvliken e-mail-indstilling du vælger, så vil du stadig få meddelelser i din indbakke, men de vil automatisk blive markeret som læst.';

$string['subject'] = 'Emne';
$string['date'] = 'Dato';
$string['read'] = 'Læst';
$string['unread'] = 'Ulæst';

$string['markasread'] = 'Marker som læst';
$string['selectall'] = 'Vælg alle';
$string['recurseall'] = 'Gør alle rekursive'; //Øh... Hvad? Hvorfor ville man gøre det? Eller bruger Mahara en ny definition på rekursiv?
$string['alltypes'] = 'Alle typer';

$string['markedasread'] = 'Markerede dine beskeder som læst';
$string['failedtomarkasread'] = 'Markering af dine beskeder som læst mislykkedes';

$string['deletednotifications'] = 'Slettede %s beskeder';
$string['failedtodeletenotifications'] = 'Sletning af dine beskeder mislykkedes';

$string['stopmonitoring'] = 'Stands overvågning';
$string['viewsandartefacts'] = 'Visninger og artefakter';
$string['views'] = 'Visninger';
$string['artefacts'] = 'Artefakter';
$string['groups'] = 'Grupper';
$string['monitored'] = 'Overvåger';
$string['stopmonitoring'] = 'Stands overvågning';

$string['stopmonitoringsuccess'] = 'Overvågningen blev standset';
$string['stopmonitoringfailed'] = 'Standsning af overvågning mislykkedes';

$string['newwatchlistmessage'] = 'Ny aktivitet på din overvågningsliste';
$string['newwatchlistmessageview'] = '%s har redigeret sin visning "%s"';

$string['newviewsubject'] = 'Ny visning oprettet';
$string['newviewmessage'] = '%s har oprettet en ny visning "%s"';

$string['newcontactusfrom'] = 'Ny kontakt os fra';
$string['newcontactus'] = 'Ny kontakt os';

$string['newviewaccessmessage'] = 'Du er blevet tilføjet til adgangslisten til visningen kaldet "%s" af %s';
$string['newviewaccessmessagenoowner'] = 'Du er blevet tilføjet til adgangslisten til visningen kaldet "%s"';
$string['newviewaccesssubject'] = 'Ny visningsadgang';

$string['viewmodified'] = 'har ændret sin visning';
$string['ongroup'] = 'i gruppe'; // i/på/ved? Kontekst!
$string['ownedby'] = 'ejet af';

$string['objectionablecontentview'] = 'Anstødeligt indhold på visning "%s" anmeldt af %s';
$string['objectionablecontentviewartefact'] = 'Anstødeligt indhold på visning "%s" i "%s" anmeldt af %s';

$string['newgroupmembersubj'] = '%s er nu et gruppemedlem!';
$string['removedgroupmembersubj'] = '%s er ikke løngere et gruppemedlem';

$string['addtowatchlist'] = 'Tilføj til overvågningsliste';
$string['removefromwatchlist'] = 'Fjern fra overvågningsliste';

$string['missingparam'] = 'Påkrævet parameter %s var tomt for aktivitetstypen %s';

$string['institutionrequestsubject'] = '%s har bed om medlemskab af %s.';
$string['institutionrequestmessage'] = 'Du kan tilføje brugere til institutioner på institutionsmedlems siden:';

$string['institutioninvitesubject'] = 'Du er blevet inviteret til at blive medlem af institutionen: %s.';
$string['institutioninvitemessage'] = 'Du kan bekræfte dit medlemskab af institutionen på din institutionsindstillinger side:';

$string['deleteallnotifications'] = 'Slet alle beskeder';
$string['reallydeleteallnotifications'] = 'Er du sikker på at du vil slette alle dine beskeder?';

$string['viewsubmittedsubject'] = 'Visning indsendt til %s';
$string['viewsubmittedmessage'] = '%s har indsendt sin visning "%s" til %s';

?>
