<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage core
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['changepassworddesc'] = 'Nyt kodeord';
$string['changepasswordotherinterface'] = 'Du kan <a href="%s">ændre dit kodeord</a> gennem et andet interface';
$string['oldpasswordincorrect'] = 'Dette er ikke dit nuværende kodeord';

$string['changeusernameheading'] = 'Skift brugernavn';
$string['changeusername'] = 'Nyt brugernavn';
$string['changeusernamedesc'] = 'Det brugernavn du benytter til at logge ind på %s. Brugernavne er 3-30 tegn lange og må indeholde bogstaver, tal og de fleste gængse symboler, bortset fra mellemrum.';

$string['accountoptionsdesc'] = 'Generelle kontoindstilinger';
$string['friendsnobody'] = 'Ingen må tilføje mig som ven';
$string['friendsauth'] = 'Nye venner kræver min godkendelse';
$string['friendsauto'] = 'Nye venner godkendes automatisk';
$string['friendsdescr'] = 'Venneanmodninger';
$string['updatedfriendcontrolsetting'] = 'Indstillinger opdateret';

$string['wysiwygdescr'] = 'HTML-editor';
$string['on'] = 'Slået til';
$string['off'] = 'Slået fra';
$string['disabled'] = 'Slået fra';
$string['enabled'] = 'Slået til';

$string['messagesdescr'] = 'Beskeder fra andre brugere';
$string['messagesnobody'] = 'Ingen brugere må sende mig beskeder';
$string['messagesfriends'] = 'Kun brugere fra min venneliste må sende mig beskeder';
$string['messagesallow'] = 'Alle brugere må sende mig beskeder';

$string['language'] = 'Sprog';

$string['showviewcolumns'] = 'Vis indstillinger for at tilføje og fjerne kolonner når jeg redigerer en visning';

$string['tagssideblockmaxtags'] = 'Maksimalt antal tags i tag-sky'; // Tag Cloud. Træls ord at oversætte.
$string['tagssideblockmaxtagsdescription'] = 'Det maksimale antal tags, der kan vises i din tag-sky.';

$string['enablemultipleblogs'] = 'Slå flere blogs til';
$string['enablemultipleblogsdescription']  = 'Som standard har du én blog. Hvis du vil have mere end én blog, slå denne mulighed til.';
$string['disablemultipleblogserror'] = 'Du kan ikke slå flere blogs fra medmindre du kun har én blog';

$string['hiderealname'] = 'Skjul rigtigt navn';
$string['hiderealnamedescription'] = 'Slå denne mulighed til hvis du har et vist navn og ikke vil have at andre brugere skal kunne finde dig via dit rigtige navn i brugersøgninger.';

$string['showhomeinfo'] = 'Vis information om Mahara på forsiden';

$string['prefssaved']  = 'Indstillingerne blev gemt';
$string['prefsnotsaved'] = 'Indstillingerne blev ikke gemt';

$string['maildisabled'] = 'E-mail slået fra';
$string['maildisabledbounce'] = <<<EOF
E-mail afsending til din e-mail-adresse er blevet slået fra, da for mange e-mails blev returneret til serveren.
Undersøg om din e-mail virker korrekt før du slår e-mail til igen i kontoindstillingerne på %s.
EOF;
$string['maildisableddescription'] = 'E-mail afsending til din e-mail-adresse er blevet slået fra. Du kan <a href="%s">slå e-mail til igen</a> fra dine kontoindstillinger.';

$string['deleteaccount'] = 'Slet konto';
$string['deleteaccountdescription'] = 'Hvis du sletter din konto vil din profilinformation og dine visninger ikke længere kunne ses af andre brgere.  Indholdet af de forunindlæg du kan have skrevet vil stadig være synligt, men du vil ikke løngere være nævnt som forfatter af dem.';
$string['accountdeleted'] = 'Din konto er blevet slettet.';
?>
