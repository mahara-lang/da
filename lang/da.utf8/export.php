<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['allmydata'] = 'Al min data';
$string['chooseanexportformat'] = 'Vælg et eksportformat';
$string['clicktopreview'] = 'Klik for et eksempel';
$string['collectionstoexport'] = 'Samlinger der skal eksporteres';
$string['creatingzipfile'] = 'Opretter Zip fil';
$string['Done'] = 'Udført';
$string['Export']     = 'Eksport';
$string['exportgeneratedsuccessfully'] = 'Eksporten er blevet genereret. %sKlik her for at downloade den%s';
$string['exportgeneratedsuccessfullyjs'] = 'Eksporten er blevet genereret. %sFortsæt%s';
$string['exportingartefactplugindata'] = 'Eksporterer artefakt plugin data';
$string['exportingartefacts'] = 'Eksporterer artefakter';
$string['exportingartefactsprogress'] = 'Eksporterer artefakter: %s/%s';
$string['exportingfooter'] = 'Eksporterer sidefod';
$string['exportingviews'] = 'Eksporterer visninger';
$string['exportingviewsprogress'] = 'Eksporterer visninger: %s/%s';
$string['exportpagedescription'] = 'Dette værktøj eksporterer al din profilinformation og alle dine visninger, men ikke dine indstillinger for websiden.';
$string['exportyourportfolio'] = 'Eksporterer din portefølje';
$string['generateexport'] = 'Generer eksport';
$string['noexportpluginsenabled'] = 'Ingen eksports plugins er slået til fra administratoren, så du kan ikke bruge denne funktion';
$string['justsomecollections'] = 'Bare nogle af mine samlingers';
$string['justsomeviews'] = 'Bare nogle af mine visninger';
$string['pleasewaitwhileyourexportisbeinggenerated'] = 'Vent venligst mens din eksport genereres...';
$string['reverseselection'] = 'Inverter det valgte'; // Kontekst? Som i Photoshop?
$string['selectall'] = 'Vælg alt';
$string['setupcomplete'] = 'Opsætning fuldført';
$string['Starting'] = 'Starter';
$string['unabletoexportportfoliousingoptions'] = 'Ikke i stand til at eksportere en portefølje med de valgte indstillinger';
$string['unabletogenerateexport'] = 'Ikke i stand til at generere eksporten';
$string['viewstoexport'] = 'Visninger der skal eksporteres';
$string['whatdoyouwanttoexport'] = 'Hvad vil du gerne eksportere?';
$string['writingfiles'] = 'Skriver filer';
$string['youarehere'] = 'Du er her';
$string['youmustselectatleastonecollectiontoexport'] = 'Du skal vælge mindst én samling for at eksportere';
$string['youmustselectatleastoneviewtoexport'] = 'Du skal vælge mindst én visning for at eksportere';
$string['zipnotinstalled'] = 'Dit system har ikke zip kommandoen. Installer zip for at slå denne funktion til';

?>
