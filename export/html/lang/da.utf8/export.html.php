<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage export.html
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'Uafhængig HTML webside';
$string['description'] = 'Opretter en uafhængig webside med dataen fra din portefølje. Du kan ikke importere denne igen, men den kan læses i alle standard internet browsere.';
$string['usersportfolio'] = '%s - Portfolie';

$string['preparing'] = 'Forbereder %s';
$string['exportingdatafor'] = 'Eksporterer data for %s';
$string['buildingindexpage'] = 'Bygger indeksside';
$string['copyingextrafiles'] = 'Kopierer ekstra filer';
?>
