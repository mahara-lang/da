<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage interaction-forum
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

$string['addpostsuccess'] = 'Indlægget blev tilføjet';
$string['addtitle'] = 'Tilføj forum';
$string['addtopic'] = 'Tilføj emne';
$string['addtopicsuccess'] = 'Emnet er blevet tilføjet';
$string['autosubscribeusers'] = 'Tilmeld brugere automatisk?';
$string['autosubscribeusersdescription'] = 'Vælg om gruppens brugere automatisk skal abonnere på dette forum';
$string['Body'] = 'Brødtekst';
$string['cantaddposttoforum'] = 'Du har ikke tilladelse til at lave indlæg i dette forum';
$string['cantaddposttotopic'] = 'Du har ikke tilladelse til at lave indlæg i dette emne';
$string['cantaddtopic'] = 'Du har ikke tilladelse til at oprette enmer på dette forum';
$string['cantdeletepost'] = 'Du har ikke tilladelse til at slette indlæg på dette forum';
$string['cantdeletethispost'] = 'Du har ikke tilladelse til at slette dette indlæg';
$string['cantdeletetopic'] = 'Du har ikke tilladelse til at slette enmer på dette forum';
$string['canteditpost'] = 'Du har ikke tilladelse til at redigere dette indlæg';
$string['cantedittopic'] = 'Du har ikke tilladelse til at redigere dette emne';
$string['cantfindforum'] = 'Kunne ikke finde forum med id %s';
$string['cantfindpost'] = 'Kunne ikke finde indlæg med id %s';
$string['cantfindtopic'] = 'Kunne ikke finde emne med id %s';
$string['cantviewforums'] = 'Du har ikke tilladelse til at se forummer i denne gruppe';
$string['cantviewtopic'] = 'Du har ikke tilladelse til at se emner i dette forum';
$string['chooseanaction'] = 'Vølg en handling';
$string['clicksetsubject'] = 'Klik for at Click vælge et emne';
$string['Closed'] = 'Lukket';
$string['Close'] = 'Luk';
$string['closeddescription'] = 'Lukkede emner kan kun besvares af moderatorer og gruppens administratorer';
$string['Count'] = 'Antal'; // Kan også være "Tæl" eller "Greve". Nok ikke den sidste, men den skal ses i kontekst.
$string['createtopicusersdescription'] = 'Hvis sat til "Alle gruppemedlemmer" kan alle oprette nye emner og besvare eksisterende emner.  Hvis sat til "Kun moderatorer og gruppeadministratorer" kan kun moderatorer og gruppeadministratorer oprette nye emner, men alle brugere kan besvare eksiterende emner.';
$string['currentmoderators'] = 'Nuværende moderatorer';
$string['defaultforumtitle'] = 'Generel diskussion';
$string['defaultforumdescription'] = '%s generelt diskussions forum';
$string['deleteforum'] = 'Slet forum';
$string['deletepost'] = 'Slet indlæg';
$string['deletepostsuccess'] = 'Indlægget er blevet slettet';
$string['deletepostsure'] = 'Er du sikker på du vil gøre dette? Det kan ikke fortrydes.';
$string['deletetopic'] = 'Slet emne';
$string['deletetopicvariable'] = 'Slet emne \'%s\'';
$string['deletetopicsuccess'] = 'Emnet er blevet slettet';
$string['deletetopicsure'] = 'Er du sikker på du vil gøre dette? Det kan ikke fortrydes.';
$string['editpost'] = 'Rediger indlæg';
$string['editpostsuccess'] = 'Indlægger er blevet redigeret';
$string['editstothispost'] = 'Ændringer i dette indlæg:';
$string['edittitle'] = 'Rediger forum';
$string['edittopic'] = 'Rediger emne';
$string['edittopicsuccess'] = 'Emnet er blevet redigeret';
$string['forumname'] = 'Forum navn';
$string['forumposthtmltemplate'] = "<div style=\"padding: 0.5em 0; border-bottom: 1px solid #999;\"><strong>%s af %s</strong><br>%s</div>

<div style=\"margin: 1em 0;\">%s</div>

<div style=\"font-size: smaller; border-top: 1px solid #999;\">
<p><a href=\"%s\">Besvar dette indløg online</a></p>
<p><a href=\"%s\">Afmeld fra dette %s</a></p>
</div>";
$string['forumposttemplate'] = "%s af %s
%s
------------------------------------------------------------------------

%s

------------------------------------------------------------------------
For at se og besvare dette indlæg online, følg dette link:
%s

For at afmelde fra dette %s, besøg:
%s";
$string['forumsuccessfulsubscribe'] = 'Abonnement på forum tilmeldt';
$string['forumsuccessfulunsubscribe'] = 'Abonnement på forum afmeldt';
$string['gotoforums'] = 'Gå til forummer';
$string['groupadmins'] = 'Gruppeadministratorere';
$string['groupadminlist'] = 'Gruppeadmins:';
$string['Key'] = 'Nøgle'; // Skal også ses i kontekst
$string['lastpost'] = 'Sidste indlæg';
$string['latestforumposts'] = 'Nyeste forum indlægs';
$string['Moderators'] = 'Moderatorer';
$string['moderatorsandgroupadminsonly'] = 'Kun moderatorer og gruppeadministratorer';
$string['moderatorslist'] = 'Moderatorer:';
$string['moderatorsdescription'] = 'Moderatorer kan redigere og slette emner og indlæg. De kan også åbne, lukke emner samt markere og fjerne emner som fremhævede';
$string['name'] = 'Forum';
$string['nameplural'] = 'Fora';
$string['newforum'] = 'Nyt forum';
$string['newforumpostnotificationsubject'] = '%s: %s: %s';
$string['newpost'] = 'Nyt indlæg: ';
$string['newtopic'] = 'Nyt emne';
$string['noforumpostsyet'] = 'Der er ingen nye indlæg i denne gruppe endnu';
$string['noforums'] = 'Der er ingen fora i denne gruppe';
$string['notopics'] = 'Der er ingen emner i dette forum';
$string['Open'] = 'Åbe';
$string['Order'] = 'Rækkefølge'; // Skal kontekst checkes
$string['orderdescription'] = 'Vælg hvilken rækkefølge dette forum skal ses i i forhold til de andre fora';
$string['Post'] = 'Indlæg';
$string['postbyuserwasdeleted'] = 'Et indlæg af %s blev slettet';
$string['postdelay'] = 'Indlægsforsinkelse';
$string['postdelaydescription'] = 'Den minimale tid (i minutter), der skal gå før et nyt indlæg kan blive mailet ud til abonnenter på forummet.  Forfatteren af indlægget kan redigere i det i dette tidsrum.';
$string['postedin'] = '%s lavede et indlæg i %s';
$string['Poster'] = 'Afsender';
$string['postreply'] = 'Send svar';
$string['Posts'] = 'Indlæg';
$string['allposts'] = 'Alle indlæg';
$string['postsvariable'] = 'Indlæg: %s';
$string['potentialmoderators'] = 'Potentielle moderatorer';
$string['re'] = 'Re: %s'; // Efterladt som "Re:", men kunne også være "Sv:". Beholdte "Re:" for at undgå et emne kaldet "Sv: Re: Sv: Re: Emne" hvis brugerne bruger forskellige sprog.
$string['regulartopics'] = 'Normale emner';
$string['Reply'] = 'Svar';
$string['replyforumpostnotificationsubject'] = 'Re: %s: %s: %s';
$string['replyto'] = 'Svar til: ';
$string['Sticky'] = 'Fremhævet';
$string['stickydescription'] = 'Fremhævede emner er øverst på alle sider';
$string['stickytopics'] = 'Fremhævede emner';
$string['Subscribe'] = 'Abonner';
$string['Subscribed'] = 'Afmeld';
$string['subscribetoforum'] = 'Abonner på forum';
$string['subscribetotopic'] = 'Abonner på emne';
$string['Subject'] = 'Emne';
$string['Topic'] = 'Emne';
$string['Topics'] = 'Emner';
$string['topiclower'] = 'emne';
$string['topicslower'] = 'emne';
$string['topicclosedsuccess'] = 'Emnerne er blevet lukket';
$string['topicisclosed'] = 'Dette emne er lukket. Kun moderatorer og gruppeadministratorer kan tilføje nye svar';
$string['topicopenedsuccess'] = 'Emnerne er blevet åbnet';
$string['topicstickysuccess'] = 'Emnerne er blevet fremhævede';
$string['topicsubscribesuccess'] = 'Abonnetment på emnerne er blevet tilmeldt';
$string['topicsuccessfulunsubscribe'] = 'Abonnement på emnet er blevet afmeldt';
$string['topicunstickysuccess'] = 'Fremhæning af emnet er blevet fjernet';
$string['topicunsubscribesuccess'] = 'Abonnement på emnerne er blevet afmeldt';
$string['topicupdatefailed'] = 'Emneopdatering mislykkedes';
$string['typenewpost'] = 'Nyt forum indlæg';
$string['Unsticky'] = 'Fjern fremhævning'; // Er der ikke et antonym for frehæv?
$string['Unsubscribe'] = 'Afmeld abonnement';
$string['unsubscribefromforum'] = 'Afmeld fra forum abonnement';
$string['unsubscribefromtopic'] = 'Afmeld fra emne abonnement';
$string['updateselectedtopics'] = 'Opdater valgte emner';
$string['whocancreatetopics'] = 'Hvem kan oprette emner';
$string['youcannotunsubscribeotherusers'] = 'Du kan ikke afmelde andre burgeres abonnement';
$string['youarenotsubscribedtothisforum'] = 'Du abonnerer ikke på dette forum';
$string['youarenotsubscribedtothistopic'] = 'Du abonnerer ikke på dette emne';

$string['today'] = 'I dag';
$string['yesterday'] = 'I går';
$string['strftimerecentrelative'] = '%%v, %%k:%%M';
$string['strftimerecentfullrelative'] = '%%v, %%l:%%M %%p';

$string['indentmode'] = 'Indryknings forum layout'; // Den er lidt søgt, men jeg tror den går
$string['indentfullindent'] = 'Udvid helt';
$string['indentmaxindent'] = 'Udvid til maksimum';
$string['indentflatindent'] = 'Ingen indrykninger';
$string['indentmodedescription'] = 'Vælg hvordan emner i dette forums skal indrykkes.';
$string['maxindent'] = 'Maksimal indrykningsniveau';
$string['maxindentdescription'] = 'Indstiller det maksimale indrykningsniveau for et emne. Dette har kun en effekt hvis indryknings layoutet er indstillet til at udvide til maksimum.';

$string['closetopics'] = 'Luk nye emner';
$string['closetopicsdescription'] = 'Hvis valg, vil alle nye emner i dette forum være lukket som standard.  Kun moderatorer og gruppeadministratorer kan tilføje nye svar til lukkede emner.';

