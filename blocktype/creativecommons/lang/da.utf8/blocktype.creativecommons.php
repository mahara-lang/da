<?php
/**
 * Creative Commons License Block type for Mahara
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage blocktype-creativecommons
 * @author     Francois Marier <francois@catalyst.net.nz>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2009 Catalyst IT Ltd
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'Creative Commons Licens';
$string['description'] = 'Vedlæg en Creative Commons licens til din visning';
$string['blockcontent'] = 'Blokindhold'; // Kan enten være en blok eller en blokereing.

$string['alttext'] = 'Creative Commons Licens';
$string['licensestatement'] = "Dette værk er givet i licens med en <a rel=\"license\" href=\"%s\">Creative Commons %s 3.0 Unported License</a>.";
$string['sealalttext'] = 'This license is acceptable for Free Cultural Works.'; // Ikke oversat, da Free Cultural Works er et meget bestemt begreb, som jeg ikke har kunnet finde en dasnk version af. Plus, så står de på engelsk på seglet.

$string['config:noncommercial'] = 'Tillad kommercielt brug af dit værk?';
$string['config:noderivatives'] = 'Tillad bearbejdelse af di værk?';
$string['config:sharealike'] = 'Ja, så længe andre deler på samme vilkår';

$string['by'] = 'Navngivelse';
$string['by-sa'] = 'Navngivelse - Del på samme vilkår';
$string['by-nd'] = 'Navngivelse - Ingen bearbejdelse';
$string['by-nc'] = 'Navngivelse - Ikke-kommerciel';
$string['by-nc-sa'] = 'Navngivelse - Ikke-kommerciel - Del på samme vilkår';
$string['by-nc-nd'] = 'Navngivelse - Ikke-kommerciel - Ingen bearbejdelse';
// Creative Commons termer taget fra www.creativecommons.dk. De burde være rigtige.
?>
