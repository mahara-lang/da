<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage blocktype-wall
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'Væg';
$string['otherusertitle'] = "%ss væg";
$string['description'] = 'Viser et område, hvor folk kan give dig kommentarer';
$string['noposts'] = 'Ingen vægopslag.';
$string['makeyourpostprivate'] = 'Gør dit indlæg privat?';
$string['viewwall'] = 'Vis væg';
$string['backtoprofile'] = 'Tilbage til profil';
$string['wall'] = 'Væg';
$string['wholewall'] = 'Se hele væggen';
$string['reply'] = 'svar';
$string['delete'] = 'slet indlæg';
$string['deletepost'] = 'Slet indlæg';
$string['Post'] = 'Indlæg';
$string['deletepostsure'] = 'Er du sikker på at du vil gøre dette? Det kan ikke fortrydes.';
$string['deletepostsuccess'] = 'Indlægget er blevet slettet';
$string['maxcharacters'] = "Maksimalt %s tegn per indlæg.";
$string['sorrymaxcharacters'] = "Beklager, dit indlæg kan ikke være mere end %s tegn langt.";

// Config strings
$string['postsizelimit'] = "Indlægsstørrelse begrænsning";
$string['postsizelimitdescription'] = "Du kan begrænse størrelsen på vægindlæg her. Eksisterende indlæg vil ikke blive ændret";
$string['postsizelimitmaxcharacters'] = "Maksimalt antal tegn";
$string['postsizelimitinvalid'] = "Dette er ikke et gyldigt tal.";
$string['postsizelimittoosmall'] = "Grænsen kan ikke være lavere end nul.";

?>
