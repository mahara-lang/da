<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage blocktype-externalfeeds
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'Extern feed';
$string['description'] = 'Indsæt er eksternt RSS eller ATOM feed';
$string['feedlocation'] = 'Feed placering';
$string['feedlocationdesc'] = 'URL til et gyldigt RSS feed ATOM feed';
$string['itemstoshow'] = 'Objekter, der skal vises';
$string['itemstoshowdescription'] = 'Mellem 1 og 20';
$string['showfeeditemsinfull'] = 'Skal feedobjekterne vises fuldt?';
$string['showfeeditemsinfulldesc'] = 'Om der skal vises en opsummering af feedobjekterne, eller om den fulde teskt fra hver også skal vises.';
$string['invalidurl'] = 'Den URL er ugyldig. Du kan kun vise feeds fra http og https adresser.';
$string['invalidfeed'] = 'Den feed ser ugyldig ud. Den rapporterede fejl var: %s';
$string['lastupdatedon'] = 'Sidst opdateret den %s';
$string['defaulttitledescription'] = 'Hvis du efterlade dette blankt, vil feedets navn blive brugt';
?>
