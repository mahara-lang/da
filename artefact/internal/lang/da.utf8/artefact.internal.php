<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['pluginname'] = 'Profil';

$string['profile'] = 'Profil';
$string['myfiles'] = 'Mine filer';

$string['mandatory'] = 'Påkrævet';
$string['public'] = 'Offentlig';

$string['aboutdescription'] = 'Indtast dit rigtige fornavn og efternavn her. Hvis du vil vise et andet navn til andre brugere, så indtast det som dit viste navn.';
$string['infoisprivate'] = 'Denne information er privat indtil du inkluderer den i en visning, der deles med andre.';
$string['viewmyprofile'] = 'Vis min profil';

// profile categories
$string['aboutme'] = 'Om mig';
$string['contact'] = 'Kontaktinformation';
$string['messaging'] = 'Chat';
$string['general'] = 'Generelt';

// profile fields
$string['firstname'] = 'Fornavn';
$string['lastname'] = 'Efternavn';
$string['fullname'] = 'Fuldt navn';
$string['institution'] = 'Institution';
$string['studentid'] = 'Studienummer';
$string['preferredname'] = 'Vist navn';
$string['introduction'] = 'Introduktion';
$string['email'] = 'E-mail-adresse';
$string['maildisabled'] = 'E-mail slået fra';
$string['officialwebsite'] = 'Firma hjemmeside';
$string['personalwebsite'] = 'Personlig hjemmeside';
$string['blogaddress'] = 'Blog';
$string['address'] = 'Postadresse';
$string['town'] = 'By';
$string['city'] = 'Region';
$string['country'] = 'Land';
$string['homenumber'] = 'Hjemmetelefon';
$string['businessnumber'] = 'Arbejdstelefon';
$string['mobilenumber'] = 'Mobiltelefon';
$string['faxnumber'] = 'Fax';
$string['icqnumber'] = 'ICQ nummer';
$string['msnnumber'] = 'Windows Live Messenger';
$string['aimscreenname'] = 'AIM kaldenavn';
$string['yahoochat'] = 'Yahoo! Chat';
$string['skypeusername'] = 'Skype navn';
$string['jabberusername'] = 'Jabber navn';
$string['occupation'] = 'Stilling';
$string['industry'] = 'Branche';

// Field names for view user and search user display
$string['name'] = 'Navn';
$string['principalemailaddress'] = 'Primær e-mail';
$string['emailaddress'] = 'Alternativ e-mail';

$string['saveprofile'] = 'Gem profil';
$string['profilesaved'] = 'Profilen er blevet gemt';
$string['profilefailedsaved'] = 'Profilen blev ikke gemt!';


$string['emailvalidation_subject'] = 'E-mail validering';
$string['emailvalidation_body'] = <<<EOF
Kære %s,

Du har tilføjet e-mail-adressen %s til din brugerkonto i Mahara. Besøg linket nedenfor for at aktivere denne adresse.

%s

Hvis dette er din adresse, men du ikke har ønsket den tilføjet til Mahara, så besøg linket nedenfor til at afvise e-mail aktivering.

%s
EOF;

$string['validationemailwillbesent'] = 'en validerings e-mail vil blive sendt når du gemmer din profil';
$string['validationemailsent'] = 'en validerings e-mail er blevet sendt';
$string['emailactivation'] = 'E-mail aktivering';
$string['emailactivationsucceeded'] = 'E-mail aktivering gennemført';
$string['emailalreadyactivated'] = 'E-mail allerede aktiveret';
$string['emailactivationfailed'] = 'E-mail aktivering mislykkedes';
$string['emailactivationdeclined'] = 'E-mail aktivering afvist';
$string['verificationlinkexpired'] = 'Verificeringslink er udløbet';
$string['invalidemailaddress'] = 'Ugyldig e-mail-adresse';
$string['unvalidatedemailalreadytaken'] = 'E-mail-adressen som du prøver at validere er allerede i brug';
$string['addbutton'] = 'Tilføj';

$string['emailingfailed'] = 'Profil gemt, men e-mails blev ikke sendt til: %s';

$string['loseyourchanges'] = 'Mist dine ændringer?';

$string['editprofile'] = 'Rediger profil';
$string['editmyprofile'] = 'Rediger min profil';
$string['Title'] = 'Titel';

$string['Created'] = 'Oprettet';
$string['Description'] = 'Beskrivelse';
$string['Download'] = 'Download';
$string['lastmodified'] = 'Sidst ændret';
$string['Owner'] = 'Ejer';
$string['Preview'] = 'Eksempel';
$string['Size'] = 'Størrelse';
$string['Type'] = 'Type';

$string['profileinformation'] = 'Profil information';
$string['profilepage'] = 'Profil side';
$string['viewprofilepage'] = 'Vis profil side';
$string['viewallprofileinformation'] = 'Vis al profil information';

?>
