<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage blocktype-profileinfo
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'Profil information';
$string['description'] = 'Vælg hvilke profil informationer, der skal vises';

$string['aboutme'] = 'Om mig';
$string['fieldstoshow'] = 'Felter der skal vises';
$string['introtext'] = 'Introduktions tekst';
$string['useintroductioninstead'] = 'Du kan bruge informationsfeltet fra din profil i stedet, ved at slå det til og lade dette felt stå tomt';
$string['dontshowprofileicon'] = "Vis ikke et profil-ikon";
$string['dontshowemail'] = "Vis ikke e-mail-adresse";
$string['uploadaprofileicon'] = "Du har ikke nogle profil-ikoner. <a href=\"%sartefact/file/profileicons.php\" target=\"_blank\">Upload et</a>";

?>
