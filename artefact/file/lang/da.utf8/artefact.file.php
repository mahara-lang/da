<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['pluginname'] = 'Filer';

$string['sitefilesloaded'] = 'Webside filer indlæst';
$string['addafile'] = 'Tilføj fil';
$string['archive'] = 'Arkiv';
$string['bytes'] = 'bytes';
$string['cannoteditfolder'] = 'Du har ikke tilladelse til at tilføje indhold til denne mappe';
$string['cannoteditfoldersubmitted'] = 'Du kan ikke tilføje indhold til en mappe i en indsent visning.';
$string['cannotremovefromsubmittedfolder'] = 'Du kan ikke fjerne indhold til en mappe i en indsent visning';
$string['changessaved'] = 'Ændringer gemt';
$string['clickanddragtomovefile'] = 'Klik og træk for at flytte %s';
$string['contents'] = 'Indhold';
$string['copyrightnotice'] = 'Ophavsretserklæring'; // Hedder det her mon noget helt specielt juramæssigt?
$string['create'] = 'Opret';
$string['Created'] = 'Oprettede';
$string['createfolder'] = 'Opret mappe';
$string['confirmdeletefile'] = 'Er du sikker på, at du vil slette denne fil?';
$string['confirmdeletefolder'] = 'Er du sikker på, at du vil slette denne mappe?';
$string['confirmdeletefolderandcontents'] = 'Er du sikker på, at du vil slette denne mappe og al dens indhold?';
$string['customagreement'] = 'Speciel aftale';
$string['Date'] = 'Dato';
$string['defaultagreement'] = 'Standard aftale';
$string['defaultquota'] = 'Standard kvote';
$string['defaultquotadescription'] = 'Du kan angive den mængde diskplads som nye brugere vil få som deres kvote her. Nuværende brugeres kvoter vil ikke blive ændret.';
$string['maxquotaenabled'] = 'Håndhæv en maksimal kvote for hele siden';
$string['maxquota'] = 'Maksimal kvote';
$string['maxquotatoolow'] = 'Den maksimale kvote kan ikke være mindre end standard kvoten.';
$string['maxquotaexceeded'] = 'Du angav en kvote over den maksimale tilgængelige indstilling for denne side.(%s). Prøv en mindre værdi eller kontakt side administratoren for at øge maksimum kvoten.';
$string['maxquotaexceededform'] = 'Angiv en fil kvote på mindre end %s.';
$string['maxquotadescription'] = 'Du kan indstille den maksimale kvote, som en administrator kan tildele en bruger. Nuværende brugeres kvoter vil ikke blive ændret.';
$string['deletingfailed'] =  'Sletning mislykkedes: Fil eller mappe eksisterer ikke længere';
$string['deletefile?'] = 'Er du sikker på, at du vil slette denne fil?';
$string['deletefolder?'] = 'Er du sikker på, at du vil slette denne mappe?';
$string['Description'] = 'Beskrivelse';
$string['destination'] = 'Destination';
$string['Details'] = 'Detalier';
$string['Download'] = 'Download';
$string['downloadfile'] = 'Download %s';
$string['downloadoriginalversion'] = 'Download den originale version';
$string['editfile'] = 'Rediger fil';
$string['editfolder'] = 'Rediger mappe';
$string['editingfailed'] = 'Redigering mislykkedes: Fil eller mappe eksisterer ikke længere';
$string['emptyfolder'] = 'Tom mappe';
$string['file'] = 'Fil'; // Capitalised to be consistent with names of all the other artefact types
$string['File'] = 'Fil';
$string['fileadded'] = 'Fil valgt';
$string['filealreadyindestination'] = 'Den flyttede fil er allerede i den mappe';
$string['fileappearsinviews'] = 'Filen er en del af en eller flere af dine visninger.';
$string['fileattached'] = 'Filen er vedhætet til %s andre ting i dit portefølje.';
$string['fileremoved'] = 'Fil fjernet';
$string['files'] = 'filer';
$string['Files'] = 'Filer';
$string['fileexists'] = 'Fil findes allerede';
$string['fileexistsoverwritecancel'] =  'En fil med det navn eksisterer allerede.  Du kan prøve et andet navn eller overskrive den eksisterende fil.';
$string['filelistloaded'] = 'Fil liste indlæst';
$string['filemoved'] = 'Filen er blevet flyttet';
$string['filenamefieldisrequired'] = 'Fil feltet er påkrævet';
$string['fileinstructions'] = 'Upload dine billeder, dokumenter eller andre filer for at inkludere dem i dine visninger. Træk og slip ikonerne for at flytte filer mellem mapper.';
$string['filethingdeleted'] = '%s slettet';
$string['filewithnameexists'] = 'En fil eller mappe med navnet "%s" findes allerede.';
$string['folder'] = 'mappe';
$string['Folder'] = 'Mappe';
$string['folderappearsinviews'] = 'Denne mappe er en del af en eller flere af dine visninger.';
$string['Folders'] = 'Mapper';
$string['foldernotempty'] = 'Denne mappe er ikke tom.';
$string['foldercreated'] = 'Mappe oprettet';
$string['foldernamerequired'] = 'Angiv et navn til den nye mappe.';
$string['gotofolder'] = 'Gå til %s';
$string['groupfiles'] = 'Gruppe filer';
$string['home'] = 'Hjem';
$string['htmlremovedmessage'] = 'Du ser på <strong>%s</strong> af <a href="%s">%s</a>. Den viste fil nedenfor er filtreret for at fjerne skadeligt indhold, og er kun en grov repræsentation af originalen.';
$string['htmlremovedmessagenoowner'] = 'Du ser på <strong>%s</strong>. Den viste fil nedenfor er filtreret for at fjerne skadeligt indhold, og er kun en grov repræsentation af originalen.';
$string['image'] = 'Billede';
$string['lastmodified'] = 'Sidst ændret';
$string['myfiles'] = 'Mine filer';
$string['Name'] = 'Navn';
$string['namefieldisrequired'] = 'Navne feltet er påkrævet';
$string['maxuploadsize'] = 'Maksimal upload størrelse';
$string['movefaileddestinationinartefact'] = 'Du kan ikke placere in mappe i den selv.';
$string['movefaileddestinationnotfolder'] = 'Du kan kun flytte filer in i mapper.';
$string['movefailednotfileartefact'] = 'Kun fil, folder og billede artefakter kan flyttes.';
$string['movefailednotowner'] = 'Du har ikke tilladelse til at flytte filen ind i denne mappe.';
$string['movefailed'] = 'Flytning mislykkedes.';
$string['movingfailed'] = 'Flytning mislykkedes: Fil eller mappe eksisterer ikke længere';
$string['nametoolong'] = 'Det navn er for langt. Vælg et kortere navn.';
$string['nofilesfound'] = 'Ingen filer fundet';
$string['notpublishable'] = 'Du har ikke tilladelse til at offentliggøre denne fil';
$string['overwrite'] = 'Overskriv';
$string['Owner'] = 'Ejer';
$string['parentfolder'] = 'Overmappe'; // Lyder forkert...
$string['Preview'] = 'Eksempel'; // Lyder til gengæld rigtigt.
$string['requireagreement'] = 'Kræv aftale';
$string['removingfailed'] = 'Fjernelse mislykkedes: Fil eller mappe eksisterer ikke længere';
$string['savechanges'] = 'Gem ændringer';
$string['selectafile'] = 'Vælg en fil';
$string['selectingfailed'] = 'Valg mislykkedes: Fil eller mappe eksisterer ikke længere';
$string['Size'] = 'Størrelse';
$string['spaceused'] = 'Pladsforbrug';
$string['timeouterror'] = 'Fil upload mislykkeds: Prøv at uploade filen igen';
$string['title'] = 'Navn';
$string['titlefieldisrequired'] = 'Navnefeltet er påkrævet';
$string['Type'] = 'Type';
$string['upload'] = 'Upload';
$string['uploadagreement'] = 'Upload aftale';
$string['uploadagreementdescription'] = 'Aktiver denne funktion hvis du vil kræve at brugerne accepterer teksten denfor før de kan uploade filer til siden.';
$string['uploadexceedsquota'] = 'Hvis du uploadede denne fil ville du overskride din disk kvote. Prøv at slette nogle af dine uploadede filer.';
$string['uploadfile'] =  'Upload fil';
$string['uploadfileexistsoverwritecancel'] =  'En fil med det navn findes allerede.  Du kan omdøbe den fil du er ved at uploade eller overskrive den eksiserende fil.';
$string['uploadingfiletofolder'] =  'Uploader %s til %s';
$string['uploadoffilecomplete'] = 'Upload af %s udført';
$string['uploadoffilefailed'] =  'Upload af %s miskykkedes';
$string['uploadoffiletofoldercomplete'] = 'Upload af %s til %s udført';
$string['uploadoffiletofolderfailed'] = 'Upload af %s til %s mislykkedes';
$string['usecustomagreement'] = 'Brug speciel aftale';
$string['youmustagreetothecopyrightnotice'] = 'Du skal acceptere ophavsretserklæringen';
$string['fileuploadedtofolderas'] = '%s uploadet til %s som "%s"';
$string['fileuploadedas'] = '%s uploadet som "%s"';


// File types
$string['ai'] = 'Postscript Dokument';
$string['aiff'] = 'AIFF Audio Fil';
$string['application'] = 'Ukendt Applikation';
$string['au'] = 'AU Audio Fil';
$string['avi'] = 'AVI Video Fil';
$string['bmp'] = 'Bitmap Billede';
$string['doc'] = 'MS Word Dokument';
$string['dss'] = 'Digital Speech Standard Sound Fil';
$string['gif'] = 'GIF Billede';
$string['html'] = 'HTML Fil';
$string['jpg'] = 'JPEG Billede';
$string['jpeg'] = 'JPEG Billede';
$string['js'] = 'Javascript Fil';
$string['latex'] = 'LaTeX Dokument';
$string['m3u'] = 'M3U Audio Fil';
$string['mp3'] = 'MP3 Audio Fil';
$string['mp4_audio'] = 'MP4 Audio Fil';
$string['mp4_video'] = 'MP4 Video Fil';
$string['mpeg'] = 'MPEG Film';
$string['odb'] = 'Openoffice Database';
$string['odc'] = 'Openoffice Calc Fil';
$string['odf'] = 'Openoffice Formula Fil';
$string['odg'] = 'Openoffice Graphics Fil';
$string['odi'] = 'Openoffice Billede';
$string['odm'] = 'Openoffice Master Dokument Fil';
$string['odp'] = 'Openoffice Præsentation';
$string['ods'] = 'Openoffice Regneark';
$string['odt'] = 'Openoffice Dokument';
$string['oth'] = 'Openoffice Web Dokument';
$string['ott'] = 'Openoffice Skabelon Dokument';
$string['pdf'] = 'PDF Dokument';
$string['png'] = 'PNG Billede';
$string['ppt'] = 'MS Powerpoint Dokument';
$string['quicktime'] = 'Quicktime Film';
$string['ra'] = 'Real Audio Fil';
$string['rtf'] = 'RTF Dokument';
$string['sgi_movie'] = 'SGI Movie Fil';
$string['sh'] = 'Shell Script';
$string['tar'] = 'TAR Arkiv';
$string['gz'] = 'Gzip Komprimeret Fil';
$string['bz2'] = 'Bzip2 Komprimeret Fil';
$string['txt'] = 'Rå Tekst Fil';
$string['wav'] = 'WAV Audio Fil';
$string['wmv'] = 'WMV Video Fil';
$string['xml'] = 'XML Fil';
$string['zip'] = 'ZIP Arkiv';
$string['swf'] = 'SWF Flash Film';
$string['flv'] = 'FLV Flash Film';
$string['mov'] = 'MOV Quicktime Film';
$string['mpg'] = 'MPG Film';
$string['ram'] = 'RAM Real Player Film';
$string['rpm'] = 'RPM Real Player Film';
$string['rm'] = 'RM Real Player Film';


// Profile icons
$string['cantcreatetempprofileiconfile'] = 'Kunne ikke lave midlertidigt profil-ikon billede i %s';
$string['profileiconsize'] = 'Profil-ikon størrelse';
$string['profileicons'] = 'Profil-ikoner';
$string['Default'] = 'Standard';
$string['deleteselectedicons'] = 'Slet valgte ikoner';
$string['profileicon'] = 'Profil-ikon';
$string['noimagesfound'] = 'Ingen billeder fundet';
$string['uploadedprofileiconsuccessfully'] = 'Upload af nyt profil-ikon udført';
$string['profileiconsetdefaultnotvalid'] = 'Kunne ikke indstille standard profil-ikonet, valget var ikke gyldigt';
$string['profileiconsdefaultsetsuccessfully'] = 'Standard profil-ikon indstillet';
$string['profileiconsdeletedsuccessfully'] = 'Profil-ikon(er) slettet';
$string['profileiconsnoneselected'] = 'Ingen profil-ikoner valgt til sletning';
$string['onlyfiveprofileicons'] = 'Du må kun uploade fem profil-ikoner';
$string['or'] = 'eller';
$string['profileiconuploadexceedsquota'] = 'Hvis du uploadede dette profil-ikon ville du overskride din disk kvote. Prøv at slette nogle af dine uploadede filer.';
$string['profileiconimagetoobig'] = 'Billedet du uploadede var for stort (%sx%s pixels). Det må ikke være større end %sx%s pixels';
$string['uploadingfile'] = 'uploader fil...';
$string['uploadprofileicon'] = 'Upload Profil-ikon';
$string['profileiconsiconsizenotice'] = 'Du må uplade op til <strong>fem</strong> profil-ikoner her, og vælge et som skal vises som dit standardikon til hver en tid. Dine ikoner skal være mellem 16x16 og %sx%s pixels store.';
$string['setdefault'] = 'Indstil standard';
$string['Title'] = 'Titel';
$string['imagetitle'] = 'Billedtitel';
$string['usenodefault'] = 'Brug ingen standard';
$string['usingnodefaultprofileicon'] = 'Bruger nu intet standardikon';
$string['wrongfiletypeforblock'] = 'Filen du uploadede var ikke den korrekte type til denne blok.';

// Unzip
$string['Contents'] = 'Indhold';
$string['Continue'] = 'Fortsæt';
$string['extractfilessuccess'] = 'Oprettede %s mapper and %s filer.';
$string['filesextractedfromarchive'] = 'Filer pakket ud fra arkiv';
$string['filesextractedfromziparchive'] = 'Filer pakket ud fra Zip arkiv';
$string['fileswillbeextractedintofolder'] = 'Filer bliver pakket ud i %s';
$string['insufficientquotaforunzip'] = 'Din resterende fil kvote er for likke til at pakke denne fil ud.';
$string['invalidarchive'] = 'Felj under læsning af arkiv fil.';
$string['pleasewaitwhileyourfilesarebeingunzipped'] = 'Vent venligst mens dine filer bliver pakket ud.';
$string['spacerequired'] = 'Pladskrav';
$string['Unzip'] = 'Pak ud';
$string['unzipprogress'] = '%s filer/mapper oprettet.';
?>
