<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage blocktype-internalmedia
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'Indsat medie'; // Hmm, kan ikke lide den oversættelse...
$string['description'] = 'Vælg filer til indsat visning';

$string['media'] = 'Medie';
$string['flashanimation'] = 'Flash-animation';

$string['typeremoved'] = 'Denne blok henviser til en medietype som er blevet forbudt af administratoren.';
$string['configdesc'] = 'Vælg hvilke filtyber brugerne kan indsætte i denne blok. Hvis du forbyder en filetype der allerede er blevet brugt i en blok, vil den ikke blive vist længere.';
?>
