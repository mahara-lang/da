<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['pluginname'] = 'Kommentar';
$string['Comment'] = 'Kommentar';
$string['Comments'] = 'Kommentarer';
$string['comment'] = 'kommentar';
$string['comments'] = 'kommentarer';

$string['Allow'] = 'Tillad';
$string['allowcomments'] = 'Tillad kommentarer';
$string['approvalrequired'] = 'Kommenterer er modererede så hvis du vil gøre denne kommentar offentlig, vil den ikke være synlig for andre før ejeren har godkendt den.';
$string['attachfile'] = "Vedhæft fil";
$string['Attachments'] = "Vedhæftninger";
$string['cantedithasreplies'] = 'Du kan kun redigere den nyeste kommentar';
$string['canteditnotauthor'] = 'Du er ikke forfatteren til denne kommentar';
$string['cantedittooold'] = 'Du kan kun redigere kommentarer, der er mindre end %d minutter gamle';
$string['commentmadepublic'] = "Kommentar offentliggjort";
$string['commentdeletedauthornotification'] = "Din kommentar på %s blev slettet:\n%s";
$string['commentdeletednotificationsubject'] = 'Kommentar på %s slettet';
$string['commentnotinview'] = 'Kommentaren %d er ikke i visning %d';
$string['commentremoved'] = 'Kommentar fjernet';
$string['commentremovedbyauthor'] = 'Kommentar fjernet af forfatteren';
$string['commentremovedbyowner'] = 'Kommentar fjernet af ejeren';
$string['commentremovedbyadmin'] = 'Kommentar fjernet af en administrator';
$string['commentupdated'] = 'Kommentar opdateret';
$string['editcomment'] = 'Rediger kommentar';
$string['editcommentdescription'] = 'Du kan opdatere dine kommentarer, hvis de er mindre end %d minutter gamle og ikke har fået nyere svar.  Derefter kan du stadig være i stand til at slette din komnentarer og tilføje nye.';
$string['entriesimportedfromleapexport'] = 'Optegnelser importeret fra en LEAP eksport, der ikke var i stand til at bliver importeret andetsted';
$string['feedback'] = 'Feedback';
$string['feedbackattachdirname'] = 'commentfiles';
$string['feedbackattachdirdesc'] = 'Filer vedhæftet til kommentarer på din portefølje';
$string['feedbackattachmessage'] = 'De vedhæftede filer bel tilføjet til din %s mappe';
$string['feedbackonviewbyuser'] = 'Feedback om %s af %s';
$string['feedbacksubmitted'] = 'Feedback indsendt';
$string['makepublic'] = 'Offentliggør';
$string['makepublicnotallowed'] = 'Du har ikke tilladelse til at offentliggøre denne kommentar';
$string['makepublicrequestsubject'] = 'Anmod om at gøre en privat kommentar offentlig';
$string['makepublicrequestbyauthormessage'] = '%s anmoder dig om at gøre deres kommentar offentlig.';
$string['makepublicrequestbyownermessage'] = '%s anmoder dig om at gøre din kommentar offentlig.';
$string['makepublicrequestsent'] = 'En besked er blevet sendt til %s for at anmode om at gøre kommentaren offentlig.';
$string['messageempty'] = 'Beskeden er tom. Du kan kun sende en tom besked, hvis du vedhæfter en fil.';
$string['Moderate'] = 'Moderer';
$string['moderatecomments'] = 'Moderer kommentarer';
$string['moderatecommentsdescription'] = 'Kommentarer vil være private indtil du har godkendt dem.';
$string['newfeedbacknotificationsubject'] = 'Ny feedback om %s';
$string['placefeedback'] = 'Giv feedback';
$string['reallydeletethiscomment'] = 'Er du sikker på at du vil slette denne kommentar?';
$string['thiscommentisprivate'] = 'Denne kommentar er privat.';
$string['typefeedback'] = 'Feedback';
$string['youhaverequestedpublic'] = 'Du har anmodet om at denne kommentar bliver gjort offentlig.';

$string['feedbacknotificationhtml'] = "<div style=\"padding: 0.5em 0; border-bottom: 1px solid #999;\"><strong>%s kommenterede på %s</strong><br>%s</div>

<div style=\"margin: 1em 0;\">%s</div>

<div style=\"font-size: smaller; border-top: 1px solid #999;\">
<p><a href=\"%s\">Svar på denne kommentar online</a></p>
</div>";
$string['feedbacknotificationtext'] = "%s kommenterede på %s
%s
------------------------------------------------------------------------

%s

------------------------------------------------------------------------
For at se og besvare denne kommentar online, følg dette link:
%s";
$string['feedbackdeletedhtml'] = "<div style=\"padding: 0.5em 0; border-bottom: 1px solid #999;\"><strong>En kommentar om %s blev fjernet</strong><br>%s</div>

<div style=\"margin: 1em 0;\">%s</div>

<div style=\"font-size: smaller; border-top: 1px solid #999;\">
<p><a href=\"%s\">%s</a></p>
</div>";
$string['feedbackdeletedtext'] = "En kommentar om %s blev fjernet
%s
------------------------------------------------------------------------

%s

------------------------------------------------------------------------
For at se %s online, følg dette link:
%s";
?>
