<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage artefact-resume
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['pluginname'] = 'CV';

// Tabs
$string['introduction'] = 'Introduktion';
$string['educationandemployment'] = 'Uddannelser og ansættelser';
$string['achievements'] = 'Resultater';
$string['goals'] = 'Mål';
$string['skills'] = 'Kompetencer';
$string['interests'] = 'Interesser';

$string['myresume'] = 'Mit CV';
$string['mygoals'] = 'Mine mål';
$string['myskills'] = 'Mine kompetencer';
$string['coverletter'] = 'Følgebrev';
$string['interest'] = 'Interesser';
$string['contactinformation'] = 'Kontaktinformation';
$string['personalinformation'] = 'Personlige oplysninger';
$string['dateofbirth'] = 'Fødselsdato';
$string['placeofbirth'] = 'Fødselssted';
$string['citizenship'] = 'Statsborgerskab';
$string['visastatus'] = 'Visa status';
$string['female'] = 'Kvinde';
$string['male'] = 'Mand';
$string['gender'] = 'Køn';
$string['maritalstatus'] = 'Civilstand';
$string['resumesaved'] = 'CV gemt';
$string['resumesavefailed'] = 'Opdatering af dit CV mislykkedes';
$string['educationhistory'] = 'Uddannelseshistorie';
$string['employmenthistory'] = 'Ansættelseshistorie';
$string['certification'] = 'Certificeringer, akkrediteringer og priser';
$string['book'] = 'Bøger og udgivelser';
$string['membership'] = 'Professionelle medlemskaber';
$string['startdate'] = 'Startdato';
$string['enddate'] = 'Slutdato';
$string['date'] = 'Dato';
$string['position'] = 'Stilling';
$string['qualification'] = 'Kvalifikation';
$string['title'] = 'Titel';
$string['description'] = 'Beskrivelse';
$string['employer'] = 'Arbejdsgiver';
$string['jobtitle'] = 'Jobtitel';
$string['jobdescription'] = 'Jobbeskrivelse';
$string['institution'] = 'Institution';
$string['qualtype'] = 'Kvalifikationstype';
$string['qualname'] = 'Kvalifikationsnavn';
$string['qualdescription'] = 'Kvalifikationsbeskrivelse';
$string['contribution'] = 'Bidrag';
$string['detailsofyourcontribution'] = 'Dit bidrag i detaljer';
$string['compositedeleteconfirm'] = 'Er du sikker på, at du vil slette dette?';
$string['compositesaved'] = 'Det er blevet gemt';
$string['compositesavefailed'] = 'Det blev ikke gemt';
$string['compositedeleted'] = 'Det blev slettet';
$string['backtoresume'] = 'Tilbage til mit CV';
$string['personalgoal'] = 'Personlige mål';
$string['academicgoal'] = 'Akademiske mål';
$string['careergoal'] = 'Karrieremål';
$string['defaultpersonalgoal'] = '';
$string['defaultacademicgoal'] = '';
$string['defaultcareergoal'] = '';
$string['personalskill'] = 'Personlige kompetencer';
$string['academicskill'] = 'Akademiske kompetencer';
$string['workskill'] = 'Faglige kompetencer';
$string['goalandskillsaved'] = 'Mål og kompetencer gemt';
$string['resume'] = 'CV';
$string['current'] = 'Nuværende';
$string['moveup'] = 'Flyt op';
$string['movedown'] = 'Flyt ned';
$string['viewyourresume'] = 'Vis dit CV';
$string['resumeofuser'] = 'CV for %s';
$string['employeraddress'] = 'Arbejdsgiver adresse';
$string['institutionaddress'] = 'Institution adresse';

?>
