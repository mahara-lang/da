<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['pluginname'] = 'Blogs';

$string['blog'] = 'Blog';
$string['blogs'] = 'Blogs';
$string['addblog'] = 'Opret blog';
$string['addpost'] = 'Nyt indlæg';
$string['alignment'] = 'Justering'; // Skal lige holde øje med hvor den her bruges.
$string['allowcommentsonpost'] = 'Tillad kommentarer på dit indlæg.';
$string['allposts'] = 'Alle indlæg';
$string['attach'] = 'Vedhæft';
$string['attachedfilelistloaded'] = 'List over vedhæftede filer indlæst';
$string['attachedfiles'] = 'Vedhæftede filer';
$string['attachment'] = 'Vedhæftning';
$string['attachments'] = 'Vedhæftninger';
$string['blogcopiedfromanotherview'] = 'Bemærk: Denne blok er blevet kopieret fra en anden visning. Du kan flytte og fjerne den, men du kan ikke ændre på hvad %s der er i den.';
$string['blogdesc'] = 'Beskrivelse';
$string['blogdescdesc'] = 'F.eks. ‘En optegnelse af Jills oplevelser og reflektioner’.'; // "Jill"? Burde nok finde et dansk navn at skife ud med. Hvad er de danske Jack og Jill?
$string['blogdoesnotexist'] = 'Du prøver at tilgå en blog som ikke eksisterer';
$string['blogpostdoesnotexist'] = 'Du prøver at tilgå et blogindlæg som ikke eksisterer';
$string['blogpost'] = 'Blogindlæg';
$string['blogdeleted'] = 'Blog slettet';
$string['blogpostdeleted'] = 'Blogindlæg slettet';
$string['blogpostpublished'] = 'Blogindlæg udgivet';
$string['blogpostsaved'] = 'Blogindlæg gemt';
$string['blogsettings'] = 'Blogindstillinger';
$string['blogtitle'] = 'Titel';
$string['blogtitledesc'] = 'F.eks. ‘Jills Omsorgspraktik journal’.'; // Jill igen. Hun er populær. Bør nok lave et helt nyt eksempel her.
$string['border'] = 'Omrids';
$string['cancel'] = 'Annuller';
$string['createandpublishdesc'] = 'Dette vil oprette blogindlægget og gøre det tilgængeligt for andre.';
$string['createasdraftdesc'] = 'Dette vil oprette blogindlægget, men det vil ikke være tilgængeligt for andre før du vælger at udgive det.';
$string['createblog'] = 'Opret blog';
$string['dataimportedfrom'] = 'Data importeret fra %s';
$string['defaultblogtitle'] = '%ss blog';
$string['delete'] = 'Slet';
$string['deleteblog?'] = 'Er du sikker på at du vil slette denne blog?';
$string['deleteblogpost?'] = 'Er du sikke på at du vil slette dette indlæg?';
$string['description'] = 'Beskrivelse';
$string['dimensions'] = 'Dimensioner';
$string['draft'] = 'Kladde';
$string['edit'] = 'REdiger';
$string['editblogpost'] = 'Rediger blogindlæg';
$string['entriesimportedfromleapexport'] = 'Optegnelser importeret fra en LEAP eksport, der ikke var i stand til at bliver importeret andetsted';
$string['errorsavingattachments'] = 'Vedhæftningerne blev ikke gemt, da der opstod en fejl';
$string['horizontalspace'] = 'Vandret plads';
$string['insert'] = 'Indsæt';
$string['insertimage'] = 'Indsæt billede';
$string['moreoptions'] = 'Flere indstillinger';
$string['mustspecifytitle'] = 'Dit indlæg skal have en titel';
$string['mustspecifycontent'] = 'Dit indlæg skal have noget indhold';
$string['myblogs'] = 'Mine blogs';
$string['myblog'] = 'Min blog';
$string['name'] = 'Navn';
$string['newattachmentsexceedquota'] = 'Den samlede størrelse af de nye filer du har uploadet til dette indlæg ville overskride din kvote. Du kan være i stand til at gemme dit indlæg hvis du fjerner nogle af de vedhæftede filer.';
$string['newblog'] = 'Ny blog';
$string['newblogpost'] = 'Nyt blogindlæg i bloggen "%s"';
$string['newerposts'] = 'Nyere indlæg';
$string['nopostsyet'] = 'Ingen indlæg endnu.';
$string['addone'] = 'Lav et!';
$string['noimageshavebeenattachedtothispost'] = 'Ingen billeder er vedhæftet til dette indlæg.  Du skal uploade eller vedhæfte et billede til indlægget for at du kan indsætte det.';
$string['nofilesattachedtothispost'] = 'Ingen vedhæftede filer';
$string['noresults'] = 'Ingen blogindlæg fundet';
$string['olderposts'] = 'Ældre indlæg';
$string['post'] = 'indlæg';
$string['postbody'] = 'Brødtekst'; //Igen en der skal holdes øje med.
$string['postbodydesc'] = ' ';
$string['postedon'] = 'Skrevet den';
$string['postedbyon'] = 'Skrevet af %s den %s';
$string['posttitle'] = 'Titel';
$string['posts'] = 'indlæg';
$string['publish'] = 'Udgiv';
$string['publishfailed'] = 'Der skete en fejl.  Dit indlæg blev ikke udgivet.';
$string['publishblogpost?'] = 'Er du sikker på at du vil udgive dette indlæg?';
$string['published'] = 'Udgivet';
$string['remove'] = 'Fjern';
$string['save'] = 'Gem';
$string['saveandpublish'] = 'Gem';
$string['saveasdraft'] = 'Gem som kladde';
$string['savepost'] = 'Gem indlæg';
$string['savesettings'] = 'Gem indstillinger';
$string['settings'] = 'Indstillinger';
$string['thisisdraft'] = 'Dette indlæg er en kladde';
$string['thisisdraftdesc'] = 'Når dit indlæg er en kladde kan kun du se det.';
$string['title'] = 'Titel';
$string['update'] = 'Opdatering';
$string['verticalspace'] = 'Lodret plads';
$string['viewblog'] = 'Vis blog';
$string['youarenottheownerofthisblog'] = 'Du er ikke ejeren af denne blog';
$string['youarenottheownerofthisblogpost'] = 'Du er ikke ejeren af dette blogindlæg';
$string['cannotdeleteblogpost'] = 'Der skete en fejl med fjernelsen af dette blogindlæg.';

$string['baseline'] = 'Grundlinje';
$string['top'] = 'Top';
$string['middle'] = 'Midten';
$string['bottom'] = 'Bund';
$string['texttop'] = 'Tekst top';
$string['textbottom'] = 'Tekst bund';
$string['left'] = 'Venstre';
$string['right'] = 'Højre';
$string['src'] = 'Billede URL';
$string['image_list'] = 'Vedhæftet billede';
$string['alt'] = 'Beskrivelse';

$string['copyfull'] = 'Andre vil få deres egen kopi af din %s';
$string['copyreference'] = 'Andre kan vise din %s i deres visninger';
$string['copynocopy'] = 'Spring denne blok over når du kopierer visningen';

$string['viewposts'] = 'Kopierede indlæg (%s)';
$string['postscopiedfromview'] = 'Indlæg kopieret fra %s';

$string['youhavenoblogs'] = 'Du har ikke en blog.';
$string['youhaveoneblog'] = 'Du har 1 blog.';
$string['youhaveblogs'] = 'Du har %s blogs.';

$string['feedsnotavailable'] = 'Feeds er ikke tilgængelige i denne artefakt type.';
$string['feedrights'] = 'Copyright %s.';

$string['enablemultipleblogstext'] = 'Du har én blog. Hvis du vil have flere kan du slå indstillingen for flere blogs til på <a href="%saccount/">kontoindstillinger</a> siden.';
?>
