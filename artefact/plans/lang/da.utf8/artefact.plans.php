<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

/* Plans */
$string['description'] = 'Beskrivelse';
$string['deleteplanconfirm'] = 'Er du sikker på at du vil slette denne plan? Når planen slettes, fjernes alle opgaver den indeholder.';
$string['deleteplan'] = 'Slet plan';
$string['deletethisplan'] = 'Slet plan: \'%s\'';
$string['editplan'] = 'Rediger plan';
$string['editingplan'] = 'Redigerer plan';
$string['managetasks'] = 'Håndter opgaver';
$string['myplans'] = 'Mine planer';
$string['newplan'] = 'Ny plan';
$string['noplansaddone'] = 'Ingen planer endnu. %sLav en%s!';
$string['noplans'] = 'Ingen planer at vise';
$string['plan'] = 'Plan';
$string['plans'] = 'Planer';
$string['plandeletedsuccessfully'] = 'Planen er blevet slettet.';
$string['plannotdeletedsuccessfully'] = 'Der skete en fejl med sletning af planen.';
$string['plannotsavedsuccessfully'] = 'Der skete en fejl med indsendelsen af dette skema. Undersøg de markerede felter og prøv igen.';
$string['plansavedsuccessfully'] = 'Planen er blevet gemt.';
$string['planstasks'] = 'Plan \'%s\' opgaver.';
$string['planstasksdesc'] = 'Tilføj opgaver nedenfor eller brug knappen til højre til at begynde på din plan.';
$string['saveplan'] = 'Gem plan';
$string['title'] = 'Titel';
$string['titledesc'] = 'Titlen bliver brugt til at vise hver opgave i Mine planer bloktypen.';

/* Tasks */
$string['alltasks'] = 'Alle opgaver';
$string['completed'] = 'Udført';
$string['completiondate'] = 'Udførelsesdato';
$string['completeddesc'] = 'Marker dine opgaver som udførte.';
$string['deletetaskconfirm'] = 'Er du sikker på at du vil slete denne opgave?';
$string['deletetask'] = 'Slet opgave';
$string['deletethistask'] = 'Slet opgave: \'%s\'';
$string['edittask'] = 'Rediger opgave';
$string['editingtask'] = 'Redigerer opgave';
$string['mytasks'] = 'Mine opgaver';
$string['newtask'] = 'Ny opgave';
$string['notasks'] = 'Ingen opgaver at vise.';
$string['notasksaddone'] = 'Ingen opgaver endnu. %sLav en%s!';
$string['savetask'] = 'Gem opgave';
$string['task'] = 'opgave';
$string['tasks'] = 'Opgaver';
$string['taskdeletedsuccessfully'] = 'Opgaven er blevet slettet.';
$string['tasksavedsuccessfully'] = 'Opgaven er blevet gemt.';


?>
