<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage auth-internal
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['internal'] = 'Intern';
$string['title'] = 'Intern';
$string['description'] = 'Godkend mod Maharas egen databse';

$string['completeregistration'] = 'Gennemfør registering';
$string['emailalreadytaken'] = 'Denne e-mail-adresse er allerede registeret her';
$string['iagreetothetermsandconditions'] = 'Jeg accepterer sidens vilkår og betingelser';
$string['passwordformdescription'] = 'Dit kodeord skal være mindst seks tegn langt og indeholde mindst ét tal og to bogstaver';
$string['passwordinvalidform'] = 'Dit kodeord skal være mindst seks tegn langt og indeholde mindst ét tal og to bogstaver';
$string['registeredemailsubject'] = 'Du er nu registreret hos %s';
$string['registeredemailmessagetext'] = 'Hej %s,

Tak for at du registrede en konto hos %s. Følg venligst dette link
for at færdiggøre registreringen:

%sregister.php?key=%s

Linket udløber om 24 timer.

--
Venlig hilsen,
%s holdet';
$string['registeredemailmessagehtml'] = '<p>Hej %s,</p>
<p>Tak for at du registrede en konto hos %s. Følg venligst dette link
for at færdiggøre registreringen:</p>
<p><a href="%sregister.php?key=%s">%sregister.php?key=%s</a></p>
Linket udløber om 24 timer.</p>

<pre>--
Venlig hilsen,
%s holdet</pre>';
$string['registeredok'] = '<p>Du er nu blevet registreret. Check din e-mail for instruktioner i, hvordan du kan aktivere din konto</p>';
$string['registrationnosuchkey'] = 'Beklager, det ser ikke ud til at der er en registration med denne nøgle. Du har måske ventet længere end 24 timer med at færdiggøre din regristereing? Ellers er det nok vores fejl.';
$string['registrationunsuccessful'] = 'Beklager, dit forsøg på at registere dig mislykkedes. Det er vores fejl, ikke din. Prøv igen senere.';
$string['usernamealreadytaken'] = 'Beklager, dette brugernavn er allerede i brug';
$string['usernameinvalidform'] = 'Brugernavne må indeholde bogstaver, tal og de mest gængse symboler, og skal mære mellem 3 og 30 tegn lange.  Mellemrum er ikke tilladte.';
$string['youmaynotregisterwithouttandc'] = 'Du må ikke registere med mindre du accepterer sidens <a href="terms.php">vilkår og betingelser</a>';
